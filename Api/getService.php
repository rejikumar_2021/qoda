<?php
require 'application/autoload.php';
class GetService extends AppmodelUpdate
{
	
	/************* Device token**********************/
	public function write_Tocken()
	{
		$tocken=$_POST['token'];
		$fcm_key=$_POST['fcm_key'];
		
		if($tocken!=""&&$fcm_key!="")
		{
			
			$obj=new AppmodelUpdate();
			$Count=$obj->check_Tocken_exist($tocken);
			if($Count==0)
			{
				$res=$obj->write_device_info($tocken,$fcm_key);
				if($res==true)
				{
					$msg="Device information saved successfully";
				}
				else
				{
					$msg="Error in device information updation";
				}
			}
			else
			{
				$res=$obj->update_device_info($tocken,$fcm_key);
				if($res==true)
				{
					$msg="device information updated";
				}
				else
				{
					$msg="ERROR!Please try again";
				}
			}
				
		}
		else
		{
			$msg= "Required parameters are missing";
		}
				
				return $msg;
	}
	/************* Device token**********************/
	
/*********** Distance Calculation***********************/
	
public function getDistance($lat1,$long1,$lat2,$long2)
{
	
	$key="AIzaSyDKwHVgBBbPk8UshI_aih94gz4EVZgJ0k4";
	$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=".$key."&origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=en-EN";
	//echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
	//return $response_a;
	//print_r($response_a);
  	return array('distance' => $dist, 'time' => $time);
	
}	

public function getSpDistance()
{
	$cusLat=$_POST['latitude'];
	$cusLng=$_POST['longitude'];
	$sId=$_POST['sId'];
	if($cusLat!=""&&$cusLng!=""&&$sId!="")
	{
	$obj=new AppmodelUpdate();
	$obj->updateCusLocation($cusLat,$cusLng,$sId);
	$res=$obj->getSpLocationDetails($sId);
	switch($res[0]['srStatus'])
	{
		case 0:
		$message="Cancel by user";
		break;
		case 1:
		$message="Accepted by SP";
		break;
		case 2:
		$message="Accepted by user";
		break;
		case 3:
		$message="Cancel by SP";
		break;
		case 4:
		$message="Completed";
		break;
		
	}
	if($res[0]['lattitude']!=""&&$res[0]['longitude']!="")
	{
		
		$km=$this->getDistance($cusLat,$cusLng,$res[0]['lattitude'],$res[0]['longitude']);
		
	}
	else
	{
		$km=0;
	}
	
	$info=array("latitude"=>$res[0]['lattitude'],"longitude"=>$res[0]['longitude'],"distance"=>$km['distance'],"time"=>$km['time'],"status"=>$res[0]['srStatus'],"message"=>$message,"isCompleted"=>$res[0]['isCompleted']);
	}
	else
	{
		$info="required parameters missing";
	}
	return $info;
	
	
}

public function getCusDistance()
{
	$spLat=$_POST['latitude'];
	$spLng=$_POST['longitude'];
	$sId=$_POST['sId'];
	$type=1;
	$obj=new AppmodelUpdate();
	$obj->updateSpLocation($spLat,$spLng,$sId);
	$res=$obj->getCusLocationDetails($sId);
	switch($res[0]['status'])
	{
		case 0:
		$resMsg="Cancelled by user";
		break;
		case 1:
		$resMsg="Open";
		break;
		case 2:
		$resMsg="Cancel by Service Provider";
		break;
		case 3:
		$resMsg="Pending";
		break;
		case 4:
		$resMsg="Compleated";
		break;		
	}
	if($spLat!=""&&$spLng!=""&&$sId!="")
	{
	if($res[0]['lattitude']!=""&&$res[0]['longitude']!="")
	{
		
		$km=$this->getDistance($spLat,$spLng,$res[0]['lattitude'],$res[0]['longitude']);
	}
	else
	{
		$km=0;
	}
	
	$info=array("latitude"=>$res[0]['lattitude'],"longitude"=>$res[0]['longitude'],"distance"=>$km['distance'],"time"=>$km['time'],"status"=>$res[0]['status'],"message"=>$resMsg);
	}
	else
	{
		$info="required parameters are missing";
	}
	return $info;
	
}



}
/************* Distance Calculation*****************/

$service=$_POST['service'];
switch($service)
{
	case "token":
	$fun=new GetService();
	$res=$fun->write_Tocken();
	break;

	case "distance":
	$fun=new GetService();
	$res=$fun->getDistance();
	break;
	
	case "sp_distance":
	$fun=new GetService();
	$res=$fun->getSpDistance();
	
	break;
	
	case "cus_distance": 
	$fun=new GetService();
	$res=$fun->getCusDistance();
	break;
	
	
	default:
	$res=array("status"=>"Invalid request");
}
echo json_encode($res);
?>