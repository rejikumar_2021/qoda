<?php

class AppModel extends Database{
	      private $stripeKey="sk_live_ben1LV1rF9Ld3XwkLvb8Ms37"; // sk_test_Pp0EjdOCo9HLNyFThyBzLkVj test / live key  sk_live_ben1LV1rF9Ld3XwkLvb8Ms37
		 //private $stripeKey="sk_test_Pp0EjdOCo9HLNyFThyBzLkVj"; // sk_test_Pp0EjdOCo9HLNyFThyBzLkVj test / live key 
	      private $keySalt='krishanthakur97';
          private $_newArr=array();
	      private $_success='1';
	      private $_undefined='2';
          private $_error='0';
	      private $_emailNotExist="Oops! This email address doesn't exist!"; 
	      private $_emailExist="Oops! This email address already exist!";
	      private $_goAhead="Go Ahead";
	      private $_userNotExist="Oops! User doesn't exist";
	      private $_successLogin="Successfully Login"; 
	      private $_unexceptionError="Something goes wrong";
	      private $_faceLoginWrong="User account doesn't exist please SignUp";
	     // private $_userDeletedStatus="Your account has been blocked. Please contact with admin.";
		 private $_userDeletedStatus="Your account has been deleted.";
	      private $_passNotMatch="Email or Password doesn't match";
	      private $_alreadyExist="Already registerd exist";
	      private $_registerSuccess="Registerd successfully";
	      private $_oldPassword="Old Password doesn't match";
	      private $_chngeSucc="Password Change successfully";
	      private $_chkEmail="Email sent successfully";
	      private $_profileUpdate="Profile updated successfully";	
	      private $_logout="Logout successfully";	
	      private $_ariseTicket="Ticket arise successfully";
	      private $_alreadyServiceRequest="You have already requested for a service"; 
	      private $_anotherChk="Job is not available";
	      private $_alreadyJObsubmit="job not available";
	      private $_waitforUser="Awaiting customer confirmation";
	      private $_canceljob="Job cancelled successfully";
	      private $_jobCancelBySp="Job cancelled by Service Provider";
	      private $_amountInsufficient="Oops! Insufficient amount";	
	      private $_cancelByUser="Job cancelled by customer";
	      private $_approvedByOther="Customer has choose another service provider";
		  
	public function checkEmailExist($emailId){   //Check User Email Exist  
		try {
			
			$sMtUserRegistration=$this->_connection->prepare("SELECT userId from userRegistration WHERE userEmail='".$emailId."'");
			$sMtUserRegistration->execute();
			$count = $sMtUserRegistration->rowCount();
			$userData=$sMtUserRegistration->fetch(PDO::FETCH_ASSOC);
			
			$sMtSpRegistration=$this->_connection->prepare("SELECT spId from spRegistration WHERE spEmail='".$emailId."'");
			$sMtSpRegistration->execute();
			$countT = $sMtSpRegistration->rowCount(); 
			$spData=$sMtSpRegistration->fetch(PDO::FETCH_ASSOC);
			if($count==1&&$countT!=1)
			{
				$response=array('status'=>1,'message'=>"Already an User","regType"=>1,"ID"=>$userData['userId']);
			}
			if($count!=1&&$countT==1)
			{
				$response=array('status'=>1,'message'=>"Already a Service provider","regType"=>2,"ID"=>$spData['spId']);
			}
			
			if($count==1&&$countT==1)
			{
				$response=array('status'=>1,'message'=>"Already a Service provider and User","regType"=>3);
			}
			if($count!=1&&$countT!=1)
			{
				$response=array('status'=>0,'message'=>$this->_goAhead);
			}
			
			
			return $response;
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in checkEmailExist: " . $e->getMessage());
	        }

	}

	private function checkEmailLoginExist($emailId){   //Check User Email Exist   on login time
		try {
			$sMtUserRegistration=$this->_connection->prepare("SELECT userId from userRegistration WHERE userEmail='".$emailId."'");
			$sMtUserRegistration->execute();
		          $count = $sMtUserRegistration->rowCount();
			if($count==1){  return 1;	 }
			$sMtSpRegistration=$this->_connection->prepare("SELECT spId from spRegistration WHERE spEmail='".$emailId."'");
			$sMtSpRegistration->execute();
			$countT = $sMtSpRegistration->rowCount(); 
			if($countT==1){ return 1; }
				return 0;
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in checkEmailLoginExist: " . $e->getMessage());
	        }

	}

	private function checkUserStatus($emailId){   //Check User  Exist or Deleted  
		try {
			$sMtUserRegistration=$this->_connection->prepare("SELECT is_Deleted from userRegistration WHERE userEmail='".$emailId."'");
			$sMtUserRegistration->execute();
		          $urOw=$sMtUserRegistration->fetch(PDO::FETCH_OBJ);
			return $urOw->is_Deleted;
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in checkEmailLoginExist: " . $e->getMessage());
	        }

	}

	private function getUserPrimaryId($userId){ // Get Customer (User) Primary Key or match uniqe key
		try {
			
	$sMtUserRegistration=$this->_connection->prepare("SELECT userId,is_Deleted from userRegistration WHERE userUniqueId='".trim($userId)."'");
			$sMtUserRegistration->execute();
			$count = $sMtUserRegistration->rowCount();
			if($count==0){  echo json_encode(array('status'=>$this->_error,'message'=>$this->_userNotExist));  die; }
			$rOw=$sMtUserRegistration->fetch(PDO::FETCH_OBJ);
			if($rOw->is_Deleted==1){ echo json_encode(array('status'=>$this->_error,'message'=>$this->_userDeletedStatus)); die;   }
			return $rOw->userId;
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in getUserPrimaryId: " . $e->getMessage());
	        }
	}
	
	private function getServiceProvidePrimaryId($userId){    // Get Service Provider Primary Key or match uniqe key
		try {
			
			$sMtUserRegistration=$this->_connection->prepare("SELECT spId,spbLockStatus from spRegistration WHERE spUniqeId='".$userId."'");
			
			$sMtUserRegistration->execute();
			 $count = $sMtUserRegistration->rowCount();
			if($count==0){  echo json_encode(array('status'=>$this->_error,'message'=>$this->_userNotExist)); die;  }
			$rOw=$sMtUserRegistration->fetch(PDO::FETCH_OBJ);
			
			if($rOw->spbLockStatus==1){ echo json_encode(array('status'=>$this->_error,'message'=>$this->_userDeletedStatus));  die;  }
			return $rOw->spId;	
		 }catch (PDOException $e) { parent::close();
		 
		  	die("Connection failed in checkEmailExist: " . $e->getMessage());
	        }
	}
	
	
	private function getProviderCarInfo($userId){    // get Service Provider CAr Information
			$arr=array();
			$getCArrInfo=$this->_connection->prepare("SELECT * from spVehicleInfo WHERE spId='".$userId."'");
			$getCArrInfo->execute();
			$rOw=$getCArrInfo->fetch(PDO::FETCH_OBJ);	
			$arr['carImage']=(string)$rOw->spVhImage; 
			$arr['carModel']=(string)$rOw->spVhModelNo;
			$arr['year']=(string)$rOw->spVhYear;
			$arr['liesenceNo']=(string)$rOw->spVhLicenseNo;
			return $arr;
	}
	private function getCustomerCarInfo($userId){    // getUserCArInformation
			$arr=array();
			$getCArrInfo=$this->_connection->prepare("SELECT * from userVehicleInfo WHERE userId='".$userId."'");
			$getCArrInfo->execute();
			$rOw=$getCArrInfo->fetch(PDO::FETCH_OBJ);	
			$arr['carImage']=(string)$rOw->uvImage;
			$arr['carModel']=(string)$rOw->uvModel;
			$arr['year']=(string)$rOw->uvYear;
			$arr['liesenceNo']=(string)$rOw->uvLicenseNo;
			$arr['carMake']=(string)$rOw->uvMake;
			return $arr;
	}


	public function get_user_status($userUniqueId){    // get_user_status
			$arr=array();
          		$arr['status']='0';
			$getCArrInfo=$this->_connection->prepare("SELECT is_Deleted from userRegistration WHERE userUniqueId='".$userUniqueId."'");
			$getCArrInfo->execute();
			if($rOw=$getCArrInfo->fetch(PDO::FETCH_OBJ))
			{
			$arr['is_Deleted']=(string)$rOw->is_Deleted;
						$arr['status']='1';

			}
		 	return $arr;
	}
	public function get_sp_status($userUniqueId){    // get_sp_status
			$arr=array();
          		$arr['status']='0';
			$getCArrInfo=$this->_connection->query("SELECT spbLockStatus from spRegistration WHERE spUniqeId='".$userUniqueId."'");
		          $rOw=$getCArrInfo->fetch(PDO::FETCH_OBJ);
			 $arr['is_block']=(string)$rOw->spbLockStatus;
			 $arr['status']='1';
 
		 	return $arr;
	}
	
	
	public function userLoginUpdate($userEmail,$userPassword,$userloginType,$socialId,$deviceType,$deviceToken,$fcmkey)
	{
		//userType 0=>Service provider,1=>Customer,2=>both
					$reTurn=$this->checkEmailLoginExist($userEmail);   // Check Email Exist or Not
				if($reTurn==0){ return array('status'=>$this->_error,'message'=>$this->_faceLoginWrong);   die;  }
				$ureTurn=$this->checkUserStatus($userEmail);   // Check Email Exist or Not
				if($ureTurn==1){ return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus);  die;   }
				
				$pSswd=$this->encrypt($userPassword); 
				      
			 	$sMtUserManualLogin=$this->_connection->prepare("SELECT * from userRegistration WHERE userEmail='".$userEmail."' AND userPassword='".$pSswd."' ");	
				$sMtUserManualLogin->execute();
				$cUsLoginCount = $sMtUserManualLogin->rowCount();
				
				$sMtSpManualLogin=$this->_connection->prepare("SELECT * from spRegistration WHERE spEmail='".$userEmail."' AND spPassword='".$pSswd."' ");	
			$sMtSpManualLogin->execute();
			$spLoginCount = $sMtSpManualLogin->rowCount();
			
			$this->updateFcmKey($deviceToken,$fcmkey);
			
			if($userloginType==0){
			
			if($spLoginCount==1&&$cUsLoginCount==1)
			{
				$CusrOw=$sMtUserManualLogin->fetch(PDO::FETCH_OBJ);	
				
	 $this->_connection->query("update  userRegistration set  onlineStatus='1',deviceToken='".$deviceToken."' ,devcieType='".$deviceType."' where  userUniqueId='".$CusrOw->userUniqueId."' ");
	 
				$carInfO=$this->getCustomerCarInfo($CusrOw->userId);
				
				$rOw=$sMtSpManualLogin->fetch(PDO::FETCH_OBJ);
				
			if($rOw->spStatus==0)
			{

				$spAprovalStatus=array("approval"=>0,'message'=>'Please wait for admin approval');
			}
			else
			{
				$this->_connection->query("update  spRegistration set spDeviceType='".$deviceType."' ,spDeviceToken='".$deviceToken."', onlineStatus='1' where  spUniqeId='".$rOw->spUniqeId."' ");
				$spAprovalStatus=array("approval"=>1);
			}
			if($rOw->spbLockStatus==1)
			{
				$spLockStatus=array("lock"=>0,"message"=>$this->_userDeletedStatus);
			}
			else
			{
				$spLockStatus=array("lock"=>1);
			}
				
				$customer=array('userId'=>$CusrOw->userUniqueId,'userType'=>'2','userName'=>$CusrOw->userName,'image'=>$CusrOw->userImage,'loginType'=>$CusrOw->loginType,'userWallet'=>number_format($CusrOw->userWallet,2, '.', ''),'userNumber'=>$CusrOw->userMobile,'carInfo'=>$carInfO,'address1'=>$CusrOw->address1,'address2'=>$CusrOw->address2,'state'=>$CusrOw->state,'city'=>$CusrOw->city,'zipCode'=>$CusrOw->zipCode);
				
				$spDetails=array('userId'=>$rOw->spUniqeId,'userType'=>'0','userName'=>$rOw->spUserName,'image'=>(string)$rOw->spImage,'loginType'=>$rOw->spLoginType,'userWallet'=>number_format($rOw->spWallet,2, '.', ''),'userNumber'=>(string)$rOw->spMobile,'carInfo'=>$carInfO,'isWoklocationSet'=>$WoklocationSet,"address1"=>$rOw->spAddress1,"address2"=>$rOw->spAddress2,"state"=>$rOw->spState,"city"=>$rOw->spCity,"zipCode"=>$rOw->spZipCode,"spAprovalStatus"=>$spAprovalStatus,"spbLockStatus"=>$spLockStatus,"spID"=>$rOw->spUniqeId);
				$response=array('status'=>$this->_success,'message'=>$this->_successLogin,'userType'=>'2','customerDetails'=>$customer,"spDetails"=>$spDetails);
				return $response;
				
			}
			
			else if($spLoginCount==1&&$cUsLoginCount==0)
			{
				
				$rOw=$sMtSpManualLogin->fetch(PDO::FETCH_OBJ);
			if($rOw->spStatus==0){
				return array('status'=>$this->_error,'message'=>'Please wait for admin approval'); die; 
			}
			if($rOw->spbLockStatus==1){
				return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus);  die; 
			}

		$this->_connection->query("update  spRegistration set spDeviceType='".$deviceType."' ,spDeviceToken='".$deviceToken."', onlineStatus='1' where  spUniqeId='".$rOw->spUniqeId."' ");
		
				$carInfO=$this->getProviderCarInfo($rOw->spId); 
		$sMtregSpManualLogin=$this->_connection->prepare("SELECT * from spRegistration WHERE spUniqeId='".$rOw->spUniqeId."'  ");	
		$sMtregSpManualLogin->execute();
		$spregrOw=$sMtregSpManualLogin->fetch(PDO::FETCH_OBJ);
		$splatitude=$spregrOw->spLattitude;
		$splongitude=$spregrOw->spLongitude;
		$WoklocationSet='1';
		if(($splatitude=='0') && ($splongitude=='0'))
		{
			$WoklocationSet='0';
		}
			
	 return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->spUniqeId,'userType'=>'0','userName'=>$rOw->spUserName,'image'=>(string)$rOw->spImage,'loginType'=>$rOw->spLoginType,'userWallet'=>number_format($rOw->spWallet,2, '.', ''),'userNumber'=>(string)$rOw->spMobile,'carInfo'=>$carInfO,'isWoklocationSet'=>$WoklocationSet,"address1"=>$rOw->spAddress1,"address2"=>$rOw->spAddress2,"state"=>$rOw->spState,"city"=>$rOw->spCity,"zipCode"=>$rOw->spZipCode); 
			}
			
			
			else if($spLoginCount==0&&$cUsLoginCount==1)
			{
				$rOw=$sMtUserManualLogin->fetch(PDO::FETCH_OBJ);	

	 $this->_connection->query("update  userRegistration set  onlineStatus='1',deviceToken='".$deviceToken."' ,devcieType='".$deviceType."' where  userUniqueId='".$rOw->userUniqueId."' ");
	 
				$carInfO=$this->getCustomerCarInfo($rOw->userId); 
				 
			 	return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->userUniqueId,'userType'=>'1','userName'=>$rOw->userName,'image'=>$rOw->userImage,'loginType'=>$rOw->loginType,'userWallet'=>number_format($rOw->userWallet,2, '.', ''),'userNumber'=>$rOw->userMobile,'carInfo'=>$carInfO,'address1'=>$rOw->address1,'address2'=>$rOw->address2,'state'=>$rOw->state,'city'=>$rOw->city,'zipCode'=>$rOw->zipCode); 
			}
			else
			{
				return array('status'=>$this->_error,'message'=>$this->_passNotMatch);		
			}
			
			
			}
			else if($userloginType==1)
			{
				$sMtUserSocialLogin=$this->_connection->prepare("SELECT * from userRegistration WHERE userEmail='".$userEmail."'");
				$sMtSpSocialLogin=$this->_connection->prepare("SELECT * from spRegistration WHERE spEmail='".$userEmail."'");
				$sMtSpSocialLogin->execute();
				$sMtUserSocialLogin->execute();
				$spSocialcount = $sMtSpSocialLogin->rowCount();
				$socialcount = $sMtUserSocialLogin->rowCount();
				if($socialcount==1&&$spSocialcount==1)
				{
					
				
					
					$sp=$sMtSpSocialLogin->fetch(PDO::FETCH_OBJ);
					$user=$sMtUserSocialLogin->fetch(PDO::FETCH_OBJ);
					
					$carInfO=$this->getCustomerCarInfo($user->userId);
					
					if($sp->spStatus==0)
			{

				$spAprovalStatus=array("approval"=>0,'message'=>'Please wait for admin approval');
			}
			else
			{
				$this->_connection->query("update  spRegistration set spDeviceType='".$deviceType."' ,spDeviceToken='".$deviceToken."', onlineStatus='1' where  spUniqeId='".$rOw->spUniqeId."' ");
				$spAprovalStatus=array("approval"=>1);
			}
			if($sp->spbLockStatus==1)
			{
				$spLockStatus=array("lock"=>0,"message"=>$this->_userDeletedStatus);
			}
			else
			{
				$spLockStatus=array("lock"=>1);
			}
					
					$customer=array('userId'=>$user->userUniqueId,'userType'=>'2','userName'=>$user->userName,'image'=>$user->userImage,'loginType'=>$user->loginType,'userWallet'=>number_format($user->userWallet,2, '.', ''),'userNumber'=>$CusrOw->userMobile,'carInfo'=>$carInfO,'address1'=>$CusrOw->address1,'address2'=>$CusrOw->address2,'state'=>$CusrOw->state,'city'=>$CusrOw->city,'zipCode'=>$CusrOw->zipCode);
				
				$spDetails=array('userId'=>$sp->spUniqeId,'userType'=>'0','userName'=>$sp->spUserName,'image'=>(string)$sp->spImage,'loginType'=>$sp->spLoginType,'userWallet'=>number_format($sp->spWallet,2, '.', ''),'userNumber'=>(string)$sp->spMobile,'carInfo'=>$carInfO,'isWoklocationSet'=>$WoklocationSet,"address1"=>$sp->spAddress1,"address2"=>$sp->spAddress2,"state"=>$sp->spState,"city"=>$sp->spCity,"zipCode"=>$sp->spZipCode,"spAprovalStatus"=>$spAprovalStatus,"spbLockStatus"=>$spLockStatus,"spID"=>$sp->spUniqeId);
				$response=array('status'=>$this->_success,'message'=>$this->_successLogin,'userType'=>'2','customerDetails'=>$customer,"spDetails"=>$spDetails);
				return $response;
				
				
				
					
				}
				else if($socialcount==0&&$spSocialcount==1)
				{
				 
				$rOw=$sMtSpSocialLogin->fetch(PDO::FETCH_OBJ);
		
				if($rOw->spStatus==0){
				return array('status'=>$this->_error,'message'=>'Please wait for admin approval'); die; 
			}
				if($rOw->spbLockStatus==1){
				return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus);  die; 
			}
				$splatitude=$rOw->spLattitude;
					$splongitude=$rOw->spLongitude;
					$WoklocationSet='1';
					if(($splatitude=='0') && ($splongitude=='0'))
					{
						$WoklocationSet='0';
					}
			$this->_connection->query("update  spRegistration set spDeviceType='".$deviceType."' ,spDeviceToken='".$deviceToken."',onlineStatus='1' where  spUniqeId='".$rOw->spUniqeId."' ");
	
				 $carInfO=$this->getProviderCarInfo($rOw->spId); 
	 return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->spUniqeId,'userType'=>'0','userName'=>$rOw->spUserName,'image'=>(string)$rOw->spImage,'loginType'=>$rOw->spLoginType,'userWallet'=>number_format($rOw->spWallet,2, '.', ''),'userNumber'=>(string)$rOw->spMobile,'carInfo'=>$carInfO,'isWoklocationSet'=>$WoklocationSet,"address1"=>$rOw->spAddress1,"address2"=>$rOw->spAddress2,"spState"=>$rOw->spState,"spCity"=>$rOw->spCity,"spZipCode"=>$rOw->spZipCode); 
	
				}
				else if($socialcount==1&&$spSocialcount==0)
				{
						
				
						$rOw=$sMtUserSocialLogin->fetch(PDO::FETCH_OBJ);

					if($rOw->is_Deleted==1){
				return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus); 
			}
				
	 $this->_connection->query("update  userRegistration set   onlineStatus='1',deviceToken='".$deviceToken."' ,devcieType='".$deviceType."' where  userUniqueId='".$rOw->userUniqueId."' ");
	 
	
				 $carInfO=$this->getCustomerCarInfo($rOw->userId); 
			 	return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->userUniqueId,'userType'=>'1','userName'=>$rOw->userName,'image'=>$rOw->userImage,'loginType'=>$rOw->loginType,'userWallet'=>number_format($rOw->userWallet,2, '.', ''),'userNumber'=>$rOw->userMobile,'carInfo'=>$carInfO,'address_1'=>$rOw->address1,'address2'=>$rOw->address2,'state'=>$rOw->state,'city'=>$rOw->city,'zipCode'=>$rOw->zipCode); 
				
				
				
				}
				else if($socialcount==0&&$spSocialcount==0)
				{
					return array('status'=>$this->_error,'message'=>$this->_faceLoginWrong);  
				}
			}
			
				
				
		
		
	}

	public function userLogin($userEmail,$userPassword,$userloginType,$socialId,$deviceType,$deviceToken){    // Customer Login and Service Provider Login  userType 0 for Service Provider & 1 for customer


			if($userloginType==0){
				$reTurn=$this->checkEmailLoginExist($userEmail);   // Check Email Exist or Not
				if($reTurn==0){ return array('status'=>$this->_error,'message'=>$this->_faceLoginWrong);   die;  }
				$ureTurn=$this->checkUserStatus($userEmail);   // Check Email Exist or Not
				if($ureTurn==1){ return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus);  die;   }


				$pSswd=$this->encrypt($userPassword); 
				      
			 	$sMtUserManualLogin=$this->_connection->prepare("SELECT * from userRegistration WHERE userEmail='".$userEmail."' AND userPassword='".$pSswd."' ");	
				$sMtUserManualLogin->execute();
				$manualCount = $sMtUserManualLogin->rowCount();	
				
				
				if($manualCount==1){ 
					$rOw=$sMtUserManualLogin->fetch(PDO::FETCH_OBJ);	

	 $this->_connection->query("update  userRegistration set  onlineStatus='1',deviceToken='".$deviceToken."' ,devcieType='".$deviceType."' where  userUniqueId='".$rOw->userUniqueId."' ");
	 
	 // $this->_connection->query("update device_info set uid='".$rOw->userId."',user_type='1' where token='".$deviceToken."' ");
	  
				$carInfO=$this->getCustomerCarInfo($rOw->userId);  

			 	return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->userUniqueId,'userType'=>'1','userName'=>$rOw->userName,'image'=>$rOw->userImage,'loginType'=>$rOw->loginType,'userWallet'=>number_format($rOw->userWallet,2, '.', ''),'userNumber'=>$rOw->userMobile,'carInfo'=>$carInfO,'address_1'=>$rOw->address1,'address2'=>$rOw->address2,'state'=>$rOw->state,'city'=>$rOw->city,'zipCode'=>$rOw->zipCode); 
				  } 
	
			$sMtSpManualLogin=$this->_connection->prepare("SELECT * from spRegistration WHERE spEmail='".$userEmail."' AND spPassword='".$pSswd."' ");	
			$sMtSpManualLogin->execute();
			$spLoginCount = $sMtSpManualLogin->rowCount();
			if($spLoginCount==1){ 
			$rOw=$sMtSpManualLogin->fetch(PDO::FETCH_OBJ);
			if($rOw->spStatus==0){
				return array('status'=>$this->_error,'message'=>'Please wait for admin approval'); die; 
			}
			if($rOw->spbLockStatus==1){
				return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus);  die; 
			}

		$this->_connection->query("update  spRegistration set spDeviceType='".$deviceType."' ,spDeviceToken='".$deviceToken."', onlineStatus='1' where  spUniqeId='".$rOw->spUniqeId."' ");
		
		//$this->_connection->query("update device_info set uid='".$rOw->userId."',user_type='0' where token='".$deviceToken."' ");
		
		
				$carInfO=$this->getProviderCarInfo($rOw->spId); 
		$sMtregSpManualLogin=$this->_connection->prepare("SELECT * from spRegistration WHERE spUniqeId='".$rOw->spUniqeId."'  ");	
		$sMtregSpManualLogin->execute();
		$spregrOw=$sMtregSpManualLogin->fetch(PDO::FETCH_OBJ);
		$splatitude=$spregrOw->spLattitude;
		$splongitude=$spregrOw->spLongitude;
		$WoklocationSet='1';
		if(($splatitude=='0') && ($splongitude=='0'))
		{
			$WoklocationSet='0';
		}
			
	 return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->spUniqeId,'userType'=>'0','userName'=>$rOw->spUserName,'image'=>(string)$rOw->spImage,'loginType'=>$rOw->spLoginType,'userWallet'=>number_format($rOw->spWallet,2, '.', ''),'userNumber'=>(string)$rOw->spMobile,'carInfo'=>$carInfO,'isWoklocationSet'=>$WoklocationSet,"address1"=>$rOw->spAddress1,"address2"=>$rOw->spAddress2,"spState"=>$rOw->spState,"spCity"=>$rOw->spCity,"spZipCode"=>$rOw->spZipCode); 
			}
			
			if($manualCount==0 || $spLoginCount==0 ){ return array('status'=>$this->_error,'message'=>$this->_passNotMatch);  }
				
			}else if($userloginType==1){
		$sMtUserSocialLogin=$this->_connection->prepare("SELECT * from userRegistration WHERE userEmail='".$userEmail."'");
				$sMtUserSocialLogin->execute();	
				$socialcount = $sMtUserSocialLogin->rowCount();
				if($socialcount==1){ 
						$rOw=$sMtUserSocialLogin->fetch(PDO::FETCH_OBJ);

					if($rOw->is_Deleted==1){
				return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus); 
			}
	 $this->_connection->query("update  userRegistration set   onlineStatus='1',deviceToken='".$deviceToken."' ,devcieType='".$deviceType."' where  userUniqueId='".$rOw->userUniqueId."' ");
	 
	 //$this->_connection->query("update device_info set uid='".$rOw->userId."',user_type='1' where token='".$deviceToken."' ");
	 
				 $carInfO=$this->getCustomerCarInfo($rOw->userId); 
			 	return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->userUniqueId,'userType'=>'1','userName'=>$rOw->userName,'image'=>$rOw->userImage,'loginType'=>$rOw->loginType,'userWallet'=>number_format($rOw->userWallet,2, '.', ''),'userNumber'=>$rOw->userMobile,'carInfo'=>$carInfO,'address_1'=>$rOw->address1,'address2'=>$rOw->address2,'state'=>$rOw->state,'city'=>$rOw->city,'zipCode'=>$rOw->zipCode); 
     }
				$sMtSpSocialLogin=$this->_connection->prepare("SELECT * from spRegistration WHERE spEmail='".$userEmail."'");
				$sMtSpSocialLogin->execute();
				$spSocialcount = $sMtSpSocialLogin->rowCount();
				if($spSocialcount==1){ 
				$rOw=$sMtSpSocialLogin->fetch(PDO::FETCH_OBJ);
		
				if($rOw->spStatus==0){
				return array('status'=>$this->_error,'message'=>'Please wait for admin approval'); die; 
			}
				if($rOw->spbLockStatus==1){
				return array('status'=>$this->_error,'message'=>$this->_userDeletedStatus);  die; 
			}
				$splatitude=$rOw->spLattitude;
					$splongitude=$rOw->spLongitude;
					$WoklocationSet='1';
					if(($splatitude=='0') && ($splongitude=='0'))
					{
						$WoklocationSet='0';
					}
			$this->_connection->query("update  spRegistration set spDeviceType='".$deviceType."' ,spDeviceToken='".$deviceToken."',onlineStatus='1' where  spUniqeId='".$rOw->spUniqeId."' ");
			
			//$this->_connection->query("update device_info set uid='".$rOw->userId."',user_type='0' where token='".$deviceToken."' ");
			
			
				 $carInfO=$this->getProviderCarInfo($rOw->spId); 
	 return array('status'=>$this->_success,'message'=>$this->_successLogin,'userId'=>$rOw->spUniqeId,'userType'=>'0','userName'=>$rOw->spUserName,'image'=>(string)$rOw->spImage,'loginType'=>$rOw->spLoginType,'userWallet'=>number_format($rOw->spWallet,2, '.', ''),'userNumber'=>(string)$rOw->spMobile,'carInfo'=>$carInfO,'isWoklocationSet'=>$WoklocationSet,"address1"=>$rOw->spAddress1,"address2"=>$rOw->spAddress2,"spState"=>$rOw->spState,"spCity"=>$rOw->spCity,"spZipCode"=>$rOw->spZipCode); }	
	
				if($socialcount==0 || $spSocialcount==0){ return array('status'=>$this->_error,'message'=>$this->_faceLoginWrong);  }
			}else{
				if($count==0){  return array('status'=>$this->_error,'message'=>$this->_unexceptionError);  }

			}

	}	
	 
  	public function userRegister($userEmail,$userName,$socialId,$userMobile,$userPassword,$userImage,$usercarModel,$userCarYear,$userlicenseNo,$usercarImage,$userloginType,$address1,$address2,$state,$city,$postcode,$carmake){ // Customer Register 
		try{ 
	
		if(!empty($userEmail)){
		$sMtUserRegistration=$this->_connection->prepare("SELECT userId from userRegistration WHERE userEmail='".$userEmail."'");
		$sMtUserRegistration->execute();
		$count = $sMtUserRegistration->rowCount();
		if($count==1){ return array('status'=>$this->_error,'message'=>$this->_alreadyExist); }
		}
		if(!empty($socialId)&&!empty($userloginType)==1){
		$sMtUserSocailRegistration=$this->_connection->prepare("SELECT userId from userRegistration WHERE socialId='".$socialId."'"); // check social Customer 
		$sMtUserSocailRegistration->execute();
		$countT = $sMtUserSocailRegistration->rowCount();
		if($countT==1){ return array('status'=>$this->_error,'message'=>$this->_alreadyExist); }
				
		$sMtUserSocailSrRegistration=$this->_connection->prepare("SELECT spId from spRegistration WHERE spSocialId='".$socialId."'"); // check social SERvice provider
		$sMtUserSocailSrRegistration->execute();
		$countTT = $sMtUserSocailSrRegistration->rowCount();
		if($countTT==1){ return array('status'=>$this->_error,'message'=>$this->_alreadyExist); }
		}
		$pSswd=$this->encrypt($userPassword);                                                      // Password Encrption
		$userUniqeId=uniqid().strtotime(date("Y-m-d h:i:s")).'u';                                        // generated user uniqueId 
		$this->_connection->query("insert into userRegistration (userName, loginType, socialId, userEmail, userPassword, userMobile,  userUniqueId, userImage, createdTime, address1, address2, state, city, zipCode) VALUES ('".$userName."','".$userloginType."','".$socialId."','".$userEmail."','".$pSswd."','".$userMobile."','".$userUniqeId."','".$userImage."','".date("Y-m-d h:i:s")."','".$address1."','".$address2."','".$state."','".$city."','".$postcode."')");		 					
		$onjEmail=new Emails();
		//$onjEmail->usercustomerSignUpEmail($userEmail);
		$onjEmail->customerConfirmEmail($userName,$userEmail);
		$userId=$this->_connection->lastInsertId(); 
		
		$this->_connection->query("insert into userVehicleInfo (uvImage,uvModel,uvYear,uvLicenseNo,uvMake,userId) VALUES ('".$usercarImage."','".$usercarModel."','".$userCarYear."','".$userlicenseNo."','".$carmake."','".$userId."') ");
		return array('status'=>$this->_success,'message'=>$this->_registerSuccess);
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in userRegister: " . $e->getMessage());
	        }

	} 
	  public function spRegister($userEmail,$userName,$socialId,$userMobile,$userPassword,$userImage,$usercarModel,$userCarYear,$userlicenseNo,$usercarImage,$userloginType,$serviceType,$latt,$lOng,$address1,$address2,$state,$city,$postcode)	{ 
			// Service Provider Registeration
                    try{ 
			
			 require(__ROOT__.'/stripe/init.php');		
			if(!empty($userEmail)){
			$sMtUserRegistration=$this->_connection->prepare("SELECT spId from spRegistration WHERE spEmail='".$userEmail."'");   // check userEmail
			$sMtUserRegistration->execute();
			$count = $sMtUserRegistration->rowCount();
			if($count==1){ return array('status'=>$this->_error,'message'=>$this->_alreadyExist); }
			}
			if(!empty($socialId)&&!empty($userloginType)==1){
				$sMtUserSocailsrRegistration=$this->_connection->prepare("SELECT spId from spRegistration WHERE spSocialId='".$socialId."'"); // check social SERvice provider 
				$sMtUserSocailsrRegistration->execute();
				$countTt = $sMtUserSocailsrRegistration->rowCount();
				if($countTt==1){ return array('status'=>$this->_error,'message'=>$this->_alreadyExist); }

				$sMtUserSocailRegistration=$this->_connection->prepare("SELECT userId from userRegistration WHERE socialId='".$socialId."'"); // check social Customer 
				$sMtUserSocailRegistration->execute();
				$countT = $sMtUserSocailRegistration->rowCount();
				if($countT==1){ return array('status'=>$this->_error,'message'=>$this->_alreadyExist); }
			
			}
			\Stripe\Stripe::setApiKey($this->stripeKey);
			$account=\Stripe\Account::create(array(
				  "managed" => true,
				  "country" => "US",
				  "email" => $userEmail
				));
		             $sTipeAccountId=$account['id'];
			   $sTipeSecketKey=$account['keys']->secret;
			   $sTripePublish=$account['keys']->publishable;
	 		$pSswd=$this->encrypt($userPassword);                                                                         // Password Encrption
			$userUniqeId=uniqid().strtotime(date("Y-m-d h:i:s")).'sp';					       // generated Service Provider uniqueId 
			$this->_connection->query("insert into spRegistration (spUserName, spLoginType, spSocialId, spEmail, spPassword, spMobile,  spUniqeId, spImage, createdTime,services,spLattitude,spLongitude,stripeAccount,stripeSecret,stripePublish,spAddress1,spAddress2,spState,spCity,spZipCode) VALUES ('".$userName."','".$userloginType."','".$socialId."','".$userEmail."','".$pSswd."','".$userMobile."','".$userUniqeId."','".$userImage."','".date("Y-m-d h:i:s")."','".$serviceType."','".$latt."','".$lOng."','".$sTipeAccountId."','".$sTipeSecketKey."','".$sTripePublish."','".$address1."','".$address2."','".$state."','".$city."','".$postcode."')");		 					
			$userId=$this->_connection->lastInsertId(); 
			$onjEmail=new Emails();
			 $onjEmail->sendEmailSp_SignUp($userEmail);
			$onjEmail->sendMailAdmin();
 
			$this->_connection->query("insert into spVehicleInfo (spVhImage,spVhModelNo,spVhYear,spVhLicenseNo,spId) VALUES ('".$usercarImage."','".$usercarModel."','".$userCarYear."','".$userlicenseNo."','".$userId."') "); 
			return array('status'=>$this->_success,'message'=>$this->_registerSuccess); 
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in spRegister: " . $e->getMessage());
	        }catch (Exception $e) {
			 	 $body = $e->getJsonBody();
				  $err  = $body['error'];
				 $mEssAge=$err['message'];
				 return array('status'=>$this->_error,'message'=>$mEssAge);	
		}

	}  
	
	 public function chngePassword($userId,$userType,$oldPassword,$newPassword){                   // Change Password
		  try{ 
			if($userType==1){  		  // Customer 
				$uSERId=$this->getUserPrimaryId($userId);
				$oldpSswd=$this->encrypt($oldPassword);   
				$newpSswd=$this->encrypt($newPassword); 
				$sMtUserChngePass=$this->_connection->prepare("SELECT userId from userRegistration WHERE userId='".$uSERId."'  AND userPassword='".$oldpSswd."'");
				$sMtUserChngePass->execute();
				$count = $sMtUserChngePass->rowCount();
				if($count==1){
					$this->_connection->query("update userRegistration set userPassword='".$newpSswd."' where  userId='".$uSERId."' ");
					return array('status'=>$this->_success,'message'=>$this->_chngeSucc); 	
				}else{
					return array('status'=>$this->_error,'message'=>$this->_oldPassword); 
				}
			}else if($userType==0){		 // Service Provider			
				$uSERId=$this->getServiceProvidePrimaryId($userId);
				$oldpSswd=$this->encrypt($oldPassword);   
				$newpSswd=$this->encrypt($newPassword); 
				$sMtUserChngePass=$this->_connection->prepare("SELECT spId from spRegistration WHERE spId='".$uSERId."'  AND spPassword='".$oldpSswd."'");
				$sMtUserChngePass->execute();
				$count = $sMtUserChngePass->rowCount();
				if($count==1){
					$this->_connection->query("update spRegistration set spPassword='".$newpSswd."' where  spId='".$uSERId."' ");
					return array('status'=>$this->_success,'message'=>$this->_chngeSucc); 	
				}else{
					return array('status'=>$this->_error,'message'=>$this->_oldPassword); 
				}			
			}else{
					return array('status'=>$this->_error,'message'=>$this->_unexceptionError); 
			}
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in chngePassword: " . $e->getMessage());
	        }	
	 }
	 private  function getMaxMile(){
			  try{ 
				$getuserS=$this->_connection->query("select * from app_settings");
				$rOw=$getuserS->fetch(PDO::FETCH_OBJ);
				$arr['radious']=$rOw->radious_out_range;
				$arr['travel_cost']=$rOw->travel_cost;
				 return $arr;
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in getMaxMile: " . $e->getMessage());
	        }	
         }
	 public function getopenServices($userId){         // Service Provider Open Jobs  
		  try{ 
			$getmargin=$this->_connection->query("select admin_margin from app_settings where id='1' ");
		$getRowMargin=$getmargin->fetch(PDO::FETCH_OBJ);	
			
			$aRR=array();
			$uSERId=$this->getServiceProvidePrimaryId($userId);
			$getuserSRequest=$this->_connection->prepare("select srId from serviceRespons where spId='".$uSERId."' AND  serviceId=:serviceIdd AND srStatus='1'  ");
			$getuserS=$this->_connection->query("select * from spRegistration where spId='".$uSERId."' AND  sp_availability='0' ");
			$count = $getuserS->rowCount(); 
			
			if($count==0){
				$getuserSRequest=$this->_connection->query("select srId from serviceRespons where spId='".$uSERId."' AND srStatus='2'  ");
				$countT = $getuserSRequest->rowCount(); 
				if($countT==0){
					$this->_connection->query("update spRegistration set sp_availability='0' where  spId='".$uSERId."' ");
				} 
			}
			if($count==1){
				
			$getRow=$getuserS->fetch(PDO::FETCH_OBJ);
			
			$lAtT=$getRow->spLattitude;
			$lONG=$getRow->spLongitude;
			$maxMile=$this->getMaxMile();
			$ids=implode("','",$getRow->services);  
			
$arrays=array_map('intval', explode(',',$getRow->services));
$idss = implode("','",$arrays);


			$getOpenRequestInformation=$this->_connection->query("select *,( 3959 * acos( cos( radians('".$lAtT."') ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians('".$lONG."') ) + sin( radians('".$lAtT."') ) * sin( radians( lattitude ) ) ) ) AS distance from serviceRequest where status='1' AND serviceType IN ('".$idss."')   HAVING distance <'".$maxMile['radious']."' ORDER BY serviceId Desc ");

					while($rOw=$getOpenRequestInformation->fetch(PDO::FETCH_OBJ)){
						$serviceRequest=$rOw->serviceId;
						$getuserSRequest->bindParam(':serviceIdd',$serviceRequest);
						$getuserSRequest->execute(); 
						$roWCouNt=$getuserSRequest->rowCount();  
						if($roWCouNt==0){
							
						$arr['service_type']=$rOw->serviceType;
						$distance=$rOw->distance;
						if($distance<1){$distancee=1;}else{ $distancee=$distance;}
						$Costt=$distancee*$maxMile['travel_cost'];
						$distanceCost=number_format($Costt,2, '.', '');    //ceil($distance)*$maxMile['travel_cost'];
						$serviceCost=$this->getServiceTypeCharge($rOw->serviceType);
						$sERviceCost=number_format($serviceCost,2, '.', '');
						$margIn=number_format(($sERviceCost*$getRowMargin->admin_margin)/100,2, '.', '');
						$afterPercentCost=number_format($sERviceCost-$margIn,2, '.', '');
						$arr['servicecost']=$afterPercentCost;
						$arr['travelcost']=$distanceCost;
						$arr['totalcost']=number_format($afterPercentCost+$distanceCost,2, '.', '');
						$arr['serviceRequestId']=$rOw->serviceId;
						$arr['latitude']=$rOw->lattitude; 
						$arr['longitude']=$rOw->longitude;
					 	$arr['distance']=number_format($distance,2);
						$arr['timeAgo']=strtotime($rOw->createDate)-strtotime(date('Y-m-d h:i:s'));
						$arr['userInfo']=$this->getUserProfileInfo($rOw->userId);
						$newArr[]=$arr;
						}
					}  if(!$newArr){ $newArr=array(); }
					return array('status'=>$this->_success,'getJobs'=>$newArr);					
			} 
			return array('status'=>$this->_success,'getJobs'=>$aRR); 
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in getopenServices: " . $e->getMessage());
	        }	
	} 

	public function getPendingJobs($userId){    // Service Provider Pending Jobs  
		  try{ 
			$aRR=array();
			$uSERId=$this->getServiceProvidePrimaryId($userId);
			$getserviceRequest=$this->_connection->prepare("select *,( 3959 * acos( cos( radians(:lAT) ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(:lONG) ) + sin( radians(:lAT) ) * sin( radians( lattitude ) ) ) ) AS distance from serviceRequest where  serviceId=:serViceId");
			$getUserService=$this->_connection->query("select * from serviceRespons where spId='".$uSERId."' AND ( srStatus='1' OR  srStatus='2' ) AND isCompleted='0' ");
			while($rOw=$getUserService->fetch(PDO::FETCH_OBJ)){
						$getserviceRequest->bindParam(':serViceId',$rOw->serviceId);
						$getserviceRequest->bindParam(':lAT',$rOw->lattitude);
						$getserviceRequest->bindParam(':lONG',$rOw->longitude);
						$getserviceRequest->execute();
						$rOwWwW=$getserviceRequest->fetch(PDO::FETCH_OBJ);
						$arr['service_type']=$rOwWwW->serviceType; 
						$arr['servicecost']=number_format($rOw->serviceCost,2, '.', '');  
						$arr['travelcost']=$rOw->travelCost; 
						$arr['totalcost']=$rOw->totalCost; 
						$arr['serviceRequestId']=$rOw->serviceId;
					    $arr['serviceResponseId']=$rOw->srId;
						$arr['latitude']=$rOw->lattitude; 
						$arr['longitude']=$rOw->longitude;
						$arr['respnoseStatus']=$rOw->srStatus;
					    $arr['distance']=number_format($rOwWwW->distance,2);
						$arr['timeAgo']=strtotime($rOw->dateTime)-strtotime(date('Y-m-d h:i:s'));
						$arr['userInfo']=$this->getUserProfileInfo($rOw->userId);
						$newArr[]=$arr;

	 		} if(!$newArr){$newArr=array();}
				return array('status'=>$this->_success,'getJobs'=>$newArr); 	
		  }catch (PDOException $e) { parent::close();
		  die("Connection failed in getPendingJobs: " . $e->getMessage());
	        }	
	}
	public  function usergiveRating($userId,$rating,$comment,$srId){
		  try{ 
			$uSERId=$this->getUserPrimaryId($userId);
			$ratING=$this->_connection->query("select * from serviceRespons where userId='".$uSERId."' AND srId='".$srId."' ");
			$roWCount=$ratING->rowCount();
			if($roWCount==1){
				$rOw=$ratING->fetch(PDO::FETCH_OBJ);
				$ratINGr=$this->_connection->query("select * from rating where userId='".$uSERId."' AND srId='".$srId."' ");	
				$roWCountr=$ratING->rowCount();
				if($roWCountt==0){
					$this->_connection->query("INSERT INTO rating (userId, spId, srId, comment, rating) VALUES ('".$uSERId."', '".$rOw->spId."',  '".$srId."',  '".addslashes($comment)."',  '".$rating."')");
				}
				return array('status'=>$this->_success);
			}
			return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
		  }catch (PDOException $e) { parent::close();
		  die("Connection failed in usergiveRating: " . $e->getMessage());
	        }	
	}
	public function giveDispute($userId,$reasON,$comment,$srId){
		 try{
			$uSERId=$this->getUserPrimaryId($userId);
			$ratING=$this->_connection->query("select * from serviceRespons where userId='".$uSERId."' AND srId='".$srId."' ");
			$roWCount=$ratING->rowCount();
			if($roWCount==1){
				$rOw=$ratING->fetch(PDO::FETCH_OBJ);
	$this->_connection->query("INSERT INTO dispute_cases (srId, userId, spId, userComments, userReason, spResponse,  created_date) VALUES ('".$srId."', '".$uSERId."', '".$rOw->spId."', '".addslashes($comment)."','".addslashes($reasON)."', '', '".date('Y-m-d h:i:s')."')");	
			$emailObj=new Emails();
			$emailObj->sendEmailAdmindispute();			
			return array('status'=>$this->_success);
			}
			return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in usergiveRating: " . $e->getMessage());
	        }	 
	}
	private function getdesputeinformation($userId,$srId){   //get dispute Information
		  try{ 
			$arr='';
			$disPute=$this->_connection->query("select * from dispute_cases where spId='".$userId."' AND srId='".$srId."' ");	
			$rOw=$disPute->fetch(PDO::FETCH_OBJ);

			          $arr['disputeId']=(string)$rOw->disputeId;
				$arr['userReason']=(string)$rOw->userReason;
				$arr['userComment']=(string)$rOw->userComments;
				$arr['yourResponse']=(string)$rOw->spResponse;
				$arr['dstatus']=(string)$rOw->disputeStatus;
				$datee=strtotime(date("Y-m-d h:i:s"))-strtotime($rOw->created_date);
				if($datee<86400){
					$arr['disputeTimeover']='0';	
				}else{
				      $arr['disputeTimeover']='1';	
				}
			return $arr;
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in getdesputeinformation: " . $e->getMessage());
	        }	 
	}
	
	public function giveSpDisputeResponse($userId,$disputeId,$respnoseMsg){
		 try{ 
			$uSERId=$this->getServiceProvidePrimaryId($userId);
			$disPute=$this->_connection->query("select * from dispute_cases where spId='".$uSERId."' AND disputeId='".$disputeId."' ");
			$rowCount=$disPute->rowCount();
			if($rowCount==1){
				$this->_connection->query("update  dispute_cases set spResponse='".addslashes($respnoseMsg)."' where '".$uSERId."' AND disputeId='".$disputeId."'  ");
				return array('status'=>$this->_success);
			}
			
			return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in giveSpDisputeResponse: " . $e->getMessage());
	        }	 
	}	

	public function getCompleteJobs($userId){ // Service Provider Complete Jobs  
		  try{ 
		$disPute=$this->_connection->prepare("select * from dispute_cases where spId=:ssPid AND srId=:sRIddd ");
		$ratING=$this->_connection->prepare("select * from rating where spId=:sPid AND srId=:sRIdd ");
		$getserviceRequest=$this->_connection->prepare("select *,( 3959 * acos( cos( radians(:lAT) ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(:lONG) ) + sin( radians(:lAT) ) * sin( radians( lattitude ) ) ) ) AS distance from serviceRequest where  serviceId=:serViceId");
			$uSERId=$this->getServiceProvidePrimaryId($userId);
			$getRecords=$this->_connection->query("select * from serviceRespons where spId='".$uSERId."' AND isCompleted='1' AND srStatus='4' Order By   srId DESC ");
			while($rOw=$getRecords->fetch(PDO::FETCH_OBJ)){
						$getserviceRequest->bindParam(':serViceId',$rOw->serviceId);
						$getserviceRequest->bindParam(':lAT',$rOw->lattitude);
						$getserviceRequest->bindParam(':lONG',$rOw->longitude);
						$getserviceRequest->execute();
						$rOwWwW=$getserviceRequest->fetch(PDO::FETCH_OBJ);
						$arr['service_type']=$rOwWwW->serviceType; 
					          $arr['servicecost']=number_format($rOw->afterPercentCost,2, '.', '');  
						$arr['travelcost']=$rOw->travelCost; 
						$arr['totalcost']=$rOw->totalcostSp; 
						$arr['serviceRequestId']=$rOw->serviceId;
					          $arr['serviceResponseId']=$rOw->srId;
						$arr['latitude']=$rOw->lattitude; 
						$arr['longitude']=$rOw->longitude; 
						$arr['respnoseStatus']=$rOw->srStatus;
					    	$arr['distance']=number_format($rOwWwW->distance,2);
						$arr['datee']=$rOw->dateTime;
						$arr['timeAgo']=strtotime($rOw->dateTime)-strtotime(date('Y-m-d h:i:s'));
						$ratING->bindParam(':sPid',$uSERId);
						$ratING->bindParam(':sRIdd',$rOw->srId);
						$ratING->execute();
						$roWCount=$ratING->rowCount();
						$disPute->bindParam(':ssPid',$uSERId);
						$disPute->bindParam(':sRIddd',$rOw->srId);
						$disPute->execute();
						$roWCountt=$disPute->rowCount();
						if($roWCount==0){
							$arr['rating']='0';
							$arr['rateCmt']='';
						}else{
							$getRows=$ratING->fetch(PDO::FETCH_OBJ);
							$arr['rating']=(string)$getRows->rating;
							$arr['rateCmt']=(string)$getRows->comment;
						}
						$arr['disputeExist']=(string)$roWCountt;
						$arr['disputeinfo']=$this->getdesputeinformation($rOw->spId,$rOw->srId);
						$arr['userInfo']=$this->getUserProfileInfo($rOw->userId);	

						$newArr[]=$arr;
		
			}if(!$newArr){ $newArr=array(); }
			return array('status'=>$this->_success,'getJobs'=>$newArr); 	
		  }catch (PDOException $e) { parent::close();
		  die("Connection failed in getCompleteJobs: " . $e->getMessage());
	        }	

	}
	private function getUserProfileInfo($userId){
		 try{ 
			$arr=array();
			$getuserS=$this->_connection->query("select * from userRegistration where userId='".$userId."'  ");
			$rOw=$getuserS->fetch(PDO::FETCH_OBJ);
			$arr['userId']=$rOw->userUniqueId; 
			$arr['userName']=$rOw->userName;
			$arr['image']=$rOw->userImage;
			$arr['userNumber']=$rOw->userMobile;
			$arr['userEmail']=$rOw->userEmail;
			$getCarInfo=$this->getUsercarInfo($userId);
			$arr['carImage']=$getCarInfo['carImage'];
			return $arr;
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in getopenServices: " . $e->getMessage());
	        }	
	}
          private function getUsercarInfo($userId){
		$arr=array();
		$getuserS=$this->_connection->query("select * from userVehicleInfo where userId='".$userId."'  ");
		$rOw=$getuserS->fetch(PDO::FETCH_OBJ);
		$arr['carImage']=$rOw->uvImage;
		return $arr;
	}
	private function getServiceTypeCharge($serviceId){     // Get Service Charge
		 try{ 
			$getSerivce=$this->_connection->query("select * from services where id='".$serviceId."'  ");
			$rOw=$getSerivce->fetch(PDO::FETCH_OBJ);
			return $rOw->service_charges;
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in getopenServices: " . $e->getMessage());
	        }	
	}
		

	public function updateCustomerInfo($userId,$userName,$userMobile,$userImage,$usercarImage,$usercarModel,$userlicenseNo,$userCarYear,$address1,$address2,$state,$city,$postcode,$carMake){    // Update Customer Information
		 try{ 
		
			$uSERId=$this->getUserPrimaryId($userId);    // call the check UserPrimary function
			
			if(!empty($userImage)){
				$getInformation=$this->_connection->prepare("SELECT userImage from userRegistration WHERE userId='".$uSERId."' ");
				$getInformation->execute();
				$rOw=$getInformation->fetch(PDO::FETCH_OBJ);
				$userOldImage=$rOw->userImage;
				$this->_connection->query("update  userRegistration set userImage='".$userImage."' where userId='".$uSERId."' ");
				if(file_exists($userOldImage)) {
				    unlink($userOldImage);
				}
			} 
			if(!empty($usercarImage)){
				$getCarImage=$this->_connection->prepare("SELECT uvImage from userVehicleInfo WHERE userId='".$uSERId."' ");
				$getCarImage->execute();
				$rOw=$getCarImage->fetch(PDO::FETCH_OBJ);
				$carOldImage=$rOw->uvImage;
				$this->_connection->query("update  userVehicleInfo set uvImage='".$usercarImage."' where userId='".$uSERId."' ");
				if(file_exists($carOldImage)) {
				    unlink($carOldImage);
				}
			} 
			$this->_connection->query("update  userRegistration set userName='".$userName."',userMobile='".$userMobile."',address1='".$address1."',address2='".$address2."',state='".$state."',city='".$city."',zipCode='".$postcode."' where userId='".$uSERId."' ");
			
			$this->_connection->query("update  userVehicleInfo set uvModel='".$usercarModel."',uvYear='".$userCarYear."',uvLicenseNo='".$userlicenseNo."',uvMake='".$carMake."' where userId='".$uSERId."' ");
			parent::close();
				return array('status'=>$this->_success,'image'=>$userImage,'carImage'=>$usercarImage,'message'=>$this->_profileUpdate);
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in updateCustomerInfo: " . $e->getMessage());
	        }		

	} 

	public function updateServiceInfo($userId,$userName,$userMobile,$userImage,$usercarImage,$usercarModel,$userlicenseNo,$userCarYear,$address1,$address2,$state,$city,$postcode){     // Update Service Provider Information
		 try{ 
			
			$uSERId=$this->getServiceProvidePrimaryId($userId); // call the check ServiceProvider Primary function
			
			if(!empty($userImage)){
				$getInformation=$this->_connection->prepare("SELECT spImage from spRegistration WHERE spId='".$uSERId."' ");
				$getInformation->execute();
				$rOw=$getInformation->fetch(PDO::FETCH_OBJ);
				$userOldImage=$rOw->spImage;
				$this->_connection->query("update  spRegistration set spImage='".$userImage."' where spId='".$uSERId."' ");
				if(file_exists($userOldImage)) {
				    unlink($userOldImage);
				}
			} 
			if(!empty($usercarImage)){
				$getCarImage=$this->_connection->prepare("SELECT spVhImage from spVehicleInfo WHERE spId='".$uSERId."' ");
				$getCarImage->execute();
				$rOw=$getCarImage->fetch(PDO::FETCH_OBJ);
				$carOldImage=$rOw->spVhImage;
				$this->_connection->query("update  spVehicleInfo set spVhImage='".$usercarImage."' where spId='".$uSERId."' ");
				if(file_exists($carOldImage)) {
				    unlink($carOldImage);
				}
			} 
			
			$this->_connection->query("update  spRegistration set spUserName='".$userName."',spMobile='".$userMobile."',spAddress1='".$address1."',spAddress2='".$address2."',spState='".$state."',spCity='".$city."',spZipCode='".$postcode."' where spId='".$uSERId."' ");
			$this->_connection->query("update  spVehicleInfo set spVhModelNo='".$usercarModel."',spVhYear='".$userCarYear."',spVhLicenseNo='".$userlicenseNo."' where spId='".$uSERId."' ");

			//parent::close();
				return array('status'=>$this->_success,'image'=>$userImage,'carImage'=>$usercarImage,'message'=>$this->_profileUpdate);
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in updateServiceInfo: " . $e->getMessage());
	        }	

	}



	public function userLogout($userId){             //customer Logout
		 try{ 
			$uSERId=$this->getUserPrimaryId($userId);    // call the check UserPrimary function
			$this->_connection->query("update  userRegistration set onlineStatus='0',deviceToken='404' where userId='".$uSERId."' ");	
			$this->_connection->query("update servicerequest set status='0' where userId='$uSERId' ");
			return array('status'=>$this->_success,'message'=>$this->_logout);
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in updateServiceInfo: " . $e->getMessage());
	        }	
	}


	public function spLogout($userId){         //Service Provider Logout
		 try{ 
			$uSERId=$this->getServiceProvidePrimaryId($userId);    // call the check ServiceProvider function _logout
			$this->_connection->query("update  spRegistration set onlineStatus='0',spDeviceToken='404' where spId='".$uSERId."' ");
			return array('status'=>$this->_success,'message'=>$this->_logout);
		parent::close();
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in updateServiceInfo: " . $e->getMessage());
	        }	

	}

	public function forgetPassword($userEmail){                                  // Forget Passwod and reset Password function 
			try{ 
			$chkUsertable=$this->_connection->prepare("SELECT userId from userRegistration WHERE userEmail='".$userEmail."'");
			$chkUsertable->execute();
			$count = $chkUsertable->rowCount();
			if($count==1){ 
				$newPass=time();
				$encptPass=$this->encrypt($newPass); 
				$this->_connection->query("update userRegistration set userPassword='".$encptPass."' where userEmail='".$userEmail."' ");   // update new passwd
				$obJEMail=new Emails();
				$response=$obJEMail->forgetPassword($userEmail,$newPass);    // send email
				return array('status'=>$this->_success,'message'=>$this->_chkEmail);
			}
			$chkSptable=$this->_connection->prepare("SELECT spId from spRegistration WHERE spEmail='".$userEmail."'");
			$chkSptable->execute(); 
			$countT = $chkSptable->rowCount();  
			if($countT==1){
				$newPass=time();
				$encptPass=$this->encrypt($newPass); 
				$this->_connection->query("update spRegistration set spPassword='".$encptPass."' where spEmail='".$userEmail."' ");  // update new passwd
				$obJEMail=new Emails();
				$response=$obJEMail->forgetPassword($userEmail,$newPass); // send email
				return array('status'=>$this->_success,'message'=>$this->_chkEmail);
			}
			 return array('status'=>$this->_error,'message'=>$this->_emailNotExist);
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in updateServiceInfo: " . $e->getMessage());
	        }	
 	}
	
	public function  spUpdateLocation($userId,$lat,$long){                               // Update Service Provider Location
			try{ 
				$uSERId=$this->getServiceProvidePrimaryId($userId);
				$this->_connection->query("update  spRegistration set spLattitude='".$lat."',spLongitude='".$long."' where spId='".$uSERId."' ");
			return array('status'=>$this->_success);
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in spUpdateLocation: " . $e->getMessage());
	        }	
	}

	public function serviceRequest($userId,$serviceType,$lattitude,$longitude){     // user Add Service Request
			 try{ 
				$uSERId=$this->getUserPrimaryId($userId);   
				$queryRes=$this->_connection->query("SELECT * from serviceRequest WHERE userId='".$uSERId."' AND (status=1 || status=3 )");
				$count = $queryRes->rowCount();
				if($count==1){
					$rOw=$queryRes->fetch(PDO::FETCH_OBJ);	
					$reQuesId=(string)$rOw->serviceId;
					return array('status'=>$this->_error,'message'=>$this->_alreadyServiceRequest,'preServiceRequestId'=>$reQuesId); 
				}
$this->_connection->query("insert into  serviceRequest (userId,serviceType,lattitude,longitude,createDate) values ('".$uSERId."','".$serviceType."','".$lattitude."','".$longitude."','".date("Y-m-d h:i:s")."')");
			
			 switch($serviceType){
			  case 1:
			    $mEssage="A tire service request has been raised within your 10 mile radius";
				break;
			  case 2:
			      $mEssage="A battery service request has been raised within your 10 mile radius";
				break;
			  case 3:
		               $mEssage="A gas service request has been raised within your 10 mile radius";
				break;
			}
			
			 $lastInsert=$this->_connection->lastInsertId(); 
			 
			$type=1;
			$query=$this->_connection->query("insert into service_location_info (sId,longitude,latitude,userType)values(' $lastInsert','$longitude','$lattitude','$type')");	
			 
			$this->searchspandfind($serviceType,$mEssage,$lattitude,$longitude);
return array('status'=>$this->_success,'message'=>$this->_ariseTicket,'serviceRequestId'=>$lastInsert);
			
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in serviceRequest: " . $e->getMessage());
	        }	
	}

	 
	public function serviceRequestCancel($userId,$serviceRequestId){  // user Cancel Request
		 try{ 
		$uSERId=$this->getUserPrimaryId($userId);   
		$queryRes=$this->_connection->query("SELECT * from serviceRequest WHERE userId='".$uSERId."' AND serviceId='".$serviceRequestId."' AND status!='4' AND status!='0' AND status!='2'  ");
		$count = $queryRes->rowCount();
		if($count==1){
			$rOw=$queryRes->fetch(PDO::FETCH_OBJ);	
			$sTatus=$rOw->status;
			$Job=$rOw->serviceType;
			if($sTatus==3 || $sTatus==1){  
					//send Status
				$getRespnseRow=$this->_connection->query("SELECT * from serviceRespons WHERE userId='".$uSERId."' AND serviceId='".$serviceRequestId."' ");	
				while($getrOw=$getRespnseRow->fetch(PDO::FETCH_OBJ)){
					$spId=$getrOw->spId;
					// sp Send notification 
					$this->jobCancelSpNotification($uSERId,$serviceRequestId);

				}
			$this->_connection->query("update  serviceRespons set srStatus='0' WHERE userId='".$uSERId."' AND serviceId='".$serviceRequestId."' ");	
			//$this->_connection->query("DELETE FROM serviceRespons WHERE userId='".$uSERId."' AND serviceId='".$serviceRequestId."' ");			
			 }
			$this->_connection->query("update serviceRequest set updatedate='".date("Y-m-d h:i:s")."',status='0' where userId='".$uSERId."' AND serviceId='".$serviceRequestId."' ");
			$this->jobCancelByCustomer($Job);
			return array('status'=>$this->_success);
		}else{
			return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
		}
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in serviceRequest: " . $e->getMessage());
	        }	
	}
	private function searchspandfind($type,$message,$lattitude,$longitude){
		 try{
			
				//$userInfo=$this->_connection->query("select device_info.fcm_key from device_info inner join spregistration on spregistration.spDeviceToken=device_info.token WHERE spregistration.sp_availability=0 and spregistration.services like '%$type%'");
				
				$userInfo=$this->_connection->query("select spregistration.spId,device_info.fcm_key,( 3959 * acos( cos( radians('$lattitude') ) * cos( radians( spregistration.spLattitude ) ) * cos( radians( spregistration.spLongitude ) - radians('$longitude') ) + sin( radians('$lattitude') ) * sin( radians( spregistration.spLattitude ) ) ) ) AS distance from spregistration inner join device_info on device_info.token=spregistration.spDeviceToken where spregistration.sp_availability='0' and spregistration.services like '%$type%' HAVING distance <'10'");
				
				
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
				
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in searchspandfind: " . $e->getMessage());
	        }	
	}
	
	private function userSendNotificationafterSpAccepteed($userId,$serviceId){ //private
		 try{
				$userInfo=$this->_connection->query("SELECT device_info.fcm_key FROM device_info INNER JOIN userregistration on userregistration.deviceToken=device_info.token WHERE userregistration.userId='$userId' ");
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
					$key[]=$roW->fcm_key;
				}
				$message="Your service request has been accepted by a service person.";
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in userSendNotificationafterSpAccepteed: " . $e->getMessage());
	        }	
	}
 	public function accepetedJob($userId,$serviceRequestId,$lAtT,$lONG){  // job accepted by Sp
		 try{ 
			$uSERId=$this->getServiceProvidePrimaryId($userId);
			$maxMile=$this->getMaxMile();
		$getmargin=$this->_connection->query("select admin_margin from app_settings where id='1' ");
		$getRowMargin=$getmargin->fetch(PDO::FETCH_OBJ);	
$queryResS=$this->_connection->query("select *,( 3959 * acos( cos( radians('".$lAtT."') ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians('".$lONG."') ) + sin( radians('".$lAtT."') ) * sin( radians( lattitude ) ) ) ) AS distance from serviceRequest where status='0' AND  serviceId='".$serviceRequestId."' HAVING distance <'".$maxMile['radious']."' ORDER BY serviceId Desc ");
 $countT = $queryResS->rowCount();	
				if($countT==1){
					return array('status'=>$this->_error,'message'=>$this->_cancelByUser);
				}

$queryRes=$this->_connection->query("select *,( 3959 * acos( cos( radians('".$lAtT."') ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians('".$lONG."') ) + sin( radians('".$lAtT."') ) * sin( radians( lattitude ) ) ) ) AS distance from serviceRequest where status='1' AND  serviceId='".$serviceRequestId."' HAVING distance <='".$maxMile['radious']."' ORDER BY serviceId Desc ");
			 $count = $queryRes->rowCount();
			if($count>0){
			$rOw=$queryRes->fetch(PDO::FETCH_OBJ);	
				if($rOw->status==0){
					return array('status'=>$this->_error,'message'=>$this->_cancelByUser);
					exit;
				}
				if($rOw->status==3){
				return array('status'=>$this->_error,'message'=>$this->_approvedByOther);
					exit;

				}

		$getCheckSp=$this->_connection->query("select * from serviceRespons where spId='".$uSERId."' AND serviceId='".$serviceRequestId."' AND srStatus!='0'  AND srStatus!='3'  ");
		 $rOwCount = $getCheckSp->rowCount();
		  if($rOwCount==0){
			
			$distance=$rOw->distance;
			if($distance<=1){ $distance=1; }
			$distanceCost=$distance*$maxMile['travel_cost'];
			$servicecost=number_format($this->getServiceTypeCharge($rOw->serviceType),2, '.', '');
			$margInN=number_format(($servicecost*$getRowMargin->admin_margin)/100,2, '.', '');
		          $after=number_format($servicecost-$margInN,2, '.', '');  // Service Cost After Margin
 			$totalcostSp=number_format($after+$distanceCost,2, '.', ''); // total Cost ( After Margin + Distance)
			$tOtalCost=number_format($servicecost+$distanceCost,2, '.', '');   // OverAll Cost
			$afterPercentCost=$after;   // Service Cost After Margin
			
			  $this->_connection->query("insert into serviceRespons (userId, spId, travelCost, serviceCost, totalCost, lattitude, longitude, dateTime,serviceId,afterPercentCost,totalcostSp) values('".$rOw->userId."','".$uSERId."','".$maxMile['travel_cost']."','".$servicecost."','".$tOtalCost."','".$lAtT."','".$lONG."','".date("Y-m-d h:i:s")."','".$serviceRequestId."','".$afterPercentCost."','".$totalcostSp."')");
		
$lastInsert=$this->_connection->lastInsertId(); 

$type=0;
$query=$this->_connection->query("insert into service_location_info (sId,longitude,latitude,userType)values('$lastInsert','$lONG','$lAtT','$type')");


			$this->userSendNotificationafterSpAccepteed($rOw->userId,$lastInsert);
				return array('status'=>$this->_success,'message'=>$this->_waitforUser,'serviceResponseId'=>$lastInsert);	
			
		}
			return array('status'=>$this->_error,'message'=>$this->_alreadyJObsubmit);	
					
	          	}else{
				return array('status'=>$this->_error,'message'=>$this->_anotherChk);
			}
			
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in serviceRequest: " . $e->getMessage());
	        }	
	}
	
	public function spcheckStatusUserAccepted($userId,$serviceResponseId){   // Check User Accepted or Rejected  Response by Sp
		 try{ 
				$uSERId=$this->getServiceProvidePrimaryId($userId);
				$queryRes=$this->_connection->query("SELECT * FROM serviceRespons where spId='".$uSERId."'  AND srId='".$serviceResponseId."' ");
				$count = $queryRes->rowCount();
			if($count==1){
				$rOw=$queryRes->fetch(PDO::FETCH_OBJ);	
				return array('status'=>$this->_success,'respnoseStatus'=>$rOw->srStatus);  
			}else{
				return array('status'=>$this->_error,'message'=>$this->_unexceptionError);	
			}
				return array('status'=>$this->_error,'message'=>$this->_unexceptionError);		
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in spcheckStatusUserAccepted: " . $e->getMessage());
	        }	
	}

	public function  spCancelpendingJob($userId,$serviceResponseId){   //cancel by Sp
		try{ 
			$uSERId=$this->getServiceProvidePrimaryId($userId);	
			
			$queryRes=$this->_connection->query("SELECT * FROM serviceRespons where spId='".$uSERId."'  AND srId='".$serviceResponseId."' AND (srStatus='1' OR srStatus='2') ");
				$count = $queryRes->rowCount();
				
			if($count==1){
				$rOw=$queryRes->fetch(PDO::FETCH_OBJ);	
				$this->_connection->query("update serviceRespons set srStatus='3' where spId='".$uSERId."'  AND srId='".$serviceResponseId."' ");
			//	$this->_connection->query("update serviceRequest set status='2' where serviceId='".$rOw->serviceId."'");
				$this->_connection->query("update spRegistration set sp_availability='0' where spId='".$uSERId."'");	

				// cancel Notification   
				$this->jobCAncelBySpNOtification($rOw->userId);
				return array('status'=>$this->_success,'message'=>$this->_canceljob);
			}else{
				return array('status'=>$this->_error,'message'=>$this->_unexceptionError);	
			}
				return array('status'=>$this->_error,'message'=>$this->_unexceptionError);		
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in spcheckStatusUserAccepted: " . $e->getMessage());
	        }	
	}
	
	
	private function jobCAncelBySpNOtification($userId){
		 try{
				
				$userInfo=$this->_connection->query("SELECT device_info.fcm_key FROM device_info INNER JOIN userregistration on userregistration.deviceToken=device_info.token WHERE userregistration.userId='$userId' ");
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
					$key[]=$roW->fcm_key;
				}
				$message="Job has been Cancelled by Service Provider";
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
				
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in jobCAncelBySpNOtification: " . $e->getMessage());
	        }	
	}

	private function getSpProfile($userId){
			$queryVehicleInfo=$this->_connection->prepare("select * from spVehicleInfo where spId='".$userId."'");
			$queryRest=$this->_connection->prepare("select * from spRegistration where spId='".$userId."'");
			$queryRest->execute();
			$queryVehicleInfo->execute();
			$count = $queryVehicleInfo->rowCount();
			if($count==1){
			$getRowW=$queryVehicleInfo->fetch(PDO::FETCH_OBJ);
			$getRow=$queryRest->fetch(PDO::FETCH_OBJ);
			  $arr['userId']=$getRow->spUniqeId;
			  $arr['userName']=$getRow->spUserName;
			  $arr['userNumber']=$getRow->spMobile; 
			  $arr['image']=$getRow->spImage;
			  $arr['userEmail']=$getRow->spEmail;
			  $arr['carImage']=$getRowW->spVhImage;
			  return $arr;
			} return array();	 
	}

	public function customerGetJobInfo($userId,$serviceRequestId){   // GEt Customer job response
				try{ 
			    $uSERId=$this->getUserPrimaryId($userId);
			 $getBalance=$this->_connection->query("select userWallet from  userRegistration where userId='".$uSERId."' ");
			 $rowwallet=$getBalance->fetch(PDO::FETCH_OBJ);
			       $queryRest=$this->_connection->prepare("select *,( 3959 * acos( cos( radians(:lAt) ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(:lOnG) ) + sin( radians(:lAt) ) * sin( radians( lattitude ) ) ) ) AS distance from serviceRespons where  userId=:usERId AND serviceId=:sERviceId AND srStatus='1'  ");
			    $queryRes=$this->_connection->query("SELECT * FROM serviceRequest where userId='".$uSERId."' AND serviceId='".$serviceRequestId."' AND status='1'  ");
			    $count = $queryRes->rowCount();
			    if($count>0){
					$rOw=$queryRes->fetch(PDO::FETCH_OBJ);
				 			$arr['service_type']=$rOw->serviceType;
							$stAtus=$rOw->status;
							$arr['serviceStatus']=$stAtus;	
						if($stAtus==1){
					$queryRest->bindParam(':lAt',$barHotelId);
					$queryRest->bindParam(':lOnG',$barPlaceId);
					$queryRest->bindParam(':usERId',$uSERId);
					$queryRest->bindParam(':sERviceId',$serviceRequestId);
					$queryRest->execute();	
				while($getRow=$queryRest->fetch(PDO::FETCH_OBJ)){
 							$Arr['serviceResponseId']=(string)$getRow->srId; 
							$Arr['servicecost']=(string)number_format($getRow->serviceCost,2, '.', ''); 
							$Arr['service_type']=$rOw->serviceType;
							$diStance=$getRow->distance;
							if($diStance==0){$diStance=1; }
							$Arr['travelcost']=(string)number_format($getRow->travelCost*$diStance,2, '.', '');  
							$Arr['totalcost']=(string)$getRow->totalCost;  
							$Arr['respnoseStatus']=(string)$getRow->srStatus;    
							$Arr['distance']=(string)number_format($getRow->distance,2, '.', '');
							$Arr['timeAgo']=strtotime($getRow->dateTime)-strtotime(date('Y-m-d h:i:s'));  
							$Arr['spInfo']=$this->getSpProfile($getRow->spId);
							$Arr['wAllet']=(string)$rowwallet->userWallet;
							$newArr[]=$Arr;
				}  if(!$newArr){$newArr=array();}	 $arr['jobInfo']=$newArr;
						}else{
							 $arr['jobInfo']=array();	
						}
				           return array('status'=>$this->_success,'getJobsResponse'=>$arr);
				}
					return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
				}catch (PDOException $e) { parent::close();
		  die("Connection failed in customerGetInfo: " . $e->getMessage());
	        }	
	}
	public function rejectedJobs($userId,$serviceResponseId){            // After user Accepted ticket to  reject other tickets
			try{ 
 		 $queryRes=$this->_connection->query("SELECT * FROM serviceRespons where userId='".$userId."' AND  srStatus='1' AND srId='".$serviceResponseId."'");      					$cOuNt = $queryRes->rowCount();
			    if($cOuNt>0){
				$getRow=$queryRes->fetch(PDO::FETCH_OBJ);
				$spId=$getRow->spId;
				//send rejected notification
				$this->_connection->query("update serviceRespons set srStatus='0' where userId='".$uSERId."'  AND srId='".$serviceResponseId."' ");
				$this->jobCancelSpNotification($spId,$serviceResponseId);
				
			}												
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in rejectedJobs: " . $e->getMessage());
	        }	
	}

	  public function jobCancelSpNotificationAfterPayment($spId,$serviceId){ //private
		 try{
				
				$userInfo=$this->_connection->query("select device_info.fcm_key from device_info inner join spregistration on spregistration.spDeviceToken=device_info.token WHERE spregistration.spId='$spId'");
				
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				
				$message="'Customer has cancelled this request. Travel Cost will be paid to you";
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
					//$appObj=new Device_Notification();
 					//$respOnse=$appObj->sendPushpopup($deIceTokEN,'Customer has cancelled this request. Travel Cost will be paid to you.','cancelByUser',$serviceId);
		 		
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in jobCancelSpNotification: " . $e->getMessage());
	        }	
	}
	public function jobCancelSpNotification($spId,$serviceId){   //private
		 try{
			 
			 $userInfo=$this->_connection->query("select device_info.fcm_key from device_info inner join spregistration on spregistration.spDeviceToken=device_info.token WHERE spregistration.spId='$spId'");
				
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				
				$message="Your job has been rejected by the customer.  Apply for a different job";
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
				/*$userInfo=$this->_connection->query("select * from spRegistration where spId='".$spId."' ");
				
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
					$deIceType=$roW->spDeviceType;
					$deIceTokEN=$roW->spDeviceToken;
					if($deIceType=='Andriod'){
						
					}else{
					//$appObj=new Device_Notification();
 					//$respOnse=$appObj->sendPushpopup($deIceTokEN,'Your job has been rejected by the customer.  Apply for a different job.','cancelByUser',$serviceId);
		 			}
				}*/
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in jobCancelSpNotification: " . $e->getMessage());
	        }	
	}

	public function acceptedJobByUserNotification($spId,$serviceId,$message){ //private
			try{
				
				 $userInfo=$this->_connection->query("select device_info.fcm_key from device_info inner join spregistration on spregistration.spDeviceToken=device_info.token WHERE spregistration.spId='$spId'");
				
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
				
				
				/*$userInfo=$this->_connection->query("select * from spRegistration where spId='".$spId."' ");
				$roW=$userInfo->fetch(PDO::FETCH_OBJ);
				$deIceType=$roW->spDeviceType;
					$deIceTokEN=$roW->spDeviceToken;
					if($deIceType=='Andriod'){
						
					}else{
					//$appObj=new Device_Notification();
 					//$respOnse=$appObj->sendPushpopup($deIceTokEN,$message,'acceptedByUser',$serviceId);
		 		}*/
		
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in acceptedJobByUserNotification: " . $e->getMessage());
	        }	

	}

	public function customerAcceptedSpJob($userId,$serviceResponseId,$stripeToken,$userBallance){   // Customer Accepted Job and make payment
				try{ 
				 require(__ROOT__.'/stripe/init.php');	
			    $uSERId=$this->getUserPrimaryId($userId);
		 	  $queryRes=$this->_connection->query("SELECT * FROM serviceRespons where userId='".$uSERId."' AND  srStatus='1' AND srId='".$serviceResponseId."' ");
			   $cOuNt = $queryRes->rowCount();
			    if($cOuNt>0){
				$getRow=$queryRes->fetch(PDO::FETCH_OBJ);
				$roWCount=$this->checkServiceProvideAviality($getRow->spId);
				$this->_connection->beginTransaction();
				if($roWCount==1){
				 $amOunt=number_format($getRow->totalCost,2, '.', '');
				if($userBallance>=$amOunt){
						//noting to do 
			
				}else{
						 
				       $newAmOunt=number_format($amOunt-$userBallance,2, '.', '');
				      \Stripe\Stripe::setApiKey($this->stripeKey);
			                 $charge =\Stripe\Charge::create(array(
				 		"amount" => $newAmOunt*100,
						"currency" => "usd",
						"source" =>$stripeToken,
						"description" => "Payment from qoda"
					)); 
				  $this->_connection->query("update userRegistration set userWallet=userWallet+$newAmOunt where userId='".$uSERId."' ");	
						
				}	
				$this->_connection->query("update serviceRequest set updatedate='".date("Y-m-d h:i:s")."',status='3' where  serviceId='".$getRow->serviceId."'");
				$this->_connection->query("update spRegistration set sp_availability='1' where spId='".$getRow->spId."'"); 
				$this->_connection->query("update serviceRespons set srStatus='2' where userId='".$uSERId."'  AND srId='".$serviceResponseId."' ");
				
				$this->_connection->commit();
				$getRowW=$this->_connection->query("select serviceType from serviceRequest where  serviceId='".$getRow->serviceId."' ");
				$gettRow=$getRowW->fetch(PDO::FETCH_OBJ);
				$this->informAllRemainingSP($getRow->serviceId);
				 switch($gettRow->serviceType){
				  case 1:
				    $mEssage="Your request for the job of Tire has been Accepted by the customer.";
					break;
				  case 2:
				      $mEssage="Your request for the job of Battery has been Accepted by the customer.";
					
					break;
				  case 3:
				      $mEssage="Your request for the job of Gas has been Accepted by the customer.";
					break;
			}
					$this->acceptedJobByUserNotification($getRow->spId,$serviceResponseId,$mEssage);	
					
					$this->rejectedJobs($uSERId,$serviceResponseId);
				 return array('status'=>$this->_success);	
				}else{
					return array('status'=>$this->_error,'message'=>'Service Provider is currently not available');
				}
			  }
					return array('status'=>$this->_error,'message'=>$this->_jobCancelBySp);
			}catch(Stripe_CardError $e) {
					$arr=array('error'=>1,'success'=>0,'message'=>"Card errors are the most common type of error you should expect to handle. They result when the user enters a card that can't be charged for some reason.");	
							return $arr;
				} catch (Stripe_Error $e) {
					$arr=array('status'=>0,'message'=>$e->getMessage());		
					return $arr;
					
				}catch (PDOException $e) { parent::close();
		  die("Connection failed in customerAcceptedSpJob: " . $e->getMessage());
	        }	
	}
	private function checkServiceProvideAviality($userId){   //   check Service Provide Aviality
		try{ 
				$getquerS=$this->_connection->query("select * from spRegistration where spId='$userId' AND sp_availability='0' ");
				 return $cOuNt = $getquerS->rowCount();
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in checkServiceProvideBusyOrnot: " . $e->getMessage());
	        }	
	}
	public function checkServiceProvideBusyOrnot($userId){   // check 
		try{ 
				$uSERId=$this->getServiceProvidePrimaryId($userId);
				$getquerS=$this->_connection->query("select * from spRegistration where spId='$uSERId' AND sp_availability='0' ");
				$cOuNt = $getquerS->rowCount();
					return array('status'=>$this->_success,'available'=>(string)$cOuNt);

		}catch (PDOException $e) { parent::close();
		  die("Connection failed in checkServiceProvideBusyOrnot: " . $e->getMessage());
	        }	
	}
	public function customerRejectedJobparticular($userId,$serviceResponseId){      // Customer REjected Job
			try{ 
			  $uSERId=$this->getUserPrimaryId($userId);	
			   $queryRes=$this->_connection->query("SELECT * FROM serviceRespons where userId='".$uSERId."' AND  srId='".$serviceResponseId."' ");
			   $cOuNt = $queryRes->rowCount();
			    if($cOuNt>0){
				  $getRow=$queryRes->fetch(PDO::FETCH_OBJ);
				  $sPid=$getRow->spId;
					
				 $tRavlingCost=$getRow->travelCost;
				 $queryResS=$this->_connection->query("SELECT * FROM serviceRespons where userId='".$uSERId."' AND  srId='".$serviceResponseId."' AND  srStatus='2'");
	              		 $cOuNtT = $queryResS->rowCount();
				 if($cOuNtT==1){
					$this->_connection->query("update spRegistration set   sp_availability='0',spWallet=spWallet+$tRavlingCost   where spId='".$sPid."' ");
					$this->_connection->query("update userRegistration set userWallet=userWallet-$tRavlingCost where userId='".$uSERId."'");	
					//send notification sfs
					$this->jobCancelSpNotificationAfterPayment($sPid,$serviceResponseId);
				 }else{
					//send notification 
					$this->jobCancelSpNotification($sPid,$serviceResponseId);
				}
 				 $this->_connection->query("update  serviceRespons  set srStatus='0' where userId='".$uSERId."' AND  srId='".$serviceResponseId."'  ");
					return array('status'=>$this->_success);
			} 	return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in customerRejectedJobBefore: " . $e->getMessage());
	        }	
	}
	private function chcklocationnearby($lat,$long,$serviceId,$userId){
			try{
	$queryRest=$this->_connection->prepare("select *,( 3959 * acos( cos( radians(:lAT) ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians(:lONG) ) + sin( radians(:lAT) ) * sin( radians( lattitude ) ) ) ) AS distance from serviceRequest where  userId=:usERId AND  serviceId=:serViceId AND sendNotificaion='0' ");
			                    $queryRest->bindParam(':lAT',$lat);
					$queryRest->bindParam(':lONG',$long);
					$queryRest->bindParam(':usERId',$userId);
					$queryRest->bindParam(':serViceId',$serviceId);
					$queryRest->execute();
				 $cOuNt = $queryRest->rowCount();
				if($cOuNt>0){	
				 $getRow=$queryRest->fetch(PDO::FETCH_OBJ);
				 $distance=$getRow->distance;	
				if($distance<=1){
					
					//$userInfo=$this->_connection->query("select device_info.fcm_key from device_info inner join userregistration on userregistration.deviceToken=device_info.token WHERE userregistration.userId='$userId' and serviceId='$serviceId' and sendNotificaion='0'");
					
					$userInfo=$this->_connection->query("select device_info.fcm_key from device_info inner join userregistration on userregistration.deviceToken=device_info.token inner join servicerequest on servicerequest.userId=userregistration.userId WHERE userregistration.userId='$userId' and servicerequest.serviceId='$serviceId' and servicerequest.sendNotificaion='0'");
					
				$message="Service Provider is less than 1 mile away";
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				
				$this->_connection->query("update servicerequest set sendNotificaion=1 where serviceId='$serviceId' and userId='$userId'");
				
				return $res;
				
				
					/*userInfo=$this->_connection->query("select * from userRegistration where userId='$userId' ");
				          $roW=$userInfo->fetch(PDO::FETCH_OBJ);
					$deIceType=$roW->devcieType;
					$deIceTokEN=$roW->deviceToken;
					if($deIceType=='Andriod'){
						
					}else{
					//$appObj=new Device_Notification();
 					//$respOnse=$appObj->sendPushpopup($deIceTokEN,'Service Provider is less than 1 mile away','nearBy',$serviceId);
		 			}*/
				 $this->_connection->query("update serviceRequest set sendNotificaion='1' where userId='$userId' AND serviceId='".$serviceId."' ");
				}
				return 1;
			}
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in customerGetLocationSp: " . $e->getMessage());
	        }		

	}

	public function customerGetLocationSp($userId,$serviceResponseId){    // Customer GEt Location Sp
			try{ 

				 $queryResSp=$this->_connection->prepare("SELECT spLattitude,spLongitude FROM spRegistration where spId=:spUserId");
			 	 $uSERId=$this->getUserPrimaryId($userId);	
			           $queryResS=$this->_connection->query("SELECT spId,srStatus,isCompleted,serviceId FROM serviceRespons where userId='".$uSERId."' AND  srId='".$serviceResponseId."' ");
				 $getRow=$queryResS->fetch(PDO::FETCH_OBJ);
    				 $queryResSp->bindParam(':spUserId',$getRow->spId);
				 $queryResSp->execute(); 	
				   $getRowW=$queryResSp->fetch(PDO::FETCH_OBJ);
				$this->chcklocationnearby($getRowW->spLattitude,$getRowW->spLongitude,$getRow->serviceId,$uSERId);
				return array('status'=>$this->_success,'latitude'=>(string)$getRowW->spLattitude,'longitude'=>(string)$getRowW->spLongitude,'jobStatus'=>(string)$getRow->srStatus,'isCompleted'=>(string)$getRow->isCompleted);
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in customerGetLocationSp: " . $e->getMessage());
	        }		
	}

	public function spGetStatusJob($userId,$serviceResponseId){  //  Sp get job Status
		try{ 
			$uSERId=$this->getServiceProvidePrimaryId($userId);
			$queryRes=$this->_connection->query("SELECT srStatus,isCompleted FROM serviceRespons where spId='".$uSERId."' AND  srId='".$serviceResponseId."' ");
			$getRow=$queryRes->fetch(PDO::FETCH_OBJ);	
			return array('status'=>$this->_success,'jobStatus'=>(string)$getRow->srStatus,'isCompleted'=>(string)$getRow->isCompleted);
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in spGetStatusJob: " . $e->getMessage());
	        }		
	}
	private function getServiceName($serviceType){
			try{ 

		 $getRecords=$this->_connection->query("select service_name from services where id='".$serviceType."'");
		 $getRow=$getRecords->fetch(PDO::FETCH_OBJ);
		 return $getRow->service_name;
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in getServiceName: " . $e->getMessage());
	        }	
	}

	public function spJobcomplete($userId,$serviceResponseId){   // Sp jobComplete 
		try{ 
			$uSERId=$this->getServiceProvidePrimaryId($userId);
                              $getRecords=$this->_connection->prepare("select * from serviceRequest where serviceId=:serViceeId");
			$queryRes=$this->_connection->query("SELECT *, DATE_FORMAT(dateTime,'%b, %d %Y %h:%i %p') as datee FROM serviceRespons where spId='".$uSERId."' AND  srId='".$serviceResponseId."' AND srStatus='2' ");
			$cOuNt = $queryRes->rowCount();
			if($cOuNt==1){
				$getRow=$queryRes->fetch(PDO::FETCH_OBJ);
				$amOunt=$getRow->totalCost;
				$aftermarginCost=$getRow->totalcostSp;
				$customerId=$getRow->userId;
				$serviceReqId=$getRow->serviceId;
				$this->_connection->query("update serviceRequest set status='4', updatedate='".date("Y-m-d h:i:s")."' where userId='".$customerId."' AND  serviceId='".$serviceReqId."' ");
				$this->_connection->query("update serviceRespons set srStatus='4',isCompleted='1' where spId='".$uSERId."' AND  srId='".$serviceResponseId."'  ");
				$this->_connection->query("update spRegistration set spWallet=spWallet+$aftermarginCost,sp_availability='0' where spId='".$uSERId."' ");
				$this->_connection->query("update userRegistration set userWallet=userWallet-$amOunt where userId='".$customerId."' ");

		/*** send mail content start **/
 				$getRecords->bindParam(':serViceeId',$serviceReqId);
				$getRecords->execute(); 
				$rOwGetRecords=$getRecords->fetch(PDO::FETCH_OBJ);
				$deal_lat= $rOwGetRecords->lattitude;
				$deal_long=$rOwGetRecords->longitude;
				$geocode=file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$deal_lat.','.$deal_long.'&sensor=false');

				$output= json_decode($geocode);
				$add1  = $output->results[4]->address_components[0]->long_name.'&nbsp;,';
				$add2 = $output->results[4]->address_components[1]->long_name;
				$fulladdress =$add1 .' '.$add2;
				$ServiceName=$this->getServiceName($rOwGetRecords->serviceType); 
				$customerInfo=$this->getUserProfileInfo($customerId);
				$customerName=$customerInfo['userName'];
				$customerImage=$customerInfo['image'];	
				$customerEmail=$customerInfo['userEmail'];
				$servICeInfo=$this->getSpProfile($uSERId);
				$serviceProviderName=$servICeInfo['userName'];	
				$serviceProviderImage=$servICeInfo['image'];
				$serviceProviderEmail=$servICeInfo['userEmail'];
				$reQuestedDateTime=$rOwGetRecords->createDate;
				$ServiceCost=$getRow->serviceCost;
				$TravelCost=$getRow->travelCost;
				$completeDate=$getRow->datee;
				$totalcost=$getRow->totalCost;
				$marginCost=$getRow->totalcostSp; 
				$ServiceCostAfter=$getRow->afterPercentCost;
                                        $sendEmailObj=new Emails();
				$sendEmailObj->sendmailUserAfterComplete($customerEmail,$serviceProviderImage,$completeDate,$serviceProviderName,$ServiceName,$ServiceCost,$TravelCost,$fulladdress,$totalcost);
				$sendEmailObj->sendmailSpAfterComplete($serviceProviderEmail,$customerImage,$completeDate,$customerName,$ServiceName,$ServiceCostAfter,$TravelCost,$fulladdress,$totalcost,$marginCost);
				/*** send mail content End **/
				// notification 
				$this->jobCompletedBySpNOtification($customerId,$serviceResponseId);
				return array('status'=>$this->_success);
			} 
			return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in spJobcomplete: " . $e->getMessage());
	        }	
	}

	private function jobCancelByCustomer($id)
	{
		try
		{
			$message="A job has been cancelled";
			$userInfo=$this->_connection->query("select device_info.fcm_key FROM device_info INNER JOIN spregistration ON device_info.token=spregistration.spDeviceToken where spregistration.services like '%$id%'");
			
			
			while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
			
		}
		catch(PDOException $e)
		{
			parent::close();
			 die("Connection failed in jobCompletedBySpNOtification: " . $e->getMessage());
		}
	}
	
	private function jobCompletedBySpNOtification($userId,$serviceId){
		 try{
			 				 $userInfo=$this->_connection->query("select device_info.fcm_key from device_info inner join userregistration on userregistration.deviceToken=device_info.token WHERE userregistration.userId='$userId'");
				$message="Your job has been completed";
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
			 
				
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in jobCompletedBySpNOtification: " . $e->getMessage());
	        }	
	}
	public function userGetWallet($userId){
		try{ 
			 $uSERId=$this->getUserPrimaryId($userId);
			 $queryRes=$this->_connection->query("SELECT userWallet FROM userRegistration where userId='".$uSERId."' ");
			 $getRow=$queryRes->fetch(PDO::FETCH_OBJ);
			return array('status'=>$this->_success,'walletAmount'=>$getRow->userWallet);
			
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in userGetWallet: " . $e->getMessage());
	        }	

 	}
 	public function userTermsConditions(){
		try{ 
			  $queryRes=$this->_connection->query("SELECT personcontractfile,personType FROM personcontract ");
			 while($getRow=$queryRes->fetch(PDO::FETCH_OBJ)){
			    $arr['type']=(string)$getRow->personType;
			    $arr['filePath']=(string)$getRow->personcontractfile;
			    $newArr[]=$arr;
			 } 
			return array('status'=>$this->_success,'fileInfo'=>$newArr);
			
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in userTermsConditions: " . $e->getMessage());
	        }	

 	}
	public function spGetWallet($userId){
		try{ 
			 $uSERId=$this->getServiceProvidePrimaryId($userId);
			 $queryRes=$this->_connection->query("SELECT spWallet,spCardId FROM spRegistration where spId='".$uSERId."' ");
			 $getRow=$queryRes->fetch(PDO::FETCH_OBJ);
			return array('status'=>$this->_success,'walletAmount'=>$getRow->spWallet,'recipteId'=>(string)$getRow->spCardId);
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in userGetWallet: " . $e->getMessage());
	        }	
	}

/*	private function  createCardOfSp($spId,$token,$accOuntId){  // create Card Account
		try{ 
		   \Stripe\Stripe::setApiKey($this->stripeKey);
		   $account = \Stripe\Account::retrieve($accOuntId);
		   $account->external_accounts->create(array("external_account" =>$token));
		//  print_r($account);
			$accountCardId=$account['id'];
			$queryRes=$this->_connection->query("update spRegistration spCardId='".$accountCardId."'  where spId='".$spId."' ");
		$queryRes=$this->_connection->query(" ");	
		}catch (Exception $e) {
			 	 $body = $e->getJsonBody();
				  $err  = $body['error'];
				 $mEssAge=$err['message'];
				 return array('status'=>$this->_error,'message'=>$mEssAge);	
		}
	}	*/
	
	public function updatespRecipteid($userId,$fullName,$tokenId){ // add manage Account 
		try{ 		
			 require(__ROOT__.'/stripe/init.php');	
			 $uSERId=$this->getServiceProvidePrimaryId($userId);	
			 $queryRes=$this->_connection->query("SELECT spCardId,spEmail,stripeAccount,spWallet FROM spRegistration where spId='".$uSERId."' ");
			 $getRow=$queryRes->fetch(PDO::FETCH_OBJ);
		   \Stripe\Stripe::setApiKey($this->stripeKey);
		    $account = \Stripe\Account::retrieve($getRow->stripeAccount);
		   $accOunt=$account->external_accounts->create(array("external_account" =>$tokenId));
		   $accountCardId=$accOunt['id'];
			$queryRes=$this->_connection->query("update spRegistration set spCardId='".$accountCardId."'  where spId='".$uSERId."' ");
	 
			return array('status'=>$this->_success,'recipteId'=>$accountCardId);

		}catch (PDOException $e) { parent::close();
		  die("Connection failed in updatespRecipteid: " . $e->getMessage());
	        }catch (Exception $e) {
			 	 $body = $e->getJsonBody();
				  $err  = $body['error'];
				 $mEssAge=$err['message'];
				 return array('status'=>$this->_error,'message'=>$mEssAge);	
		}	
	}

	public function transferMoneytoAccount($userId,$moNey){
		try{ 
			 require(__ROOT__.'/stripe/init.php');	
			 $uSERId=$this->getServiceProvidePrimaryId($userId);	
			 $queryRes=$this->_connection->query("SELECT * FROM spRegistration where spId='".$uSERId."' AND spWallet>='$moNey'  ");	
			 $count = $queryRes->rowCount();	
			 if($count>0){
			 $getRow=$queryRes->fetch(PDO::FETCH_OBJ);
			 
			 \Stripe\Stripe::setApiKey($this->stripeKey);
			 $amount=$getRow->spWallet;
			 $transaction=\Stripe\Transfer::create(array(
			  "amount" => $moNey*100,
			  "currency" => "usd",
			  "destination" => $getRow->stripeAccount,
			  "description" => "Transfer for".$getRow->spEmail
			));
			$transactionId=$transaction['id'];
				
			  $queryRes=$this->_connection->query("INSERT INTO transferHistory (spId, amount, transferStripeId, dateTime) VALUES ('".$uSERId."', '".$moNey."', '".$transactionId."', '".date('Y-m-d h:i:s')."');");	
			  $queryRes=$this->_connection->query("update spRegistration set spWallet=spWallet-$moNey  where spId='".$uSERId."' "); 
			  return array('status'=>$this->_success);					
		
			 }else{
					return array('status'=>$this->_error,'message'=>$this->_amountInsufficient);
			}
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in updatespRecipteid: " . $e->getMessage());
	        }catch (Exception $e) {
			 	 $body = $e->getJsonBody();
				  $err  = $body['error'];
				 $mEssAge=$err['message'];
				 return array('status'=>$this->_error,'message'=>$mEssAge);	
		}	
	}	
	
	public function truncateData(){
	        try{ 
	                $this->_connection->query("TRUNCATE table userRegistration");
	                $this->_connection->query("TRUNCATE table userVehicleInfo");
	                $this->_connection->query("TRUNCATE table transferHistory");
	                $this->_connection->query("TRUNCATE table spVehicleInfo");
	                $this->_connection->query("TRUNCATE table spRegistration");
	                $this->_connection->query("TRUNCATE table serviceRespons");
	                $this->_connection->query("TRUNCATE table serviceRequest");
	                $this->_connection->query("TRUNCATE table rating");
	                $this->_connection->query("TRUNCATE table dispute_cases");
					 $this->_connection->query("TRUNCATE table device_info");
					 $this->_connection->query("TRUNCATE table service_location_info");
					 $this->_connection->query("TRUNCATE table spanswer");
					 
					
	                return array('status'=>$this->_success);
	        }catch (Exception $e) {
			 	 $body = $e->getJsonBody();
				  $err  = $body['error'];
				 $mEssAge=$err['message'];
				 return array('status'=>$this->_error,'message'=>$mEssAge);	
		}
	}

/******  Password Encryption  and Password  Decryption  Start ******/
		
	
	public  function encrypt($plainText)       //Password encryption
	{	
		$key=$this->keySalt;
		$secretKey =$this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
	  	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad =$this->pkcs5_pad($plainText, $blockSize);
	  	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
		{
		      $encryptedText = mcrypt_generic($openMode, $plainPad);
	      	      mcrypt_generic_deinit($openMode);
		 } 
		return bin2hex($encryptedText);
	}
 	public function decrypt($encryptedText)
	{ 
		$key=$this->keySalt;
		$secretKey =$this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText=$this->hextobin($encryptedText);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
	 	mcrypt_generic_deinit($openMode);
		return $decryptedText;
	 }
	//*********** Padding Function *********************

	 function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

	//********** Hexadecimal to Binary function for php 5.6 version ********

	function hextobin($hexString) 
   	 { 
        	$length = strlen($hexString); 
        	$binString="";   
        	$count=0; 
        	while($count<$length) 
        	{       
        	    $subString =substr($hexString,$count,2);           
        	    $packedString = pack("H*",$subString); 
        	    if ($count==0)
		    {
				$binString=$packedString;
		    } 
        	    
		    else 
		    {
				$binString.=$packedString;
		    } 
        	    
		    $count+=2; 
        	} 
  	        return $binString; 
    	  }  
 /******  Password Encryption  and Password  Decryption  End ******/
	 
	
	
public function getSpDetails($sid)
{
	try
	{
		$Query=$this->_connection->query("select spregistration.spUserName,spregistration.spSocialId,spregistration.spEmail,spregistration.spPassword,spregistration.spMobile,spregistration.spAddress1,spregistration.spAddress2,spregistration.spState,spregistration.spCity,spregistration.spZipCode,spregistration.spImage,spvehicleinfo.spVhImage,spvehicleinfo.spVhModelNo,spvehicleinfo.spVhLicenseNo,spvehicleinfo.spVhYear from spregistration
		inner join spvehicleinfo on spregistration.spId=spvehicleinfo.spId  where spregistration.spId='$sid' ");
		$Query->execute();
			$rOw=$Query->fetch(PDO::FETCH_ASSOC);
			return $rOw;	
		
	}
	catch(PDOException $e)
	{
			parent::close();
		  die("Connection failed" . $e->getMessage());
	}
}
	 
public function getCustomerDetails($cid)
{
	try
	{
		$Query=$this->_connection->query("select userregistration.*,uservehicleinfo.* from userregistration inner join uservehicleinfo on userregistration.userId=uservehicleinfo.uvId where userregistration.userId='$cid' ");
		$Query->execute();
			$rOw=$Query->fetch(PDO::FETCH_ASSOC);
			return $rOw;	
	}
	catch(PDOException $e)
	{
		parent::close();
		die("Connection failed" . $e->getMessage());
	}
}

public function updateCustomerPassword($userPassword,$userEmail)
{
	try
	{
		$Query=$this->_connection->query("update userregistration set userPassword='$userPassword' where userEmail='$userEmail' ");
		$Query->execute();
	}
	catch(PDOException $e)
	{
		parent::close();
		die("Connection failed" . $e->getMessage());
	}
}
public function updateSpPassword($userPassword,$userEmail)
{
	try
	{
		$Query=$this->_connection->query("update spregistration set spPassword='$userPassword' where spEmail='$userEmail' ");
		$Query->execute();
	}
	catch(PDOException $e)
	{
		parent::close();
		die("Connection failed" . $e->getMessage());
	}
}

public function updateFcmKey($deviceToken,$fcmkey)
{
	try
	{
		
		$qry=$this->_connection->query("select * from device_info where token='$deviceToken' ");	
		$count=$qry->rowCount();
		if($count>=1)
		{
			
			$update=$this->_connection->query("update device_info set fcm_key='$fcmkey' where token='$deviceToken'");
		}
		else
		{
			
			$insert=$this->_connection->query("insert into device_info (token,fcm_key)values('$deviceToken','$fcmkey')");
		}
	}
	catch(PDOException $e)
	{
		parent::close();
		die("Connection failed" . $e->getMessage());
		
	}
}

public function updateLocationSp($lat,$long,$deviceToken)
{
	try
	{
		$update=$this->_connection->query("update spregistration set spLattitude='$lat',spLongitude='$long' where spDeviceToken='$deviceToken'");
	}
	catch(PDOException $e)
	{
		parent::close();
		die("Connection failed" . $e->getMessage());
	}
}

private function informAllRemainingSP($id)
{
	try
	{
		 $userInfo=$this->_connection->query("SELECT device_info.* FROM device_info INNER JOIN spregistration ON spregistration.spDeviceToken=device_info.token INNER JOIN servicerespons ON servicerespons.spId=spregistration.spId WHERE servicerespons.srStatus != '2' AND servicerespons.serviceId = '$id'");
				$message="This job has been taken by another service provider.Please apply for another job!";
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
				$key[]=	$roW->fcm_key;
				}
				$key=array_unique($key);
				$key=array_filter($key);
				$obj=new firebaseNotification();
				$res=$obj->sentNotification($message,$key);
				return $res;
	}
	catch(PDOException $e)
	{
		parent::close();
		die("Connection failed" . $e->getMessage());
	}
}

}

?>
