<?php
class AppmodelUpdate extends Database{
	
	public function check_Tocken_exist($token)
	{
		try
		{
			$query=$this->_connection->query("select token from device_info where token='$token' ");
			$count = $query->rowCount(); 
			return $count;
		}
		catch(PDOException $e) { parent::close();
		die("Error: " . $e->getMessage());
		
		}
	}
	
	
	public function write_device_info($token,$key)
	{
		try
		{
			$this->_connection->query("insert into device_info(token,fcm_key)values('$token','$key')");
			return true;	
		}
		catch (PDOException $e) { parent::close();
		die("Error: " . $e->getMessage());
		
		}
	
		
	}
	
	
	public function get_service_details()
	{
		try
		{
			$query=$this->_connection->query("select * from userregistration");	
			$query->execute();
			$result =$query->fetchAll();
			return $result; 
		}
		catch (PDOException $e) { parent::close();
		die("Error: " . $e->getMessage());
		
		}
	}
	
	public function getSpLocationDetails($sId)
	{
		try
		{
			$query=$this->_connection->query("select lattitude,longitude,srStatus,isCompleted from servicerespons where serviceId='$sId'");
			$query->execute();
			$result =$query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
		catch (PDOException $e) { parent::close();
		die("Error: " . $e->getMessage());
		
		}
	}
	
	public function getCusLocationDetails($sId)
	{
		try
		{
			$query=$this->_connection->query("select lattitude,longitude,status from servicerequest where serviceId='$sId'");
			$query->execute();
			$result =$query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
			
			
		}
		catch (PDOException $e) { parent::close();
		die("Error: " . $e->getMessage());
		
		}
		
	}
	
	public function get_cutsomer_info($username)
	{
		try
		{
			$query=$this->_connection->query("select * from spregistration where spEmail='$username' ");
			$query->execute();
			$result =$query->fetchAll();
			return $result; 
			
		}
		catch(PDOException $e) { parent::close();
		die("Error: " . $e->getMessage());
		
		}
	}
	public function update_device_info($tocken,$fcm_key)
	{
		try
		{
			$query=$this->_connection->query("update device_info set fcm_key='$fcm_key' where token='$tocken'"); 
			return true;
		}
		catch(PDOException $e) { parent::close();
		die("Error: " . $e->getMessage());
		
		}
	}
	
	public function getCusID($id)
	{
		
	try
	{
		
		$Query=$this->_connection->query("select userregistration.userUniqueId from userregistration INNER JOIN spregistration ON spregistration.spEmail=userregistration.userEmail WHERE spregistration.spUniqeId='$id'");
		
			$Query->execute();
			$rOw=$Query->fetch(PDO::FETCH_ASSOC);
			if(!empty($rOw))
			{
			return $rOw;
			}
			else
			{
				$rOw=array();
				return $rOw;
			}
	}
	catch (PDOException $e)
	{
		parent::close();
		die("error".$e->getMessage());

}


	}
	
	
	public function getSpId($id)
{
	try
	{
		$Query=$this->_connection->query("select spregistration.spId from spregistration INNER JOIN userregistration ON userregistration.userEmail=spregistration.spEmail WHERE userregistration.userUniqueId='$id'");
			$Query->execute();
			$rOw=$Query->fetch(PDO::FETCH_ASSOC);
			if(!empty($rOw))
			{
			return $rOw;	
			}
			else
			{
				$rOw=array();
				return $rOw;
			}
	}
	catch (PDOException $e)
	{
		parent::close();
		die("error".$e->getMessage());
	}
}



public function updateServiceInfo($userId,$userName,$userMobile,$userImage,$usercarImage,$usercarModel,$userlicenseNo,$userCarYear,$address1,$address2,$state,$city,$postcode){     // Update Service Provider Information
		 try{ 
			
			
			if(!empty($userImage)){
				$getInformation=$this->_connection->prepare("SELECT spImage from spRegistration WHERE spId='".$uSERId."' ");
				$getInformation->execute();
				$rOw=$getInformation->fetch(PDO::FETCH_OBJ);
				$userOldImage=$rOw->spImage;
				$this->_connection->query("update  spRegistration set spImage='".$userImage."' where spId='".$uSERId."' ");
				if(file_exists($userOldImage)) {
				    unlink($userOldImage);
				}
			} 
			if(!empty($usercarImage)){
				$getCarImage=$this->_connection->prepare("SELECT spVhImage from spVehicleInfo WHERE spId='".$uSERId."' ");
				$getCarImage->execute();
				$rOw=$getCarImage->fetch(PDO::FETCH_OBJ);
				$carOldImage=$rOw->spVhImage;
				$this->_connection->query("update  spVehicleInfo set spVhImage='".$usercarImage."' where spId='".$uSERId."' ");
				if(file_exists($carOldImage)) {
				    unlink($carOldImage);
				}
			} 
			
			$this->_connection->query("update  spRegistration set spUserName='".$userName."',spMobile='".$userMobile."',spAddress1='".$address1."',spAddress2='".$address2."',spState='".$state."',spCity='".$city."',spZipCode='".$postcode."' where spId='".$uSERId."' ");
			$this->_connection->query("update  spVehicleInfo set spVhModelNo='".$usercarModel."',spVhYear='".$userCarYear."',spVhLicenseNo='".$userlicenseNo."' where spId='".$uSERId."' ");

			//parent::close();
				return array('status'=>$this->_success,'image'=>$userImage,'carImage'=>$usercarImage,'message'=>$this->_profileUpdate); 
		 }catch (PDOException $e) { parent::close();
		  die("Connection failed in updateServiceInfo: " . $e->getMessage());
	        }	

	}


public function updateSpLocation($spLat,$spLng,$sId)
{
	$this->_connection->query("update servicerespons set lattitude='$spLat',longitude='$spLng' where serviceId='$sId'");
}

public function updateCusLocation($cusLat,$cusLng,$sId)
{
	$this->_connection->query("update servicerequest set lattitude='$cusLat',longitude='$cusLng' where serviceId='$sId'");
}

}
?>