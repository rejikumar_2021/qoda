<?php
require('mailer/PHPMailerAutoload.php');
class Emails{
private $_headerEmail="support@qodamobile.com";
private $_basePath="http://qodamobile.com/Api/";
private $_imageBasePath1="http://qodamobile.com/images/layer1.png";
private $_imageBasePath2="http://qodamobile.com/images/layer2.png";
public function forgetPassword($email,$passwd){   //forget Passsword 
$tO=$email;
$sUbject="Reset your password for your QODA account";
$bOdy='<center><img src="'.$this->_imageBasePath1.'" class="img-responsive" alt="" /></center>
<p>Hi,</p>
<p>This email is to help you reset your password for your QODA account. If you did not ask for your password to be reset,
    please discard this email.</p>
<p>Below are your QODA account ID and new password:</p>
<p>Login ID: '.$email.' <br/>
New temporary Password: '.$passwd.'
</p>
<p>Sincerely,<br/>
    QODA Team</p>';
         
$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "qodainfo@gmail.com";
$mail->Password = "qodainfo123";
$mail->SetFrom("info@adroitminds.com");
$mail->Subject = $sUbject;
$mail->Body =$bOdy;
$mail->AddAddress($tO);
$mail->Send();
return 1;
 
		 
		 
}

public function customerConfirmEmail($userName,$email){

	$link=$_SERVER['HTTP_HOST']."/user/index.php/welcome/userAuthentcation?emial=".$email;
	$sUbject="Thank you for signing up with QODA!";
	$bOdy="<center><img src='".$this->_imageBasePath2."' class='img-responsive' alt='' /></center>
	<center><p style='font-size:20px'><b>Hi  '".$userName.",'</b></p>
	<p style='font-size:25px'><b>Thanks for signing up with QODA!</b></p>
		<p style='font-size:25px'>Please click this <a href='".$link."'>link</a> to activate your account</p>";
	$mail = new PHPMailer(); // create a new object
	$mail->IsSMTP(); // enable SMTP
	$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = true; // authentication enabled
	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
	$mail->Host = "smtp.gmail.com";
	$mail->Port = 465; // or 587
	$mail->IsHTML(true);
	$mail->Username = "qodainfo@gmail.com";
	$mail->Password = "qodainfo123";
	$mail->SetFrom("info@adroitminds.com");
	$mail->Subject = $sUbject;
	$mail->Body =$bOdy;
	$mail->AddAddress($email);
	$mail->Send();
	return 1;
}



public function usercustomerSignUpEmail($email){   //send Passsword 
$tO=$email;
$sUbject="Thank you for signing up with QODA!  We are excited to have you";
$bOdy="<center><img src='".$this->_imageBasePath2."' class='img-responsive' alt='' /></center>
<center><p style='font-size:25px'><b>Thanks for signing up with QODA!</b></p>
	<p style='font-size:25px'>We are excited to have you use QODA as your<br>
	on-demand roadside assistance service.</p>
	<p style='font-size:25px'>Request a service and in minutes a service provider will be<br>
there to assist you and <b> get you back on the road!</b></p>
<p style='font-size:25px'><b>4 easy steps to get assistance:</b></p>
<p style='font-size:25px;line-height: 35px'>1. Use the QODA app to request service.<br>
2. Accept the service person that has accepted your request.<br>
3. Let the service provider complete the request.<br>
4. We will charge your credit card on file and email you a <br>receipt.</br>
</p>
<p style='font-size:25px'><b>Tips to Get Started:</b></p>
<p style='font-size:25px'>Request as soon as you pull over to a safe place.</p>
<p style='font-size:25px'>Estimate your service charge before committing.</p>
<p style='font-size:25px'>Charges are based on service requested and distance
between you and the service person.</p>
<p style='font-size:25px;margin-top: 100px;'><b> Question? Contact Us.</b></p>
<p style='font-size:25px;'>QODA community managers are here to assist you<br>
throughout your experience. Contact us at<br>
<a href='mailto:support@qodamobile.com'><b>support@qodamobile.com.</b></a></p>
</center>
";
	
	         
$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "qodainfo@gmail.com";
$mail->Password = "qodainfo123";
$mail->SetFrom("info@adroitminds.com");
$mail->Subject = $sUbject;
$mail->Body =$bOdy;
$mail->AddAddress($tO);
$mail->Send();
return 1;
	
	
	
}
public function sendEmailSp_SignUp($email){
$tO=$email;
$sUbject="A new service person profile has been created";
$bOdy="<center><img src='".$this->_imageBasePath2."' class='img-responsive' alt='' /></center>
<center><p style='font-size:25px'><b>Thanks for signing up with QODA!</b></p>
	<p style='font-size:25px'>We are excited to have you as a <b>QODA service provider,</b><br>
	providing on demand roadside assistance service.</p>
	<p style='font-size:25px'><b>A few things before you get start:</b></p>
	<p style='font-size:25px;line-height: 40px;'>Submit a <a href='http://qoda.adroitminds.com/user/index.php/login'>background check </a> with HireRight <br>
	Watch the short <font color='#FF0000'>training video</font> at <font color='#0000FF'><u><a href='http://qoda.adroitminds.com/user/'>QODA</a></u></font><br>
	Take a short <font color='#FF0000'>quiz</font> and get 100% right at <font color='#0000FF'><u><a href='http://qoda.adroitminds.com/user/'>QODA</a></u></font></p>
	<p style='font-size:25px'>Then you are ready to start providing services.</p>
	<p style='font-size:25px'><b>4 easy steps to providing assistance:</b></p>
	<p style='font-size:25px;line-height: 35px;'>1. Use the QODA app to accept a request.<br>
	2. Drive to your customer.<br>
	3. Provide exceptional service.<br>
	4. Get paid!</p>
	<p style='font-size:25px;margin-top:150px'><b>Questions? Contact Us.</b></p>
	<p style='font-size:25px'>QODA community managers are here to assist you<br>
throughout your experience. Contact us at<br>
<a href='mailto:support@qodamobile.com'><b>support@qodamobile.com.</b></a></p>
</center>
";
	
	$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "qodainfo@gmail.com";
$mail->Password = "qodainfo123";
$mail->SetFrom("info@adroitminds.com");
$mail->Subject = $sUbject;
$mail->Body =$bOdy;
$mail->AddAddress($tO);
$mail->Send();
return 1;
           
}
public function sendMailAdmin(){
$tO=$_headerEmail;
$sUbject="A new service person profile has been created";
$bOdy="<center><img src='".$this->_imageBasePath2."' class='img-responsive' alt='' /></center>

<b>App Admin</b><br>
<b>A new service person profile has been created.</b><br>
<p>Please log-in to app admin portal to complete approval process </p>
<p>Next Steps:</p>
    
<p>Background check via instantcheckmate.com.</p>

<b>Send training video link & Quiz to service person.</b>

<p>Send approval or rejection email to service person.</p>


<b>Grant access to QODA</b>

<p>Please contact support@qodamobile.com to resolve this ASAP.</p>
<p>Sincerely,<br/>
    QODA Team</p>";
	
	$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "qodainfo@gmail.com";
$mail->Password = "qodainfo123";
$mail->SetFrom("info@adroitminds.com");
$mail->Subject = $sUbject;
$mail->Body =$bOdy;
$mail->AddAddress($tO);
$mail->Send();
return 1;
         
}

public function sendEmailAdmindispute(){
$tO=$_headerEmail;
$sUbject="Dispute By Customer";
$bOdy="<center><img src='".$this->_imageBasePath2."' class='img-responsive' alt='' /></center>
<p><b><center><h2>App Admin</h2></center></b></p>
<p><b><center><h2>A dispute has been created by a service provider.</h2></center></b></p>
<p><b><center>Please log-in to app admin portal to begin dispute resolution.</center></b></p>
<p><b><center><h2>Next Steps:</h2></center></b></p>
<p><b><center>Send customer email asking for service dispute details.</center></b></p>
<p><b><center>Send service person email asking for dispute details.</center></b></p>
<p><b><center>If service person successfully completed request, customer will be charged </center></b></p><p><b><center>service fee plus travel cost.</center></b></p>
<p><b><center>If service person did not successfully complete request, customer will ONLY be charged travel cost.</center></b></p>


<p><b><center><h2>Please contact <a href='mailto:support@qodamobile.com'>support@qodamobile.com</a> to resolve this ASAP.</h2> </center></b></p>
<p><b><center><h2>QODA Mobile, LLC</h2> </center></b></p> 
<p><b><center><h2>San Francisco, CA 94109</h2> </center></b></p>
<p><b><center><h2><a href='http://qodamobile.com/'>qodamobile.com</a></h2> </center></b></p>
<p>Sincerely,<br/>
    QODA Team</p>";
	
	$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "qodainfo@gmail.com";
$mail->Password = "qodainfo123";
$mail->SetFrom("info@adroitminds.com");
$mail->Subject = $sUbject;
$mail->Body =$bOdy;
$mail->AddAddress($tO);
$mail->Send();
return 1;
        
}

public function sendmailUserAfterComplete($email,$userImage,$RequestDate,$ServiceProviderName,$ServiceName,$ServiceCost,$TravelCost,$Location,$totalcost){
$tO=$email;
$sUbject="Job Summary";
$bOdy="<center><img src='".$this->_imageBasePath1."' class='img-responsive' alt='' /></center>
<br/>
<br/>
<center><img src='".$this->_basePath.$userImage."' class='img-responsive' alt='' width='100px' /></center>
<br/>
<center>Thanks for using <b>".$ServiceProviderName."</b> as your service provider! On  <b>".$RequestDate."</b> </center><br/>
<br/>

<p><b>Service Details</b></p>

<p>Service Type      : <b> ".$ServiceName."</b>
</p>
<p>Service Cost      : <b>".$ServiceCost."</b>
</p>

<p>Travel Cost       : <b> ".$TravelCost."</b>
</p>
<p>Total Cost       : <b> ".$totalcost."</b>
</p>
<p>Location          : <b> ".$Location."</b>
</p>
<br/>
<p>
 Sincerely </p>
QODA Team </p>";

$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "qodainfo@gmail.com";
$mail->Password = "qodainfo123";
$mail->SetFrom("info@adroitminds.com");
$mail->Subject = $sUbject;
$mail->Body =$bOdy;
$mail->AddAddress($tO);
$mail->Send();
return 1;
 
}

public function sendmailSpAfterComplete($email,$userImage,$RequestDate,$userName,$ServiceName,$ServiceCost,$TravelCost,$Location,$totalcost,$marginCost){
$tO=$email;
$sUbject="Job Summary";
$bOdy="<center><img src='".$this->_imageBasePath1."' class='img-responsive' alt='' /></center>
<br/>
<br/>
<center><img src='".$this->_basePath.$userImage."' class='img-responsive' alt='' width='100px' /></center>
<br/>
<center>Thanks for providing service to <b>".$userName."</b> On  <b>".$RequestDate."</b>   </center><br/><br/>
<p><b>Service Details</b></p>

<p>Service Type      : <b> ".$ServiceName."</b>
</p>
<p>Service Cost      : <b>".$ServiceCost."</b>
</p>

<p>Travel Cost       : <b> ".$TravelCost."</b>
</p>
<p>Total Cost       : <b> ".$totalcost."</b>
</p>
<p>After Margin Total Cost       : <b> ".$marginCost."</b>
</p>
<p>Location          : <b> ".$Location."</b>
</p>
<br/>
<p>
 Sincerely </p>
QODA Team </p>";

$mail = new PHPMailer(); // create a new object
$mail->IsSMTP(); // enable SMTP
$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
$mail->SMTPAuth = true; // authentication enabled
$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
$mail->Host = "smtp.gmail.com";
$mail->Port = 465; // or 587
$mail->IsHTML(true);
$mail->Username = "qodainfo@gmail.com";
$mail->Password = "qodainfo123";
$mail->SetFrom("info@adroitminds.com");
$mail->Subject = $sUbject;
$mail->Body =$bOdy;
$mail->AddAddress($tO);
$mail->Send();
return 1;
  

}


}

?>
