<?php
class Database
{
    protected $_connection;
     function __construct()
    {
        try {
			$host="localhost";
			$user="root";
			$pass="";
			$db="qoda_app";
		   $getConn=parse_ini_file("ini_file/config.ini");  
		   $this->_connection = new PDO("mysql:host=".$host.";dbname=".$db, $user,$pass);
		   $this->_connection ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            die( "Connection failed: " . $e->getMessage());
        }
    }
    function close(){
         $this->_connection=null;
   }
}

?>
