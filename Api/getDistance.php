<?php
error_reporting(0);
//$lt1=$_POST['lat1'];
$lt2=$_POST['lat2'];
//$lng1=$_POST['long1'];
$lng2=$_POST['long2'];

$lt1=8.584399;
$lng1=76.880162;

$dis=GetDrivingDistance($lt1,$lt2,$lng1,$lng2);
echo json_encode($dis);
//print_r($dis);

function GetDrivingDistance($lat1, $lat2, $long1, $long2)
{
 $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
	
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
  	 return array('distance' => $dist, 'time' => $time);
   //return $response_a;
}

