<?php
include('dashboard.php'); 
?>
<style>
.q
{
	float: left;
    margin-right: 10px;
	font-weight:bold;
}
.qs
{
	margin-left: 20px;
}
.sp
{
	float: left; 
	margin-left: 22px;width: 85%;
}
.cr
{
	margin-left: 22px;
}
.glyphicon-ok
{
	color: #0af14c;
}
.glyphicon-remove
{
	color: #ef2626;
}
</style>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<!--- model-->
<div class="modal fade" id="viewResponse" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SP Evaluation Result</h4>
        </div>
        <div class="modal-body">
          <table width="100%" class="table table-hover">
          <?php
		  $i=1;
		  while($i<10)
		  {
		  ?>
          <tr>
          <td>
         <div class="q">Q</div>
         <div class="qs">Cras interdum auctor quam id bibendum. Sed quam ipsum, cursus elementum egestas nec, euismod et neque. Fusce rutrum tincidunt erat at congue. Morbi in aliquet velit?</div>
         <div class="sp">SP answer</div>
         <div class="ch_lbl"><span class="glyphicon glyphicon-ok"></span><span class="glyphicon glyphicon-remove"></span></div>
         <div class="cr">Correct</div>
          </td>
          </tr>
          <?php
		  ++$i;
		  }
		  ?>
          
          
          
          </table>
                           
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-md btn-success" name="update" id="accept">Accept</button>
          <button type="button" class="btn btn-danger">Reject</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- model-->


<div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-eye"></i>  View Results
                                </li>
                            </ol>
                        </div>
                        <table id="spResponse" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>SlNo</th>
							<th>SP Name</th>
							<th>email</th>
							<th>Attended Date</th>
							<th>View Answers</th>
                            
						</tr>
					</thead>
					<td>1</td>
                    <td>Reji kumar</td>
                    <td>abcd@abcd.com</td>
                    <td>2017-09-20</td>
                    <td><button class="showResponse">View</button>
					<tbody>
                   
							
					</tbody>
				</table>



<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="js/editQuestion.js"></script>
<script>
$(document).ready(function() {
    $('#spResponse').DataTable();
} );
</script>
<?php include('footer.php')?>