<?php include('dashboard.php');

$newwObj=new AppModel();
$reSUlt=$newwObj->getUserInformation();

$curPageURL=curPageURL();

 ?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    	<div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i> Registered Users
                                </li>
                            </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">Registered Users</span>
                        </h3>
                        
                         <div class="panel panel-primary">
					
                    <div class="userTable">
                    <table id="examplee" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone No.</th>
                <th>User Image</th>
               	<th>Model</th>
                <th>Year</th>
                <th>Licence No.</th>
                <th>Vehicle Image</th>
                <th>Signup Date</th>
                <th>Action</th>
            </tr>
        </thead>
       
        <tbody>
            <?php  for($i=0;$i<sizeOf($reSUlt);$i++){ ?>
            <tr>
                <td><?php echo $reSUlt[$i]['uSerName']; ?></td>
                <td><?php echo $reSUlt[$i]['uSerEmail']; ?> </td>
                <td><?php echo $reSUlt[$i]['uSerPhone']; ?></td>
	<?php if(!empty($reSUlt[$i]['uSerPic'])) { ?>
                <td><img alt="User Pic" src="<?php echo BASEPATH.$reSUlt[$i]['uSerPic'];  ?>" class="img-responsive" width="50px" height="50px"></td>
	<?php }else{ ?>
		<td><img alt="User Pic" src="images/usernoProfile.png" class="img-responsive" width="50px" height="50px"></td>
		<?php } ?>
               	<td><?php echo $reSUlt[$i]['uservehInfo']['userVehModel'];  ?></td>
                <td><?php echo $reSUlt[$i]['uservehInfo']['userVehYear'];  ?></td>
                <td><?php echo $reSUlt[$i]['uservehInfo']['userVehLicense'];  ?> </td>
                
	<?php if(!empty($reSUlt[$i]['uservehInfo']['userVehPic'])) { ?>
                <td><img alt="Vehicle Pic" src="<?php echo BASEPATH.$reSUlt[$i]['uservehInfo']['userVehPic'];  ?>" class="img-responsive" width="50px" height="50px"> </td>
	<?php }else{ ?>
		<td><img alt="User Pic" src="images/placeholder.png" class="img-responsive" width="50px" height="50px"></td>
		<?php } ?>
        
        <td style="min-width:80px;"><?php echo date("d-m-Y", strtotime($reSUlt[$i]['lTime'])); ?></td>
                <td><a href="sPstatus.php?uId=<?php echo $reSUlt[$i]['uSerId'];  ?>&reTurn=<?php echo $curPageURL; ?>"  data-original-title="Remove this user" onClick="return confirm('Are you sure you want to delete User?');" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a> </td>
            </tr>
             <?php } ?>
        </tbody>
    </table>
    </div>
					
                    </div>
                </div>
                <!-- /.row -->
        
            </div> 

<script>
$(document).ready(function() {
    $('#examplee').DataTable();
} );

</script>


<?php include('footer.php'); ?>
