<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    <link type="text/css" rel="stylesheet" href="css/main.css">
    <!--layout, header, footer, sidebar css-->

</head>
<body>

    <div class="login_outer">
    	<div class="login_form">
        <?php
             if(isset($_POST['login'])){

            require 'autoloadd.php';
             $userName=$_POST['userName'];
                $PAsSWORd=$_POST['userPassword'];

                $newObj=new AppModel();
                 $reSULT=$newObj->getAdminUser($userName,$PAsSWORd);
          
                if($reSULT==0){
                    echo "<script>window.alert('UserName OR Password Wrong');</script>";    
                }else{
                    session_start();
                   $_SESSION["userId"]=$reSULT;
                    echo "<script>window.location='adminPortal.php';</script>";
                }
            }
?>
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>"  method="post">
            
            <center>   <a id="logo"  class="login-logo"><img src="images/logo.png" class="img-responsive" alt="" /></a></center>
           
            <div class="form-group">
                <div class="input-group margin-bottom-sm">
                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    <input class="form-control" type="text" placeholder="Username" name="userName">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input class="form-control" type="password" placeholder="Password" name="userPassword">
                </div>
            </div>
            <input class="btn btn-block btn-lg btn-orange" type="submit" value="LOGIN" name="login">
            
        </form>
        <p class="text-center">
            <a href="forgetPasword.php">Forgot Password</a>
        </p>
    </div>
</div>
</body>
</html>
