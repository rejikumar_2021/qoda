<?php include('dashboard.php'); 

$sobj=new AppModel();
$serviceREsult=$sobj->getSErviceDetails();

?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
						<div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i>  All Service Requests
                                </li>
                            </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">All Service Requests</span>
                        </h3>
                        
                         <div class="panel panel-primary">
					
                    <div class="userTable">
                    <table id="examplee" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
	    <th>Sr. No. </th>
                <th>Customer Name</th>
                <th>Service Type</th>
                <th>Service Person Name</th>
                <th>Job Status</th>
                <th>Rating by customer</th>
                <th>Customer's Feedback</th>
	      <th>Date/Time</th>
            </tr>
        </thead>
       
        <tbody>
             <?php $j=1;  for($i=0;$i<count($serviceREsult);$i++,$j++){ ?>
            <tr>
	    <td><?php echo $j; ?></td>
                <td><?php echo $serviceREsult[$i]['custName']; ?></td>
                <td><?php echo $serviceREsult[$i]['serviceTypeName']; ?> </td>
                <td><?php echo $serviceREsult[$i]['servicepersonName']; ?></td>
                <td><?php if($serviceREsult[$i]['srStatus']==4){ echo "Completed";} if($serviceREsult[$i]['srStatus']==0){ echo "Cancel by Customer";} if($serviceREsult[$i]['srStatus']==2){ echo "In Process";}  ?> </td>
                <td><?php echo $serviceREsult[$i]['serviceRating']['custRating'];  ?></td>
                <td><?php echo $serviceREsult[$i]['serviceRating']['custFeedback'];  ?></td> 
		<td><?php echo $serviceREsult[$i]['datE']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    </div>
					
                    </div>
                </div>
                <!-- /.row -->
        
            </div> 

<script>
$(document).ready(function() {
    $('#examplee').DataTable();
} );

</script>


<?php include('footer.php'); ?>
