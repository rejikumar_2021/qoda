<?php 
include('dashboard.php'); 
$spobj=new AppModel();
$openCaseREsult=$spobj->getopenCaseResolved();
$curPageURL=curPageURL();
?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    
                    <div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i>  Resolve Open Case
                                </li>
                            </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">Resolve Open Case</span>
                        </h3>
                	 <div class="panel panel-primary">
					
                    <div class="userTable" style="margin:21px">
                    <table id="examplee" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Dispute Title </th>
                 <th>Travel Cost</th>
                <th>Total Cost</th>
                 <th>Action</th>
                <th>Dispute Status</th>
            </tr>
        </thead>
       
        <tbody>
             <?php $j=1; for($i=0;$i<sizeOf($openCaseREsult);$i++,$j++){ ?>
            <tr>
                <td><?php echo $j; ?></td>
                <td><?php echo $openCaseREsult[$i]['userReason'];  ?></td>
                <td><?php echo $openCaseREsult[$i]['serviceTCost']['svtravelCost']; ?></td>
                <td><?php echo $openCaseREsult[$i]['serviceTCost']['svTotalCost']; ?></td>
                <td><a href="viewopencase.php?dispid=<?php echo $openCaseREsult[$i]['disputeID'];  ?>" title="View Detail" ><span class="menu-title"><i class="fa fa-lg fa-eye"></i></span></a>
                    <span class="menu-title"><a href="sPstatus.php?dispId=<?php echo $openCaseREsult[$i]['disputeID'];  ?>&reTurn=<?php echo $curPageURL; ?>"  data-original-title="Remove this user" onClick="return confirm('Are you sure you want to end dispute case?');" data-toggle="tooltip" type="button" class="btn-danger"><i class="glyphicon glyphicon-remove"></i></a></span> </td>
                <?php if($openCaseREsult[$i]['disputeStatus']==0)
                {?>
                    <td>Incomplete</td>
                <?php
            }
            else
            {?>
                    <td>Complete</td>
           <?php }?>                
            </tr>
            <?php } ?>
        </tbody>
    </table>
    </div>
					
                    </div>
                </div>
                <!-- /.row -->
        
            </div> 

<script>
$(document).ready(function() {
    $('#examplee').DataTable();
} );

</script>


<?php include('footer.php'); ?>
