<?php
class Database
{
    protected $_connection;
     function __construct()
    {
        try {
		   $getConn=parse_ini_file("ini_file/config.ini");  
		   $this->_connection = new PDO("mysql:host=".$getConn['host'].";dbname=".$getConn['dbName'], $getConn['userName'], $getConn['password']);
		   $this->_connection ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            die( "Connection failed: " . $e->getMessage());
        }
    }
    function close(){
         $this->_connection=null;
   }
}

?>
