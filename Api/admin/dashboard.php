
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
   
   <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
  
</head>
<?php
require 'autoload.php';
?>
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><b>QODA</b></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
             <li class="dropdown">
                    <a href="logout.php"><i class="fa fa-lg fa-power-off"></i> <span class="menu-title">Log Out</span></a>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="adminPortal.php"><i class="fa fa-lg fa-dashboard"></i><span class="menu-title"> Dashboard</span></a>
                    </li>
                    
                    <li>
                        <a href="javascript:void(0);" data-toggle="collapse" data-target="#appSetting"><i class="fa fa-th-list fa-lg"></i> <span class="menu-title">App Settings </span><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="appSetting" class="collapse">
                            <li>
                                <a href="changeServiceCost.php"><i class="fa fa-usd fa-lg"></i><span class="menu-title">Price Matrix</span></a>
                            </li>
                            <li>
                                <a href="setRadius.php"><i class="fa fa-cog fa-lg"></i><span class="menu-title">Set Radius</span></a>
                            </li>
                            <li>
                                <a href="uploadContract.php"><i class="fa fa-file-text-o fa-lg"></i><span class="menu-title">Upload T&C</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#appUser"><i class="fa fa-th-list fa-lg"></i> <span class="menu-title">App User Information</span><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="appUser" class="collapse">
                            <li>
                                <a href="RequestServicePerson.php"><i class="fa fa-user-plus fa-lg"></i><span class="menu-title">Unverified Service Providers</span></a>
                            </li>
                             <li>
                                <a href="acceptServicePerson.php"><i class="fa fa-user-plus fa-lg"></i><span class="menu-title">Service Providers</span></a>
                            </li>
                            <li>
                                <a href="userInformation.php"><i class="fa fa-user-circle fa-lg"></i><span class="menu-title">Registered Users</span></a>
                            </li>
                            <li>
                                <a href="servicedetails.php"><i class="fa fa-info fa-lg"></i><span class="menu-title">All Service Requests</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>


<!-- <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#appBlockUser"><i class="fa fa-th-list fa-lg"></i> <span class="menu-title">Blocked Users </span><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="appBlockUser" class="collapse">
                              <li>
                                <a href="blockedServicePerson.php"><i class="fa fa-user-circle fa-lg"></i><span class="menu-title">Blocked Service Providers</span></a>
                            </li>
                            <li>
                                <a href="blockeduserInformation.php"><i class="fa fa-user-circle fa-lg"></i><span class="menu-title">Blocked Users</span></a>
                            </li>
                       
                        </ul>
                    </li>
                    -->
                    <li>




                        <a href="javascript:;" data-toggle="collapse" data-target="#appOpencase"><i class="fa fa-th-list fa-lg"></i> <span class="menu-title">Open Case</span><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="appOpencase" class="collapse">
                            <li>
                                <a href="resolveOpenCase.php"><i class="fa fa-unlock fa-lg"></i><span class="menu-title">Resolve Open Case</span></a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                    
                    
                    <a href="javascript:;" data-toggle="collapse" data-target="#Notification"><i class="fa fa-th-list fa-lg"></i> <span class="menu-title">Notification</span><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="Notification" class="collapse">
                            <li>
                                <a href="deviceNotification.php"><i class="fa fa-mobile fa-lg"></i><span class="menu-title">Push Notification</span></a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                    
                    
                     <a href="javascript:;" data-toggle="collapse" data-target="#spsettings"><i class="fa fa-th-list fa-lg"></i> <span class="menu-title">SP settings</span><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="spsettings" class="collapse">
                            <li>
                                <a href="uploadVideo.php"><i class="fa fa-tv fa-lg"></i><span class="menu-title">Upload Video</span></a>
                            </li>
                            
                            <li>
                                <a href="uploadQuestion.php"><i class="fa fa-question fa-lg"></i><span class="menu-title">Add Questions</span></a>
                            </li>
                            
                            <li>
                                <a href="viewQuestions.php"><i class="fa fa-table fa-lg"></i><span class="menu-title">View Questions</span></a>
                            </li>
                            
                            <!--<li>
                                <a href="viewResults.php"><i class="fa fa-pencil fa-lg"></i><span class="menu-title">View Results</span></a>
                            </li>-->
                            
                        </ul>
                    </li>
                    <li>
                    
                        <a href="javascript:;" data-toggle="collapse" data-target="#appAdminSetting"><i class="fa fa-th-list fa-lg"></i> <span class="menu-title">Admin Settings</span><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="appAdminSetting" class="collapse">
                            <li>
                                <a href="changePassword.php"><i class="fa fa-key fa-lg"></i><span class="menu-title">Change Password</span></a>
                            </li>
                            
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper" class="porTal">

            
