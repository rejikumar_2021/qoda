   <?php
     require 'autoloadd.php';
        if(isset($_POST['updatePasswd']))
             {

            
              $userEmail=$_POST['userEmail'];
           $neWObj=new AppModel();
              $reSULT=$neWObj->forgetpassword($userEmail);
         if($reSULT==0){
                    echo "<script>window.alert('Email does not exist.');</script>";    
                }else{
                     echo "<script>window.alert('Your Password has been reset. Please check your Email.');</script>"; 
                }
              }
                ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    <link type="text/css" rel="stylesheet" href="css/main.css">
    <!--layout, header, footer, sidebar css-->
        
</head>
<body >
<div class="wrapForget">
    <div class="login_outer">
        <center>
            <div class="panel-body">
        		<div class="login_form">
            
    	 			<a id="logo"  class="login-logo"><img src="images/logo.png" class="img-responsive" alt="" /></a>
              		<form action="forgetPasword.php" class="form-horizontal" method="post" >
               		<div class="form-group">
                    	<div class="input-group margin-bottom-sm">
  							<span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
  							<input class="form-control" type="text" placeholder="E-mail" name="userEmail">
						</div>
					</div>
					<div class="form-group">
                    	<input type="submit" name="updatePasswd" value="Get Password" class="btn btn-block btn-lg  btn-orange">
               		</div>
          			</form>
           			<p class="text-center">
                		<a href="index.php">Login</a>
           			 </p>
   				</div>
    		</div>
         </center>
	</div>
 </div>
</body>  

</html>
