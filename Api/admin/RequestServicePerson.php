<?php 
include('dashboard.php'); 
$newObj=new AppModel();
$reSUlt=$newObj->getREquestServicePerson();

$curPageURL=curPageURL();

?>
<div id="response" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                  		<div class="brEad">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Unverified Service Providers
                            </li>
                        </ol>
                        </div>
                          <h3 class="page-header">
                           <span class="menu-title">Unverified Service Providers</span>
                        </h3>
                         <div class="panel panel-primary">
					
                    <div class="userTable">
                    <table id="spList" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
               	<th>Phone No</th>
                <th>User Image</th>
                <th>Car's License Plate no</th>
                <th>User Wallet</th>
                <th>Signup Date</th>
                <th>Action</th>
            </tr>
        </thead>
       
        <tbody>
            <?php if(sizeOf($reSUlt)==0){ ?>
        <tr>
          <td colspan="7" style="text-align:center; font-weight:bold;"> Not Found </td>
        </tr>
        <?php } ?>
        <?php $j=1;  for($i=0;$i<sizeOf($reSUlt);$i++,$j++){ ?>
            <tr>
                <td><?php echo $reSUlt[$i]['sPName']; ?> </td>
                <td><?php echo $reSUlt[$i]['sPEmail'];  ?></td>
                <td><?php echo $reSUlt[$i]['sPPhone'];  ?></td>
	<?php if(!empty($reSUlt[$i]['sPPic'])) { ?>
                <td><img alt="Service Person Pic" src="<?php echo BASEPATH.$reSUlt[$i]['sPPic'];  ?>" class="img-responsive" width="100px" height="100px"></td>
	<?php }else{ ?>
		<td><img alt="Service Person Pic" src="images/usernoProfile.png" class="img-responsive" width="100px" height="100px"></td>
	 <?php } ?>
                <td><?php echo $reSUlt[$i]['sPLicenseNo'];  ?></td>
                <td><?php echo $reSUlt[$i]['sPWallet'];  ?></td>
                 <th><?php echo date("d-m-Y", strtotime($reSUlt[$i]['R_date'])); ?></th>
                <td><a href="viewPerson.php?sPId=<?php echo $reSUlt[$i]['sPId'];  ?>" title="View Service Person" ><span class="menu-title"><i class="fa fa-lg fa-eye"></i></span></a>
                    <a href="viewResult.php?spId=<?php echo $reSUlt[$i]['sPId'];  ?>"title="View Result" ><span class="menu-title"><i class="fa fa-check" aria-hidden="true"></i></span></a>
                    <a href="sPstatus.php?sPtId=<?php echo $reSUlt[$i]['sPId'];  ?>&reTurn=<?php echo $curPageURL; ?>" title="Reject Service" onClick="return confirm('Are you sure you want to reject?');"><span class="menu-title"><i class="fa fa-times" aria-hidden="true"></i></span></a>

                </td>
            </tr>
            <?php } ?>
            
        </tbody>
    </table>
    </div>
					
                    </div>
                </div>
                <!-- /.row -->
        
            </div> 
<script src="js/acceptService.js"></script>

<?php include('footer.php'); ?>
