  
 <?php include('dashboard.php');
if(isset($_POST['save'])){
  $oldpasswd=$_POST['oldpasswd'];
  $newpasswd=$_POST['newpasswd'];
  $adminID=$_SESSION["userId"];
  $newObj=new AppModel();
  $reSULT=$newObj->changePassword($adminID,$newpasswd,$oldpasswd);
  if($reSULT==1)
  {
       echo "<script>window.alert('Password changed succesfully.');</script>";    
  }
  if($reSULT==2)
  {
       echo "<script>window.alert('Old password does not match!');</script>";    
  }
}

 ?>

    <script src="js/validator.js"></script>
     
 <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row ">
                 <div class="col-lg-12">
                 	<div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i>Change Password
                                </li>
                            </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">Change Password</span>
                        </h3>
                 
                   <div class="col-lg-12">
                  
                        <div class="panel panel-primary">
                            
                        </div>
                    	<div class="formUpload">
                           <form data-toggle="validator" role="form" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                              <div class="form-group">
                                <label for="inputPaSsword" class="control-label">Old Password:</label>
                                <input type="password" class="form-control" id="inputEmail" placeholder="Old Password" name="oldpasswd" required>
                                <div class="help-block with-errors"></div>
                              </div>
                              <div class="form-group">
                                <label for="inputPassword" class="control-label">New Password:</label>
                                    <input type="password" data-minlength="6" minlength="6" class="form-control" id="inputPassword" name="newpasswd"  placeholder="New Password" required>
                                    <div class="help-block">Minimum of 6 characters</div>
                              </div>
                              <div class="form-group">
                                    <label for="inputPassword" class="control-label">Confirm Password:</label>
                                  <input type="password" class="form-control" minlength="6"  id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm" required>
                                  <div class="help-block with-errors"></div>
                              </div>
                             <div class="form-group">
                                <button type="submit" class="btn btn-primary" name="save">Submit</button>
                              </div>
                            </form>
                    	</div>
                       </div>
                	</div>
                   </div>
                 </div>
            
                <!-- /.row -->
        
            </div> 
           
             <script>
			 $('#form').validator();
			 </script> 
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
  
    
 <?php include('footer.php')?>
