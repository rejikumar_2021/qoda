function editQues(id)
{
	var j=0;
		var d=[];
		var val="id="+id;
		$.ajax({
			type:'POST',
			url:"editQuestion.php",
			data:val,
			beforeSend: function()
			{
				
			},
			complete: function()
			{
				
			},
			success: function(data)
			{
				
				var result = JSON.parse(data);
				for (var i=0; i<result.length; i++)
    for (var name in result[i]) {
		d[j]=result[i][name];
		++j;
    }
				
				
				if(d[6]==2)
				{
					$('#qsId').val(d[0]);
					$('#qsType').val(d[6]);
					$('#question').val(d[1]);
					$('#option1').prop('readonly',true);
					$('#option2').prop('readonly',true);
					$('#option3').prop('readonly',true);
					$('#correct').prop('disabled',true);
					
					$('#option1').val('');
					$('#option2').val('');
					$('#option3').val('');
					$('#correct').val('');
					$('#editQS').modal('show'); 
					
				}
				else
				{
					
					$('#option1').prop('readonly',false);
					$('#option2').prop('readonly',false);
					$('#option3').prop('readonly',false);
					$('#correct').prop('disabled',false);
					
					$('#qsId').val(d[0]);
					$('#qsType').val(d[6]);
					$('#question').val(d[1]);
					$('#option1').val(d[2]);
					$('#option2').val(d[3]);
					$('#option3').val(d[4]);
					$('#correct').val(d[5]);
					$('#editQS').modal('show'); 
					
				}
				
				
				
				
			},
			error: function()
			{
				alert("Could not connect to server");
			}
			
			
			});
		
}

$(function(){
	
	$('.editQS').click(function(){
		
		var j=0;
		var d=[];
		console.log(d);
		var id=$(this).val();
		
		var val="id="+id;
		$.ajax({
			type:'POST',
			url:"editQuestion.php",
			data:val,
			beforeSend: function()
			{
				
			},
			complete: function()
			{
				
			},
			success: function(data)
			{
				
				var result = JSON.parse(data);
				for (var i=0; i<result.length; i++)
    for (var name in result[i]) {
		d[j]=result[i][name];
		++j;
    }
				
				
				if(d[6]==2)
				{
					$('#qsId').val(d[0]);
					$('#qsType').val(d[6]);
					$('#question').val(d[1]);
					$('#option1').prop('readonly',true);
					$('#option2').prop('readonly',true);
					$('#option3').prop('readonly',true);
					$('#correct').prop('disabled',true);
					
					$('#option1').val('');
					$('#option2').val('');
					$('#option3').val('');
					$('#correct').val('');
					$('#editQS').modal('show'); 
					
				}
				else
				{
					
					$('#option1').prop('readonly',false);
					$('#option2').prop('readonly',false);
					$('#option3').prop('readonly',false);
					$('#correct').prop('disabled',false);
					
					$('#qsId').val(d[0]);
					$('#qsType').val(d[6]);
					$('#question').val(d[1]);
					$('#option1').val(d[2]);
					$('#option2').val(d[3]);
					$('#option3').val(d[4]);
					$('#correct').val(d[5]);
					$('#editQS').modal('show'); 
					
				}
				
				
				
				
			},
			error: function()
			{
				alert("Could not connect to server");
			}
			
			
			});
		
		
		
	});
	
	$('#qsType').change(function(){
		
	var ch=parseInt($(this).val());
	switch(ch)
	{
		case 2:
		$('#option1').prop('readonly',true);
		$('#option2').prop('readonly',true);
		$('#option3').prop('readonly',true);
		$('#correct').prop('disabled',true);
		break;
		case 1:
		$('#option1').prop('readonly',false);
		$('#option2').prop('readonly',false);
		$('#option3').prop('readonly',false);
		$('#correct').prop('disabled',false);
		break;
		default:
		$('#option1').prop('readonly',false);
		$('#option2').prop('readonly',false);
		$('#option3').prop('readonly',false);
		$('#correct').prop('disabled',false);
	}
		});
	$('#update').click(function()
	{
		var id=$('#qsId').val();
		var type=$('#qsType').val();
		var typeText=$('#qsType option:selected').text();
		if(type==1)
		{
		var Qs=$('#question').val();
		var opt1=$('#option1').val();
		var opt2=$('#option2').val();
		var opt3=$('#option3').val();
		var correct=$('#correct').val();
		var correctOption=$('#correct option:selected').text();
		var data="id="+id+"&qs="+Qs+"&opt1="+opt1+"&opt2="+opt2+"&opt3="+opt3+"&correct="+correct+"&type="+type;
		$.ajax({
			type:'POST',
			url:"updateQuestion.php",
			data:data,
			beforeSend: function()
			{
				
			},
			complete: function()
			{
				
			},
			success: function(data)
			{
				if(data==1)
				{
					alert("Success.Question updated successfully.");
					$('#editQS').modal('hide'); 
			

 							$('#qsList').dataTable().fnUpdate(Qs, $('tr#'+id)[0],1);
							$('#qsList').dataTable().fnUpdate(opt1, $('tr#'+id)[0],2);
							$('#qsList').dataTable().fnUpdate(opt2, $('tr#'+id)[0],3);
							$('#qsList').dataTable().fnUpdate(opt3, $('tr#'+id)[0],4);
							$('#qsList').dataTable().fnUpdate("Option &nbsp;"+correct, $('tr#'+id)[0],5);
							
		
				}
				else
				{
					alert("OOPS.Error in updation.Please try again later");
				}
			},
			error: function()
			{
				
			}
			
			
			});
		
		
		
		}
		else
		{
			var Qs=$('#question').val();
			var data="id="+id+"&qs="+Qs+"&type="+type;
			
			$.ajax({
			type:'POST',
			url:"updateQuestion.php",
			data:data,
			beforeSend: function()
			{
				
			},
			complete: function()
			{
				
			},
			success: function(data)
			{
				if(data==1)
				{
					alert("Success.Question updated successfully.");
					$('#editQS').modal('hide'); 
			

 							$('#qsList').dataTable().fnUpdate(Qs, $('tr#'+id)[0],1);
							$('#qsList').dataTable().fnUpdate("", $('tr#'+id)[0],2);
							$('#qsList').dataTable().fnUpdate("", $('tr#'+id)[0],3);
							$('#qsList').dataTable().fnUpdate("", $('tr#'+id)[0],4);
							$('#qsList').dataTable().fnUpdate("", $('tr#'+id)[0],5);
							
		
				}
				else
				{
					alert("OOPS.Error in updation.Please try again later");
				}
			},
			error: function()
			{
				
			}
			
			
			});
			
		}
		//$('#qsList #'+id+' #qs'+id).html(Qs);
		
		//$('#qsList #'+id).remove();
		//$('#qsList #3').hide();
	});
	
	$('.delateQS').click(function(){
		
		var id=$(this).val();
		if(confirm("Are you sure to delete ?"))
		{
		var data="id="+id;
		$.ajax({
			type:'POST',
			url:"deleteQuestion.php",
			data:data,
			beforeSend: function()
			{
				
			},
			complete: function()
			{
				
			},
			success: function(data)
			{
				if(data==1)
				{
					var table = jQuery("#qsList").DataTable();
					table.row('tr#'+id).remove().draw( false );
				}
				else
				{
					alert("OOPS!Something went wrong.Please try again later");
				}
			},
			error: function()
			{
				
			}
			
			
			});
		
		}
		});
		
		$('.showResponse').click(function(){
			$('#viewResponse').modal('show');
			});
});