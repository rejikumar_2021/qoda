<?php

class AppModel extends Database{


		private $_headerEmail="support@qodamobile.com";
		private $_imageBasePath1="http://qodamobile.com/images/layer1.png";
		private $_imageBasePath2="http://qodamobile.com/images/layer2.png";

		public function acceptedsp($email){ 
		$tO=$email;
		$sUbject="Congratulations! Your service provider profile has been approved!";
		$bodY='<center><img src="'.$this->_imageBasePath1.'" class="img-responsive" alt="" /></center>
		<p><b><center><h2>Congratulations! Your service provider profile has been approved!</h2></center></b></p>

		<p><b><center><h2>Start providing services today by logging on to QODA app.</h2> </center></b></p>
		<p><b><center><h2>Tips to get Started :</h2></center></b></p>

		<p><b><center><h2>Make sure you have all the right equipment to complete a service request.</h2> </center></b></p>
		<p><b><center><h2>Respond immediately when service requests pop up around you.
		                </h2> </center></b></p>
		<p><b><center><h2>Text customer about your arrival time once they accept you.</h2></center></b></p>

		<p><b><center>This helps give the customer peace of mind . If you are unable to send a text,</center></b></p>
		  
		<p><b><center> Let us know immediately at support@qodamobile.com. </center></b></p>
		<p><b><center><h2>QODA Mobile, LLC</h2> </center></b></p>
		<p><b><center><h2>San Francisco, CA 94109</h2> </center></b></p>
		<p><b><center><h2><a href="http://qodamobile.com/">qodamobile.com</a></h2> </center></b></p> 
		<p>Sincerely,<br/>
		    QODA Team</p>';
		            $headers  = "MIME-Version: 1.0" . "\r\n";
			  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			  $headers .= 'From: Qoda <'.$this->_headerEmail.'>' . "\r\n";
			  mail($tO,$sUbject,$bodY,$headers); 

		}

		public function rejectedsp($email){ 
		$tO=$email;
		$sUbject="Oh no! Your service provider profile has been rejected!";
		$bodY="<center><img src='".$this->_imageBasePath2."' class='img-responsive' alt='' /></center>
		<p><b><center><h2>Oh no, something went wrong with your Service Provider application.</h2></center></b></p>
		<p><b><center><h2>Don't worry,we'll work hard to fix this.</h2></center></b></p>
		<p><b><center><h2>Please contact support@qodamobile.com to resolve this ASAP.</h2> </center></b></p>
		<p><b><center><h2>QODA Mobile, LLC.</h2> </center></b></p>
		<p><b><center><h2>San Francisco, CA 94109</h2> </center></b></p>
		<p><b><center><h2><a href='mailto:qodamobile@gmail.com'>qodamobile@gmail.com</a></h2> </center></b></p>
		<p>Sincerely,<br/>
		    QODA Team</p>";
		            $headers  = "MIME-Version: 1.0" . "\r\n";
			  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			  $headers .= 'From: Qoda <'.$this->_headerEmail.'>' . "\r\n";
			  mail($tO,$sUbject,$bodY,$headers); 

		}

		public function rejectedCustomer($email){ 
		$tO=$email;
		$sUbject="Oh! Your profile has been deleted!";
		$bodY='<center><img src="'.$this->_imageBasePath1.'" class="img-responsive" alt="" /></center>
		<p><b><center><h2>Oh no, something went wrong! Your profile has been deleted. If you think this </h2></center></b></p>
		<p><b><center><h2> has occurred in error, please 
		                contact us immediately!</h2></center></b></p>

		<p><b><center><h2>Please contact <a href="mailto:support@qodamobile.com">support@qodamobile.com</a> to resolve this ASAP.</h2> </center></b></p>
		<p><b><center><h2>QODA Mobile, LLC</h2> </center></b></p> 
		<p><b><center><h2>San Francisco, CA 94109</h2> </center></b></p>
		<p><b><center><h2> <a href="http://qodamobile.com/">qodamobile.com</a></h2> </center></b></p>
		<p>Sincerely,<br/>
		    QODA Team</p>';
		        $headers  = "MIME-Version: 1.0" . "\r\n";
			  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			  $headers .= 'From: Qoda <'.$this->_headerEmail.'>' . "\r\n";
			  mail($tO,$sUbject,$bodY,$headers); 

		}

		public function chngePassword($adminEmail,$newpassword){ 
		$tO=$adminEmail;
		$sUbject=" Your password has been changed!";
		$bodY='<center><img src="'.$this->_imageBasePath1.'" class="img-responsive" alt="" /></center>
		<p>your new  password is : '.$newpassword.'
		<p>Sincerely,<br/>
		    QODA Team</p>';
		        $headers  = "MIME-Version: 1.0" . "\r\n";
			  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			  $headers .= 'From: Qoda <'.$this->_headerEmail.'>' . "\r\n";
			  mail($tO,$sUbject,$bodY,$headers); 

		}

	function getAdminUser($userName,$userPasswd)
	{
   
   			try {
					$reS=$this->_connection->query("select * from master_admin where adminEmail='".$userName."' AND adminPassword='".$userPasswd."'");
					$num=$reS->rowCount();
					if($num>0){
						$rOw=$reS->fetch(PDO::FETCH_OBJ);
						return $rOw->adminId;
					}else{
						return 0;
					}
			}
			catch (PDOException $e) { parent::close();
		  die("Connection failed in checkEmailExist: " . $e->getMessage());
	        }

	}

	function getREquestServicePerson()
	{
			try 
			{
				$personResUlt=$this->_connection->prepare("select * from spRegistration where spStatus='0'");
				$personResUlt->execute();
				while($rOwW=$personResUlt->fetch(PDO::FETCH_OBJ)){
					$servicePersonId=$rOwW->spId;
				    $aRR['sPId']=$servicePersonId;
					$aRR['sPName']=$rOwW->spUserName;
					$aRR['sPEmail']=$rOwW->spEmail;
					$aRR['sPPhone']=$rOwW->spMobile;
					$aRR['sPPic']=$rOwW->spImage;
					$aRR['sPLicenseNo']=$this->getsPlicense($servicePersonId);
					$aRR['sPWallet']=$rOwW->spWallet;
					$newArr[]=$aRR;
				} if(!$newArr){$newArr=array();} return $newArr;

			}
			catch (PDOException $e) { parent::close();
		  die("Connection failed in getservicePersonList: " . $e->getMessage());
	        }

	}

	function getAcceptServicePerson()
	{
			try 
			{
				$personResUlt=$this->_connection->prepare("select * from spRegistration where spStatus='1'");
				$personResUlt->execute();
				while($rOwW=$personResUlt->fetch(PDO::FETCH_OBJ)){
					$servicePersonId=$rOwW->spId;
				    $aRR['sPId']=$servicePersonId;
					$aRR['sPName']=$rOwW->spUserName;
					$aRR['sPEmail']=$rOwW->spEmail;
					$aRR['sPPhone']=$rOwW->spMobile;
					$aRR['sPPic']=$rOwW->spImage;
					$aRR['sPLicenseNo']=$this->getsPlicense($servicePersonId);
					$aRR['sPWallet']=$rOwW->spWallet;
					$newArr[]=$aRR;
				} if(!$newArr){$newArr=array();} return $newArr;

			}
			catch (PDOException $e) { parent::close();
		  die("Connection failed in getservicePersonList: " . $e->getMessage());
	        }

	}

	function getsPlicense($servicePersonId)
	{
		try 
		{
			$reSUlt=$this->_connection->prepare("select * from  spVehicleInfo where spId='".$servicePersonId."'");
			$reSUlt->execute();
			$rOw=$reSUlt->fetch(PDO::FETCH_OBJ);
			$sPlicense=$rOw->spVhLicenseNo;
			return $sPlicense;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in getservicePersonList: " . $e->getMessage());
	        }

	}

	function getParticularSvPerson($sPId)
	{   
		try 
		{
			$sPResUlt=$this->_connection->prepare("select * from spRegistration where spId='".$sPId."'");
			$sPResUlt->execute();
			while($rOW=$sPResUlt->fetch(PDO::FETCH_OBJ)){
				$aRR['sPName']=$rOW->spUserName;
				$aRR['sPEmail']=$rOW->spEmail;
				$aRR['sPPhone']=$rOW->spMobile;
				$aRR['sPWallet']=$rOW->spWallet;
				$aRR['sPPic']=$rOW->spImage;
				$newArr[]=$aRR;
				
			} if(!$newArr){$newArr=array();} return $newArr;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in getservicePersonList: " . $e->getMessage());
	        }
	}

	function getSvPersonVehicle($sPId)
	{   
		try 
		{
			$sPVehResUlt=$this->_connection->prepare("select * from spVehicleInfo where spId='".$sPId."'");
			$sPVehResUlt->execute();
			while($rOW=$sPVehResUlt->fetch(PDO::FETCH_OBJ)){
				$aRR['sPVehLicense']=$rOW->spVhLicenseNo;
				$aRR['sPVehmODEL']=$rOW->spVhModelNo;
				$aRR['sPVehYEAR']=$rOW->spVhYear;
				$aRR['sPVehPic']=$rOW->spVhImage;
				$newArr[]=$aRR;
				
			} if(!$newArr){$newArr=array();} return $newArr;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in getservicePersonList: " . $e->getMessage());
	        }
	}

	function updateSPStatus($sPTId)
	{
		try 
		{
			$sPStResUlt=$this->_connection->prepare("select * from spRegistration where spId='".$sPTId."'");
			$sPStResUlt->execute();
			$rOW=$sPStResUlt->fetch(PDO::FETCH_OBJ);
			$sPstatus=$rOW->spStatus;
			$this->acceptedsp($rOW->spEmail);
			if($sPstatus==0)
			{
				$this->_connection->query("update spRegistration set spStatus='1' where spId='".$sPTId."'");	
			}
			
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in UpdateSTATUS: " . $e->getMessage());
	        }

	}

	function rejectRequest($sPtId)
	{
		try 
		{
			$sPStResUlt=$this->_connection->prepare("select * from spRegistration where spId='".$sPtId."'");
			$sPStResUlt->execute();
			$rOW=$sPStResUlt->fetch(PDO::FETCH_OBJ);
			$sPstatus=$rOW->spStatus;
			$this->rejectedsp($rOW->spEmail);
			$this->_connection->query("delete from spRegistration  where spId='".$sPtId."'");
			$this->_connection->query("delete from spVehicleInfo  where spId='".$sPtId."'");
			return 1;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in UpdateSTATUS: " . $e->getMessage());
	        }
	}


	function getServiceCost()
	{
		try 
		{
			$sCostResUlt=$this->_connection->prepare("select * from services");
			$sCostResUlt->execute();
			while($rOW=$sCostResUlt->fetch(PDO::FETCH_OBJ)){
				$aRR['serviceName']=$rOW->service_name;
				$aRR['serviceCharge']=$rOW->service_charges;
				$aRR['serviceId']=$rOW->id;
				$newArr[]=$aRR;
				
			} if(!$newArr){$newArr=array();} return $newArr;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in ChangeSErviceCost: " . $e->getMessage());
	        }

	}

	function getAppSettingsServices()
	{
		try 
		{
			$appResUlt=$this->_connection->prepare("select * from app_settings");
			$appResUlt->execute();
			while($rOW=$appResUlt->fetch(PDO::FETCH_OBJ)){
				$aRR['appMargin']=$rOW->admin_margin;
				$aRR['appTravelCoST']=$rOW->travel_cost;
				$aRR['appId']=$rOW->id;
				$newArr[]=$aRR;
				
			} if(!$newArr){$newArr=array();} return $newArr;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in ChangeSErviceCost: " . $e->getMessage());
	        }

	}

	function updateServiceCost($serviceId,$serviceCharges)
	{
		try 
		{
			$this->_connection->query("update services set service_charges='".$serviceCharges."' where id='".$serviceId."'");	
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in UpdatingSErviceCost: " . $e->getMessage());
	        }
	}

	function updateappPrice($appId,$appValue,$appType)
	{
		try 
		{
				if($appType=='transport_price')
				{
					$this->_connection->query("update app_settings set travel_cost='".$appValue."' where id='".$appId."'");
				}
				if($appType=='app_owner_price')
				{
					$this->_connection->query("update app_settings set admin_margin='".$appValue."' where id='".$appId."'");
				}
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in UpdatingAppPrice: " . $e->getMessage());
	        }
	}

	function getRadiusRange()
	{
		try 
		{
			$radResUlt=$this->_connection->prepare("select * from app_settings");
			$radResUlt->execute();
			while($roOW=$radResUlt->fetch(PDO::FETCH_OBJ)){
				$aRR['radInrange']=$roOW->radious_in_range;
				$aRR['radOutrange']=$roOW->radious_out_range;
				$aRR['radId']=$roOW->id;
				$newArr[]=$aRR;
				
			} if(!$newArr){$newArr=array();} return $newArr;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in ChangeSErviceCost: " . $e->getMessage());
	        }
	}

	function updateappRadius($appId,$appValue,$appType)
	{
		try 
		{
				if($appType=='in_range')
				{
					$this->_connection->query("update app_settings set radious_in_range='".$appValue."' where id='".$appId."'");
				}
				if($appType=='out_range')
				{
					$this->_connection->query("update app_settings set radious_out_range='".$appValue."' where id='".$appId."'");
				}
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in UpdatingAppPrice: " . $e->getMessage());
	        }
	}

	function uploadContract($personTypeId,$cfile)
	{
		try 
		{
			$this->_connection->query("update personcontract set personcontractfile='".$cfile."' where personId='".$personTypeId."'");
			return 1;	
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in UpdatingAppPrice: " . $e->getMessage());
	        }
	}

	function getUserInformation()
	{
			try 
			{
				$userResUlt=$this->_connection->prepare("select * from userRegistration where is_Deleted='0'");
				$userResUlt->execute();
				while($rowW=$userResUlt->fetch(PDO::FETCH_OBJ)){
					$userId=$rowW->userId;
				    $aRR['uSerId']=$userId;
					$aRR['uSerName']=$rowW->userName;
					$aRR['uSerEmail']=$rowW->userEmail;
					$aRR['uSerPhone']=$rowW->userMobile;
					$aRR['uSerPic']=$rowW->userImage;
					$aRR['uservehInfo']=$this->getuserVehINfo($userId);
					$newArr[]=$aRR;
				} if(!$newArr){$newArr=array();} return $newArr;

			}
			catch (PDOException $e) { parent::close();
		  die("Connection failed in getUserList: " . $e->getMessage());
	        }

	}

	function getuserVehINfo($userId)
	{
		try 
			{
				$uservehres=$this->_connection->prepare("select * from  userVehicleInfo where userId='".$userId."'");
				$uservehres->execute();
				$rOww=$uservehres->fetch(PDO::FETCH_OBJ);
				$userVeh['userVehModel']=$rOww->uvModel;
				$userVeh['userVehYear']=$rOww->uvYear;
				$userVeh['userVehLicense']=$rOww->uvLicenseNo;
				$userVeh['userVehPic']=$rOww->uvImage;
				if(!$userVeh){$userVeh=array();} return $userVeh;
			}
			catch (PDOException $e) { parent::close();
		  die("Connection failed in getUserList: " . $e->getMessage());
	        }
	}

	function deleteUser($uId)
	{
		try 
			{
			
				$usrResUlt=$this->_connection->prepare("select * from userRegistration where userId='".$uId."'");
				$usrResUlt->execute();
				$spResUlt=$this->_connection->prepare("select srId from serviceRespons where userId='".$uId."' AND (srStatus='1' OR srStatus='2')");
				$spResUlt->execute();
				$cOuNtT = $spResUlt->rowCount();
				if($cOuNtT>0){
					while($sPrOW=$spResUlt->fetch(PDO::FETCH_OBJ))
						{
						$this->customerRejectJobparticular($uId,$sPrOW->srId);
						}
				}

				$urOW=$usrResUlt->fetch(PDO::FETCH_OBJ);
				$this->rejectedCustomer($urOW->userEmail);
				$this->deleteUserNotification($uId);
				$this->_connection->query("UPDATE userRegistration SET is_Deleted='1' WHERE userId='".$uId."' ");
			//$this->_connection->query("delete from userRegistration  where userId='".$uId."'");
				//$this->_connection->query("delete from userVehicleInfo  where userId='".$uId."'");
				//$this->_connection->query("delete from serviceRequest  where userId='".$uId."'");
				return 1;
			}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in getUserList: " . $e->getMessage());
		        }
	}

	private function customerRejectJobparticular($userId,$serviceResponseId){      // Customer REjected Job
			try{ 

		$queryRes=$this->_connection->query("SELECT * FROM serviceRespons where userId='".$userId."' AND  srId='".$serviceResponseId."' ");
		 $cOuNt = $queryRes->rowCount();
	
			    if($cOuNt>0){
				  $getRow=$queryRes->fetch(PDO::FETCH_OBJ);
				$sPid=$getRow->spId;
					
				 $tRavlingCost=$getRow->travelCost;
				 $queryResS=$this->_connection->query("SELECT * FROM serviceRespons where userId='".$userId."' AND  srId='".$serviceResponseId."' AND  srStatus='2'");
	              		$cOuNtT = $queryResS->rowCount();

				 if($cOuNtT==1){
					$this->_connection->query("update spRegistration set   sp_availability='0',spWallet=spWallet+$tRavlingCost   where spId='".$sPid."' ");
					$this->_connection->query("update userRegistration set userWallet=userWallet-$tRavlingCost where userId='".$userId."'");	
					//send notification 
					$this->jobCancelSpNotification($sPid,$serviceResponseId);
				 }else{
					
					$queryResSr=$this->_connection->query("SELECT * FROM serviceRespons where userId='".$userId."' AND  srId='".$serviceResponseId."' AND  srStatus='1'");
	              			 $srcOuNtT = $queryResSr->rowCount();
						
					if($srcOuNtT==1){
					
					$queryResS=$this->_connection->query("UPDATE spRegistration SET sp_availability='0' WHERE spId='".$sPid."' ");
					
					//send notification 
					$this->jobCancelSpNotification($sPid,$serviceResponseId);
				 }
			

				}
 				 $this->_connection->query("update  serviceRespons  set srStatus='0' where userId='".$userId."' AND  srId='".$serviceResponseId."'  ");
					return array('status'=>$this->_success);
			} 	return array('status'=>$this->_error,'message'=>$this->_unexceptionError);
			}catch (PDOException $e) { parent::close();
		  die("Connection failed in customerRejectedJobBefore: " . $e->getMessage());
	        }	
	}
	
	 private function jobCancelSpNotification($spId,$serviceId){
		 try{
				
				
				$userInfo=$this->_connection->query("select * from spRegistration where spId='".$spId."' ");
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
					$deIceType=$roW->spDeviceType;
					$deIceTokEN=$roW->spDeviceToken;
					if($deIceType=='Andriod'){
						
					}else{
					$appObj=new Device_Notification();
 					$respOnse=$appObj->sendPushpopup($deIceTokEN,'Job has been cancelled by customer','cancelByUser',$serviceId);
		 			}
				}
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in jobCancelSpNotification: " . $e->getMessage());
	        }	
	}


	private function deleteUserNotification($uId){
		 try{
		
				$userInfo=$this->_connection->query("select * from userRegistration where userId='".$uId."' ");
				while($roW=$userInfo->fetch(PDO::FETCH_OBJ)){
					$deIceType=$roW->devcieType;
					$deIceTokEN=$roW->deviceToken;
					if($deIceType=='Andriod'){
						
					}else{
					$appObj=new Device_Notification();
 					$respOnse=$appObj->sendPushpopup($deIceTokEN,'Your account has been deleted by admin','deletedUser',$uId);
					
		 			}
				}
				
		}catch (PDOException $e) { parent::close();
		  die("Connection failed in deleteUserNotification: " . $e->getMessage());
	        }	
	}

	function getSErviceDetails()
	{

		try
		{
			$st=$this->_connection->prepare("select * from serviceRespons where srStatus='4'");
			$st->execute();
			while($srOW=$st->fetch(PDO::FETCH_OBJ))
			{
				$srId=$srOW->srId;
				$userId=$srOW->userId;
				$aRR['serviceRating']=$this->getserviceRating($srId,$userId);
				$aRR['custName']=$this->getCustomerName($userId);
				$serviceId=$srOW->serviceId;
				$aRR['serviceTypeName']=$this->getServiceType($serviceId);
				$spId=$srOW->spId;
				$aRR['servicepersonName']=$this->getServiceName($spId);
				$newArr[]=$aRR;
			} if(!$newArr){$newArr=array();} return $newArr;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function getserviceRating($srId,$userId)
	{
		try
		{
			$rt=$this->_connection->prepare("select * from rating where srId='".$srId."' and userId='".$userId."'");
			$rt->execute();
			$rrOW=$rt->fetch(PDO::FETCH_OBJ);
			$srating['custFeedback']=$rrOW->comment;
			$srating['custRating']=$rrOW->rating;
			if(!$srating){$srating=array();} return $srating;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function getCustomerName($userId)
	{
		try
		{
			$urt=$this->_connection->prepare("select * from userRegistration where userId='".$userId."'");
			$urt->execute();
			$usrOW=$urt->fetch(PDO::FETCH_OBJ);
			$customerName=$usrOW->userName;
			return $customerName;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function getServiceType($serviceId)
	{
		try
		{
			$srt=$this->_connection->prepare("select * from serviceRequest where serviceId='".$serviceId."'");
			$srt->execute();
			$srvOW=$srt->fetch(PDO::FETCH_OBJ);
			$serviceType=$srvOW->serviceType;
			$srTt=$this->_connection->query("select * from services where id='".$serviceType."'");
			$svOW=$srTt->fetch(PDO::FETCH_OBJ);
			return $svOW->service_name;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function getServiceName($spId)
	{
		try
		{
			$srn=$this->_connection->prepare("select * from spRegistration where spId='".$spId."'");
			$srn->execute();
			$snrOW=$srn->fetch(PDO::FETCH_OBJ);
			$servicePersonname=$snrOW->spUserName;
			return $servicePersonname;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function getopenCaseResolved()
	{
		try
		{
			$opst=$this->_connection->prepare("select * from dispute_cases");
			$opst->execute();
			while($oprOW=$opst->fetch(PDO::FETCH_OBJ))
			{
				$srId=$oprOW->srId;
				$userId=$oprOW->userId;
				$spId=$oprOW->spId;
				$aRR['disputeStatus']=$oprOW->disputeStatus;
				$aRR['disputeID']=$oprOW->disputeId;
				$aRR['userReason']=$oprOW->userReason;
				$aRR['serviceTCost']=$this->getServiceallCost($srId);
				$newArr[]=$aRR;
			} if(!$newArr){$newArr=array();} return $newArr;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in ResolvedOPencase: " . $e->getMessage());
		        }
	}

	function getCustomerInfo($userId)
	{
		try
		{
			$urst=$this->_connection->prepare("select * from userRegistration where userId='".$userId."'");
			$urst->execute();
			$usrOW=$urst->fetch(PDO::FETCH_OBJ);
			$custInfo['customrName']=$usrOW->userName;
			$custInfo['customrEmail']=$usrOW->userEmail;
			$custInfo['customrPhone']=$usrOW->userMobile;
			$custInfo['customrPic']=$usrOW->userImage;
			if(!$custInfo){$custInfo=array();} return $custInfo;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function getServicePersonINfo($spId)
	{
		try
		{
			$srnp=$this->_connection->prepare("select * from spRegistration where spId='".$spId."'");
			$srnp->execute();
			$sPrOW=$srnp->fetch(PDO::FETCH_OBJ);
			$sPInfo['servicePName']=$sPrOW->spUserName;
			$sPInfo['servicePEmail']=$sPrOW->spEmail;
			$sPInfo['servicePPhone']=$sPrOW->spMobile;
			if(!$sPInfo){$sPInfo=array();} return $sPInfo;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function getServiceallCost($srId)
	{
		try
		{
			$srnp=$this->_connection->prepare("select * from serviceRespons where  srId='".$srId."'");
			$srnp->execute();
			$sCrOW=$srnp->fetch(PDO::FETCH_OBJ);
			
				$sCostInfo['svtravelCost']=$sCrOW->travelCost;
				$sCostInfo['svTotalCost']=$sCrOW->totalCost;
				$sCostInfo['svServiceCost']=$sCrOW->serviceCost;
				$sCostInfo['svPercentCost']=$sCrOW->afterPercentCost;
				$serviceID=$sCrOW->serviceId;
				$sCostInfo['sserviceType']=$this->getServiceType($serviceID);
				
			 if(!$sCostInfo){$sCostInfo=array();} return $sCostInfo;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in SErviceDEtails: " . $e->getMessage());
		        }
	}

	function deletedisputeCase($dispId)
	{
		try {
				$this->_connection->query("update dispute_cases set disputeStatus='1' where disputeId='".$dispId."'");
				return 1;
			}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in getUserList: " . $e->getMessage());
		        }
	}

	function changePassword($adminID,$newpasswd)
	{
		try 
		{
			$this->_connection->query("update master_admin set adminPassword='".$newpasswd."' where adminId='".$adminID."'");
			return 1;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in userLOgin: " . $e->getMessage());
		        }

	}

	function forgetpassword($userEmail)
	{
		try 
		{
			$reS=$this->_connection->query("select * from master_admin where adminEmail='".$userEmail."'");
	 	$num=$reS->rowCount();

			if($num>0){
				$newpassword=strtotime(date("Y-m-d h:i:s"));
		 		$this->_connection->query("update master_admin set adminPassword='".$newpassword."' where adminEmail='".$userEmail."'");
				$this->chngePassword($userEmail,$newpassword);
				return 1;
			}else{
				return 0;
			}
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in userLOgin: " . $e->getMessage());
		        }

	}

	function getopenCasedetails($dispid)
	{
		try 
		{
			$dispREs=$this->_connection->prepare("select * from dispute_cases where disputeId='".$dispid."'");
			$dispREs->execute();
			$dsrOW=$dispREs->fetch(PDO::FETCH_OBJ);
			$uSERid=$dsrOW->userId;
			$opencaseInfo['cuStInfo']=$this->getCustomerInfo($uSERid);
			$spId=$dsrOW->spId;
			$opencaseInfo['SPInfo']=$this->getServicePersonINfo($spId);
			$opencaseInfo['userComments']=$dsrOW->userComments;
			$opencaseInfo['spResponse']=$dsrOW->spResponse;
			$opencaseInfo['userReason']=$dsrOW->userReason;
			$opencaseInfo['disputeStatus']=$dsrOW->disputeStatus;
			$srId=$dsrOW->srId;
			$opencaseInfo['serviceResponse']=$this->getServiceallCost($srId);
			if(!$opencaseInfo){$opencaseInfo=array();} return $opencaseInfo;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in getopenCasedetails: " . $e->getMessage());
	        }
	}

	function disputeCase($dispid)
	{
		try 
		{
			$this->_connection->query("update dispute_cases set disputeStatus='1' where disputeId='".$dispid."'");
			return 1;
		
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in disputeCase: " . $e->getMessage());
	        }	
	}

	function resolvedIsputecase($dispiD)
	{
		try 
		{
		


			$disREs=$this->_connection->query("select * from dispute_cases where disputeId='".$dispiD."' ");
			$drOW=$disREs->fetch(PDO::FETCH_OBJ);
			$srId=$drOW->srId;
			$srnp=$this->_connection->query("select * from serviceRespons where  srId='".$srId."'");
			$sCrOW=$srnp->fetch(PDO::FETCH_OBJ);
		 	$serviceCost=$sCrOW->serviceCost;
			$afterPercentCost=$sCrOW->afterPercentCost;
				$spId=$sCrOW->spId;
				$userId=$sCrOW->userId;
				$this->_connection->query("update dispute_cases set disputeStatus='1',refund_status='1' where disputeId='".$dispiD."'");
			$this->_connection->query("update spRegistration set spWallet=spWallet-$afterPercentCost where spId='".$spId."'");
		 	$this->_connection->query("update userRegistration set userWallet=userWallet+$serviceCost where userId='".$userId."' ");
  			return 1;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in resolvedIsputecase: " . $e->getMessage());
	        }	

	}
	
	function blockServiceProvider($sPid){
		try 
		{
			 $this->_connection->query("update spRegistration set spbLockStatus='1' where spId='".$sPid."' ");
			return 1;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in blockSpProvider: " . $e->getMessage());
	        }	
		
	}

	function unblockServiceProvider($sPid){
			try 
		{
		 	$this->_connection->query("update spRegistration set spbLockStatus='0' where spId='".$sPid."' ");
			return 1;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed in blockSpProvider: " . $e->getMessage());
	        }	

	}


}

?>
