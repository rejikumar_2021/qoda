<?php
//0 for Service Provider & 1 for customer
include('dashboard.php');
$obj=new AppModel_update();
$data=$obj->getQlist();
?>
<style>
a:link {
  text-decoration: none;
}
.customBtn
{
	width: 85px;
    margin-bottom: 10px;
}
.customSpn
{
	margin-left: 5px;
}
</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">


 <!-- Modal -->
  <div class="modal fade" id="editQS" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Question</h4>
        </div>
        <div class="modal-body">
          <form action="#" method="post">
                         <input type="hidden" name="id" id="qsId"/>   
                            <div class="form-group">
                            <label for="contraCt" style="font-size:17px;">Question Type</label>
                            <select  class='form-control' name="qsType" id="qsType" required>
                                    <option value=''>---select---</option>
                                    <option value='2'>Descriptive</option>
                                    <option value='1'>Objective</option>
                                  </select>
                             </div>
                       
                             
                             <div class="form-group">
                            <label for="file" style="font-size:17px;">Question</label>
                            <textarea name="question" class="form-control" id="question" required></textarea>
                            </div>
                            
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Option 1</label>
                           <input type="text" name="option1" class="form-control" required id="option1">
                            </div>
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Option 2</label>
                           <input type="text" name="option2" class="form-control" required id="option2">
                            </div>
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Option 3</label>
                           <input type="text" name="option3" class="form-control" required id="option3">
                            </div>
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Correct Option</label>
                           <select  class='form-control' name="correct" required id="correct">
                           <option value="">---select---</option>
                           <option value="1">Option 1</option>
                           <option value="2">Option 2</option>
                           <option value="3">Option 3</option>
                           </select>
                            </div>
                           
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-md btn-success" name="update" id="update">Update</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-eye"></i>  View Questions
                                </li>
                            </ol>
                        </div>
        <a href="uploadQuestion.php" class="btn btn-md btn-success customBtn">Add<span class="fa fa-plus fa-lg customSpn"></span></a>
<table id="qsList" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>SlNo</th>
							<th>Question</th>
							<th>Option1</th>
							<th>Option2</th>
							<th>Option3</th>
                            <th>Answer</th>
                            <th>Action</th>
						</tr>
					</thead>
					
					<tbody>
                    <?php
					$i=1;
					foreach($data as $d)
					{
					?>
						<tr id="<?php echo $d['id']; ?>">
							<td><?php echo $i; ?></td>
							<td id="qs<?php echo $d['id'] ?>"><?php echo $d['question']; ?></td>
							<td id="op1<?php echo $d['id'] ?>"><?php echo $d['option1']; ?></td>
							<td id="op2<?php echo $d['id'] ?>"><?php echo $d['option2']; ?></td>
							<td id="op3<?php echo $d['id'] ?>"><?php echo $d['option3']; ?></td>
							<td id="cor<?php echo $d['id'] ?>"><?php if($d['type']==1){echo "Option &nbsp;".$d['correct'];} ?></td>
                            <td>
                            <a href="javascript:editQues(<?php echo $d['id']; ?>)" ><button>Edit</button></a>
                          <!-- <button class="editQS" value="<?php //echo $d['id']; ?>">Edit</button>-->
                            <button class="delateQS" value="<?php echo $d['id']; ?>">Delete</button>
                            </td>
						</tr>
						
						<?php
						++$i;
					}
					?>
							
					</tbody>
				</table>



<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#qsList').DataTable();
} );
</script>
<script src="js/editQuestion.js"></script>
<?php include('footer.php')?>