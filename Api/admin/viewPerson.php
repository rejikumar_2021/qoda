<?php include('dashboard.php'); 
$sPId=$_GET['sPId'];
if(empty($sPId)){
echo "<script>window.location='RequestServicePerson.php';</script>";

}

$getSpObj=new AppModel();
$reSULt=$getSpObj->getParticularSvPerson($sPId);
$vehreSULt=$getSpObj->getSvPersonVehicle($sPId);
?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    	<div class="brEad">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i>  View User Details
                            </li>
                        </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">View User Details</span>
                        </h3>
                         <div class="panel panel-primary">
					
                    <div class="panel panel-info" style="margin-bottom:0px !important;">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo  $reSULt[0]['sPName'];  ?></h3>
            </div>
            <div class="panel-body">
              <div class="row">

                <?php if(!empty($reSULt[0]['sPPic'])) { ?>
                  <div class="col-md-3 col-lg-3 " align="center"> 
                  <img alt="User Pic" src="<?php echo BASEPATH.$reSULt[0]['sPPic']; ?>" class="img-circle img-responsive"> </div>
                  <?php }else{ ?>
                  <div class="col-md-3 col-lg-3 " align="center"> 
                  <img alt="User Pic" src="images/usernoProfile.png" class="img-circle img-responsive">
                  </div>
                  <?php } ?>
                
                <div class=" col-md-9 col-lg-9 "> 
                <h4 style="color: #1482B9;"><span class="menu-title">User Info:</span></h4>
                  <table class="table table-user-information">
                    <tbody>
                     <tr>
                        <th>Email:</th>
                        <td><a href=""><?php echo  $reSULt[0]['sPEmail'];  ?></a></td>
                      </tr>
                        <th>Phone Number:</th>
                        <td><?php echo  $reSULt[0]['sPPhone'];  ?>
                        </td>
                           
                      </tr>
                      <tr>
                        <th>User Wallet:</th>
                        <td><?php echo  $reSULt[0]['sPWallet'];  ?></td>
                      </tr>
                       </tbody>
                  </table>
                  <br><br>
                  <h4 style="color: #1482B9;"><span class="menu-title">Vehicle Info:</span></h4>
                    <table class="table table-user-information">
                    <tbody>
                     <tr>
                        <th>License No.:</th>
                        <td><?php echo  $vehreSULt[0]['sPVehLicense'];  ?></td>
                      </tr>
                      <tr>
                        <th>Model No.:</th>
                        <td><?php echo  $vehreSULt[0]['sPVehmODEL'];  ?></td>
                      </tr>
                      <tr>
                        <th>Year:</th>
                        <td><?php echo  $vehreSULt[0]['sPVehYEAR'];  ?></td>
                      </tr>
                      <tr>
                        <th>Vehicle's Image:</th>
		<?php if(!empty($vehreSULt[0]['sPVehPic'])) { ?>
                        <td><img alt="Vehicle Pic" src="<?php echo BASEPATH.$vehreSULt[0]['sPVehPic']; ?>" class="img-responsive" width="150px" height="150px"></td>
		 <?php }else{ ?>
		<td><img alt="Vehicle Pic" src="images/placeholder.png" class="img-responsive" width="150px" height="150px"></td>
		<?php } ?>
                      </tr>
                    </tbody>
                  </table>
                  
                  
                </div>
              </div>
            </div>
                
					
                    </div>
                </div>
                <!-- /.row -->
        
            </div> 

<script>
$(document).ready(function() {
    $('#examplee').DataTable();
} );

</script>


<?php include('footer.php'); ?>
