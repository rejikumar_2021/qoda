<?php
//0 for Service Provider & 1 for customer
include('dashboard.php'); 

$obj=new AppModel_update();
$video=$obj->getCurrentVideo();
if(empty($video[0]['sp_video']))
{
	$status=1;
}

if($_POST['submit'])
{
	$type=$_POST['qsType'];
	if(!empty($type))
	{
		if($type==2) // descriptive
		{
			$question=$_POST['question'];
			if($question=="")
			{
				$error="Please enter the question";
			}
			else
			{
				$insert=$obj->insertDquestion($question);
				if($insert==true)
				{
					?>
                    <script>
                    window.location="viewQuestions.php";
                    </script>
                    <?php
					//$success="Question added successfully";
				}
				else
				{
					$error="OOPS!Something went wrong.Question not added";
				}
			}
		}
		else // Objective
		{
			$question=$_POST['question'];
			$opt1=$_POST['option1'];
			$opt2=$_POST['option2'];
			$opt3=$_POST['option3'];
			$correct=$_POST['correct'];
			if($question==""||$opt1==""||$opt2==""||$opt3==""||$correct=="")
			{
				//echo $question;
				$error="All fields are mandatory";
			}
			else
			{
				$insert=$obj->insertOquestion($question,$opt1,$opt2,$opt3,$correct);
				if($insert==true)
				{
					?>
                     <script>
                    window.location="viewQuestions.php";
                    </script>
					
                    <?php
					//$success="Question added successfully";
				}
				else
				{
					$error="OOPS!Something went wrong.Question not added";
				}
			}
			
		}
	}
	else
	{
		$error="Please select question type";
	}
}
?>
<script>
$(function(){
	$('#qsType').change(function(){
		
	var ch=parseInt($(this).val());
	switch(ch)
	{
		case 2:
		$('#option1').prop('readonly',true);
		$('#option2').prop('readonly',true);
		$('#option3').prop('readonly',true);
		$('#correct').prop('disabled',true);
		break;
		case 1:
		$('#option1').prop('readonly',false);
		$('#option2').prop('readonly',false);
		$('#option3').prop('readonly',false);
		$('#correct').prop('disabled',false);
		break;
		default:
		$('#option1').prop('readonly',false);
		$('#option2').prop('readonly',false);
		$('#option3').prop('readonly',false);
		$('#correct').prop('disabled',false);
	}
		});
});

</script>
<style>
.success
{
	border: 1px solid #1482b9;
    padding: 6px;
    border-radius: 5px;
    background-color: #138ac5;
    color: #fff;
}
.error
{
	border: 1px solid #e6174b;
    padding: 6px;
    border-radius: 5px;
    background-color: #e24456;
    color: #fff;
}
.custom
{
	border: 1px solid #1aa9f1;
    background-color: #1482b9;
    color: #fff;
}
.custom a
{
	color: #f3f3f3;
    font-weight: bold;
}
</style>
<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row ">
                 <div class="col-lg-12">
                 	<div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-question"></i>  Set Questionnaire
                                </li>
                            </ol>
                        </div>
                        
                <div class="col-md-9 col-lg-8">
                
                <?php
				if($status==1)
				{
					?>
                    <span class="alert custom">Please upload a video before uploading questions.<a href="uploadVideo.php">Upload now</a></span>
                    <?php
				}
				else
				{
				?>
                    	 <?php
						  if(isset($success))
						  {
							  ?>
                              <h4 class="success">
							  <?php echo $success;?>
                              </h4>
                              <?php
						  }
						  if(isset($error))
						  {
							  ?>
                              <h4 class="error">
							 <?php echo $error; ?>
                             </h4>
                             <?php
						  }
						  ?>
                            <form action="#" method="post">
                            
                            <div class="form-group">
                            <label for="contraCt" style="font-size:17px;">Question Type</label>
                            <select  class='form-control' name="qsType" id="qsType" required>
                                    <option value=''>---select---</option>
                                    <option value='2'>Descriptive</option>
                                    <option value='1'>Objective</option>
                                  </select>
                             </div>
                       
                             
                             <div class="form-group">
                            <label for="file" style="font-size:17px;">Question</label>
                            <textarea name="question" class="form-control" required></textarea>
                            </div>
                            
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Option 1</label>
                           <input type="text" name="option1" class="form-control" required id="option1">
                            </div>
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Option 2</label>
                           <input type="text" name="option2" class="form-control" required id="option2">
                            </div>
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Option 3</label>
                           <input type="text" name="option3" class="form-control" required id="option3">
                            </div>
                            
                            <div class="form-group">
                            <label for="file" style="font-size:17px;">Correct Option</label>
                           <select  class='form-control' name="correct" required id="correct">
                           <option value="">---select---</option>
                           <option value="1">Option 1</option>
                           <option value="2">Option 2</option>
                           <option value="3">Option 3</option>
                           </select>
                            </div>
                            
                            <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit" value="SET">
                            </div>
                            </form>
                         
                    	</div>
                     
                       </div>
                	</div>
                   </div>
                 </div>
                 <?php
				}
				?>


<?php include('footer.php')?>

