 
<?php 
include('dashboard.php');

include('updateServiceCost.php');

$neWObj=new AppModel();
$resUlt=$neWObj->getServiceCost();
$res=$neWObj->getAppSettingsServices();
?>
 <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    <div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i>  Price Matrix
                                </li>
                            </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">App Settings</span>
                        </h3>
                        <div class="panel panel-primary">
					
					
					<table class="table tabSetting" id="dev-table">
						<thead>
							<tr>
								
								<th class="tabHead">Charges Type</th>
								<th class="tabHead">Charges</th>
								<th class="tabHead">Edit</th>
							</tr>
                            <tr></tr>
						</thead>
                        
						<tbody>
					  <?php $j=1;  for($i=0;$i<sizeOf($resUlt);$i++,$j++){ ?>
						<tr>
							<th><?php echo $resUlt[$i]['serviceName']; ?> </th>
							<td>$<input type ="text" id="servicedata_<?php echo $resUlt[$i]['serviceId']; ?>"  value="<?php echo $resUlt[$i]['serviceCharge']; ?>" readonly/></td>
							<td id ="edit_<?php echo $resUlt[$i]['serviceId']; ?>">
                            	<a onClick="edit_cost(<?php echo $resUlt[$i]['serviceId']; ?>)" class="btn  btn-primary" ><i class="fa fa-pencil"></i> Edit</a>
                            </td>
                   			<td id ="save_<?php echo $resUlt[$i]['serviceId']; ?>" style="display:none;">
                            	<a onClick="save_cost(<?php echo $resUlt[$i]['serviceId']; ?>)" class="btn  btn-primary"><i class="fa fa-pencil"></i> Save</a>
                            </td>
						</tr>
							
					<?php } 
            $m=1;  for($n=0;$n<sizeOf($res);$n++,$m++){
          ?>
            <tr>
                          <th>App Owner's Margin</th>
              <td>%<input type ="text" id="app_owner_text_<?php echo $res[$n]['appId']; ?>"  value="<?php echo $res[$n]['appMargin']; ?> " readonly/></td>
              <td id ="edit_app_owner_<?php echo $res[$n]['appId']; ?>"> 
                              <a onClick="edit_price(<?php echo $res[$n]['appId']; ?>,'app_owner_price')" class="btn  btn-primary" ><i class="fa fa-pencil"></i> Edit</a>
                            </td>
                            <td id ="save_app_owner_<?php echo $res[$n]['appId']; ?>" style="display:none;">
                              <a onClick="save_price(<?php echo $res[$n]['appId']; ?>,'app_owner_price')" class="btn  btn-primary"><i class="fa fa-pencil"></i> Save</a>
                            </td>
            </tr>
                            
                        <tr>
                          <th>Travel Cost    (Per Mile)</th>
              <td>$<input type ="text" id="transport_text_<?php echo $res[$n]['appId']; ?>"  value="<?php echo $res[$n]['appTravelCoST']; ?>" readonly/></td>
              <td id ="edit_transport_<?php echo $res[$n]['appId']; ?>">  
                              <a onClick="edit_price(<?php echo $res[$n]['appId']; ?>,'transport_price')" class="btn  btn-primary" ><i class="fa fa-pencil"></i> Edit</a>
                             </td>
                             <td id ="save_transport_<?php echo $res[$n]['appId']; ?>" style="display:none;">
                              <a onClick="save_price(<?php echo $res[$n]['appId']; ?>,'transport_price')" class="btn  btn-primary"><i class="fa fa-pencil"></i> Save</a>
                             </td>
                         </tr>
                    <?php }?>
				</tbody>
			</table>
		</div>
     </div>
   </div>
 <!-- /.row -->
        
</div> 
<script type="text/javascript">

function edit_cost(id){
      $("#servicedata_"+id).attr("readonly", false);
      $("#save_"+id).css("display", "block");
      $("#edit_"+id).css("display", "none");
   
 }



function edit_price(id,type){
    
    if(type=='transport_price'){
         $("#transport_text_"+id).attr("readonly", false);
         $("#save_transport_"+id).css("display", "block");
         $("#edit_transport_"+id).css("display", "none");
        
    }
    if(type=='app_owner_price'){
         $("#app_owner_text_"+id).attr("readonly", false);
         $("#save_app_owner_"+id).css("display", "block");
         $("#edit_app_owner_"+id).css("display", "none");
    }
    
       
}

function isInteger(x) {
        return x % 1 === 0;
    }

function save_price(id,type){
    
        if(type=='transport_price'){
             var val = $("#transport_text_"+id).val();
            
           }
    if(type=='app_owner_price'){
        var val = $("#app_owner_text_"+id).val();
        
    }
  
  if(!isNaN(val)){
                if(isInteger(val)==false){
          
                        var num = val.toString().split('.')[1].length
                        if(num > 2){
                            alert('Value should not be greator than 2 decimal places');
                            return false;
                        }
                    }
               }else {
                   alert('Please enter a valid number');return false;
               }
           
               $.ajax({
                       
                        method: "POST",
                        url   : "https://admin.qodamobile.com/changeServiceCost.php",
                        data  : { id:id,value:val,type :type,save:true},
                        success: function(data) { 
                                  location.reload(false)
                   }
               });
         
    }



function save_cost(id){
      var val = $("#servicedata_"+id).val();
        if(isInteger(val)){
               $.ajax({
                        
                        method: "POST",
                        url   : "https://admin.qodamobile.com/changeServiceCost.php",
                        data  : { id:id,value:val,send:true},
                        success: function(data) { 
                                  location.reload(false)
                   }
               });
         }else {
                   alert('Please Enter an integer value');
               }
               
    }
     
 


</script>
<?php

  include('footer.php')?>
