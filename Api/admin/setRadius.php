<?php 
include('dashboard.php');

include('updateServiceCost.php');

$newObj=new AppModel();
$resUlt=$newObj->getRadiusRange();

?>
 <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row ">
     
                    <div class="col-lg-12">
                    	<div class="brEad">
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i>  Set Radius
                                </li>
                            </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">Set Radius</span>
                        </h3>
                    	<div class="panel panel-primary">
				
					
						<table class="table tabSetting" id="dev-table">
						<thead>
							<tr>
								
								<th class="tabHead">Radius Range</th>
								<th class="tabHead">Distance</th>
								<th class="tabHead">Edit</th>
							</tr>
                            <tr></tr>
						</thead>
                        
						<tbody>
                   <?php $j=1;  for($i=0;$i<sizeOf($resUlt);$i++,$j++){ ?>
					<tr>
								
								<th>In Range &nbsp;(Miles)</th>
								<td id="service_<?php echo $resUlt[$i]['radId']; ?>"><input type ="text" id="text_in_range_<?php echo $resUlt[$i]['radId']; ?>"  value="<?php echo $resUlt[$i]['radInrange']; ?>" readonly/></td>
								<td id ="edit_in_range_<?php echo $resUlt[$i]['radId']; ?>"><a onClick="edit_range(<?php echo $resUlt[$i]['radId']; ?>,'in_range')" class="btn  btn-primary" >
                                         <i class="fa fa-pencil"></i> Edit</a></td>
                   <td id ="save_in_range_<?php echo $resUlt[$i]['radId']; ?>" style="display:none;"><a onClick="save_range(<?php echo $resUlt[$i]['radId']; ?>,'in_range')" class="btn  btn-primary">
                                         <i class="fa fa-pencil"></i> Save</a></td>
							</tr>
							<tr>
						
								<th>Out of Range &nbsp;(Miles)</th>
								<td id="service_<?php echo $resUlt[$i]['radId']; ?>"><input type ="text" id="text_out_range_<?php echo $resUlt[$i]['radId']; ?>"  value="<?php echo $resUlt[$i]['radOutrange']; ?>" readonly/></td>
								<td id ="edit_out_range_<?php echo $resUlt[$i]['radId']; ?>"><a onClick="edit_range(<?php echo $resUlt[$i]['radId']; ?>,'out_range')" class="btn  btn-primary" >
                                         <i class="fa fa-pencil"></i> Edit</a></td>
                   <td id ="save_out_range_<?php echo $resUlt[$i]['radId']; ?>" style="display:none;"><a onClick="save_range(<?php echo $resUlt[$i]['radId']; ?>,'out_range')" class="btn  btn-primary">
                                         <i class="fa fa-pencil"></i> Save</a></td>
                   <td></td>
							</tr>
                            
                            <?php }?>
							
								
                            
                            
						</tbody>
					</table>
				</div>
                </div>
                    </div>
                </div>
                <!-- /.row -->
        
            </div> 
            <script type="text/javascript">

function edit_range(id,type){
    
    if(type=='in_range'){
         $("#text_in_range_"+id).attr("readonly", false);
         $("#save_in_range_"+id).css("display", "block");
         $("#edit_in_range_"+id).css("display", "none");
        
    }
    if(type=='out_range'){
         $("#text_out_range_"+id).attr("readonly", false);
         $("#save_out_range_"+id).css("display", "block");
         $("#edit_out_range_"+id).css("display", "none");
    }
    
       
}

function save_range(id,type){
    if(type =='in_range'){
     var val    =    $("#text_in_range_"+id).val();
        
        
    }
    if(type=='out_range'){
     var val  =$("#text_out_range_"+id).val();
         
    }
 
        if($.isNumeric(val)){
               $.ajax({
                        
                        method: "POST",
                        url   : "https://admin.qodamobile.com/setRadius.php",
                        data  : { id:id,value:val,type :type,radii:true},
                        success: function(data) { 
                                  location.reload(false)
                   }
               });
         }else {
                   alert('Please Enter numeric value');
               }
               
    }



 

</script>

 <?php include('footer.php')?>
