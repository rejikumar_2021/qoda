<?php 
include('dashboard.php'); 
$dispid=$_GET['dispid'];
if(empty($dispid)){
echo "<script>window.location='resolveOpenCase.php';</script>";

}

$dispObj=new AppModel();
$reSULt=$dispObj->getopenCasedetails($dispid);
$curPageURL=curPageURL();

?>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>

<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    	<div class="brEad">
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="adminPortal.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i>  View Open Case Details
                            </li>
                        </ol>
                        </div>
                        <h3 class="page-header">
                           <span class="menu-title">View Open Case Details</span>
                           <a class="btn btn-primary" href="resolveOpenCase.php" title="Back" style="float:right;">Back</a>
                        </h3>

                         <div class="panel panel-primary">
					
                    <div class="panel panel-info" style="margin-bottom:0px !important;">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo  $reSULt['userReason'];  ?></h3> 
            </div>
            <div class="panel-body">
              <div class="row">

                <?php if(!empty($reSULt['cuStInfo']['customrPic'])) { ?>
                  <div class="col-md-3 col-lg-3 " align="center"> 
                  <img alt="User Pic" src="<?php echo BASEPATH.$reSULt['cuStInfo']['customrPic']; ?>" class="img-circle img-responsive"> </div>
                  <?php }else{ ?>
                  <div class="col-md-3 col-lg-3 " align="center"> 
                  <img alt="User Pic" src="images/usernoProfile.png" class="img-circle img-responsive">
                  </div>
                  <?php } ?>
                
                <div class=" col-md-9 col-lg-9 "> 
                <h4 style="color: #1482B9;"><span class="menu-title">User Info:</span></h4>
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <th>Customer Name:</th>
                        <td><?php echo  $reSULt['cuStInfo']['customrName'];  ?></td>
                      </tr>
                     <tr>
                        <th>Email:</th>
                        <td><?php echo  $reSULt['cuStInfo']['customrEmail'];  ?></td>
                      </tr>
                        <th>Phone Number:</th>
                        <td><?php echo  $reSULt['cuStInfo']['customrPhone'];  ?>
                        </td>
                           
                      </tr>
                       </tr>
                        <th>User Feedback :</th>
                        <td><?php echo  $reSULt['userComments'];  ?>
                        </td>
                           
                      </tr>
                      
                       </tbody>
                  </table>
                  <br><br>
                  <h4 style="color: #1482B9;"><span class="menu-title">Service Person Info:</span></h4>
                    <table class="table table-user-information">
                    <tbody>
                     <tr>
                        <th>SP Name:</th>
                        <td><?php echo  $reSULt['SPInfo']['servicePName'];  ?></td>
                      </tr>
                      <tr>
                        <th>SP Email:</th>
                        <td><?php echo  $reSULt['SPInfo']['servicePEmail'];  ?></td>
                      </tr>
                      <tr>
                        <th>SP Phone:</th>
                        <td><?php echo  $reSULt['SPInfo']['servicePPhone'];   ?></td>
                      </tr>
                      <tr>
                        <th>SP Feedback:</th>
                        <td><?php echo  $reSULt['spResponse'];   ?></td>
                      </tr>
                    </tbody>
                  </table>
                  
                <br><br>
                  <h4 style="color: #1482B9;"><span class="menu-title">Service Type:<b> <?php echo  $reSULt['serviceResponse']['sserviceType'];  ?> </b></span></h4>
                    <table class="table table-user-information">
                    <tbody>
		  <tr>
                        <th>Total Cost:</th>
                        <td><?php echo  $reSULt['serviceResponse']['svTotalCost'];  ?></td>
                      </tr>
                     <tr>
                        <th>Travel Cost:</th>
                        <td><?php echo  $reSULt['serviceResponse']['svtravelCost'];  ?></td>
                      </tr>
		 <tr>
                        <th>Service Cost:</th>
                        <td><?php echo  $reSULt['serviceResponse']['svServiceCost'];   ?></td>
                      </tr>
		   <tr>
                        <th>After Margin Service Cost:</th>
                        <td><?php echo  $reSULt['serviceResponse']['svPercentCost'];   ?></td>
                      </tr>
                    
                     
                      <tr>
                        <th>Toal Cost After Margin:</th>
                        <td><?php echo  $reSULt['serviceResponse']['aftersvTotal'];   ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php   if($reSULt['disputeStatus']=='0'){  ?> 
              <div class="col-lg-11 " align="center"> 
           
                <div class="form-group">
                    <a class="btn btn-primary btn-danger" href="sPstatus.php?dispID=<?php echo $dispid;  ?>&reT=<?php echo $curPageURL; ?>" title="Without Refund" >Complete Without Refund</a>
                    <a class="btn btn-primary" href="sPstatus.php?dispiD=<?php echo $dispid;  ?>&reTUrn=<?php echo $curPageURL; ?>" title="With Refund" >Complete With Refund</a>
                </div>
              <?php } ?>
              </div>
            </div>
                
					
                    </div>
                </div>
                <!-- /.row -->
        
            </div> 

<script>
$(document).ready(function() {
    $('#examplee').DataTable();
} );

</script>


<?php include('footer.php'); ?>
