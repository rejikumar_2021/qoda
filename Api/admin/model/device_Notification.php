 <?php 
 class Device_Notification{
 	 
 public function sendPushpopup($dEvicetoken,$alertMsg,$flag,$jobId){
	 		$apnsHost = Apple_HOST;
			$apnsCert =Apple_API_NOTI;
			$apnsPort = 2195;
			$token=$dEvicetoken;

			$streamContext = stream_context_create();
			@stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);

			$apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);

			$payload['aps'] = array('alert' =>$alertMsg, 'badge' => 1, 'sound' => 'default','flag' =>$flag,'id'=>$jobId);

			$output = json_encode($payload);
			$token = pack('H*', str_replace(' ', '', $token));
			$apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;
		 
		if(fwrite($apns, $apnsMessage))
		{
			@socket_close($apns);
		 	fclose($apns);
			 return TRUE;
			
		}else{
			return FALSE;
		}
}	


 	public  function sendPushpopupandroid($owner_device_token,$ownrr_massage,$flag,$ch_id)
	{  
	     $target=$owner_device_token;
	     $payload = array('alert' => $ownrr_massage, 'badge' => 1, 'sound' => 'default', 'flag' =>$flag,'fr_id'=>$ch_id);
		  $server_key=FCM_KEY;
		$url = 'https://fcm.googleapis.com/fcm/send';
		 $fields = array();
		$fields['data'] = $payload;
		if(is_array($target)){
			$fields['registration_ids'] = $target;
		}else{
			$fields['to'] = $target;
		}

		 $headers = array(
			'Content-Type:application/json',
		  'Authorization:key='. FCM_KEY
		);
			
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		if ($result === FALSE) {
			die('FCM Send Error: ' . curl_error($ch));
		}
		curl_close($ch);
	     	return $result;
	    }	
  
 }
 ?>
