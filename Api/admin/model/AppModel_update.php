<?php
class AppModel_update extends Database{
	
	public function getDeviceToken($type)
	{
		try
		{
			if($type==1)
			{
		$toKen=$this->_connection->prepare("SELECT device_info.fcm_key FROM device_info INNER JOIN userregistration on userregistration.deviceToken=device_info.token");
		$toKen->execute();
		$result =$toKen->fetchAll(PDO::FETCH_ASSOC);
		return $result;
			}
			else
			{
				$toKen=$this->_connection->prepare("SELECT device_info.fcm_key FROM device_info INNER JOIN spregistration on spregistration.spDeviceToken=device_info.token");
		$toKen->execute();
		$result =$toKen->fetchAll(PDO::FETCH_ASSOC);
		return $result;
			}
		
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed : " . $e->getMessage());
	        }
	}
	public function getDeviceTokenAll()
	{
		try
		{
		$toKen=$this->_connection->prepare("select fcm_key from device_info");
		$toKen->execute();
		$result =$toKen->fetchAll(PDO::FETCH_ASSOC);
		return $result;
		}
		catch (PDOException $e) { parent::close();
		  die("Connection failed : " . $e->getMessage());
	        }
	}
	public function insertPushMessages($title,$msg,$uid)
	{
		try
		{
		$insert=$this->_connection->prepare("insert into push_notifications(title,message,sender_id)values('$title','$msg','$uid')");
		$insert->execute();
		return true;
	}
		catch (PDOException $e) { parent::close();
		  die("Connection failed : " . $e->getMessage());
	        }
	}
	
	public function getCurrentVideo()
	{
		try
		{
			$qry=$this->_connection->prepare("select sp_video from app_settings");
			$qry->execute();
			$data=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		catch(PDOException $e)
		{
		}
	}
	
	public function updateVideo($file)
	{
		try
		{
			$up=$this->_connection->prepare("update app_settings set sp_video='$file' where id=1 ");
			$up->execute();
			return true;
		}
		catch
		(PDOException $e) { parent::close();
		  die("Connection failed : " . $e->getMessage());
	        }
	
	}
	
	public function insertDquestion($question)
	{
		try
		{
			$up=$this->_connection->prepare("insert into questionnaire(question,type)values('$question','2')");
			$up->execute();
			return true;
		}
		catch
		(PDOException $e) { parent::close();
		  die("Connection failed : " . $e->getMessage());
	        }
	
	}
	
	public function insertOquestion($question,$opt1,$opt2,$opt3,$correct)
	{
	
		try
		{
			$up=$this->_connection->prepare("insert into questionnaire(question,option1,option2,option3,correct,	type)values('$question','$opt1','$opt2','$opt3','$correct','1')");
			$up->execute();
			return true;
		}
		catch
		(PDOException $e) { parent::close();
		  die("Connection failed : " . $e->getMessage());
	        }	
	}
	
	public function getQlist()
	{
		try
		{
			$qry=$this->_connection->prepare("select * from questionnaire order by id desc");
			$qry->execute();
			$data=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		catch(PDOException $e)
		{
			parent::close();
			die("Connection faild :".$e->getMessage());
		}
	}
	public function getQuestionByID($id)
	{
		try
		{
			$qry=$this->_connection->prepare("Select * from questionnaire where id='$id' ");
			$qry->execute();
			$data=$qry->fetchAll(PDO::FETCH_ASSOC);
			return $data;
		}
		catch(PDOException $e)
		{
			die("Connection faild :".$e->getMessage());
		}
	}
	
	public function UpdateQuestion($id,$type,$qs,$opt1,$opt2,$opt3,$correct)
	{
		try
		{
			$Qry=$this->_connection->prepare("update questionnaire set question='$qs',option1='$opt1',option2='$opt2',option3='$opt3',correct='$correct',type='$type' where id='$id'");
			$Qry->execute();
			return true;
		}
		catch(PDOException $e)
		{
			die("OOPS!".$e->getMessage());
		}
	}
	
	public function deleteQuestion($id)
	{
		try
		{
			$qry=$this->_connection->prepare("delete from questionnaire where id='$id'");
			$qry->execute();
			return true;
		}
		catch(EXCeption $e)
		{
			die("OOPS!".$e->getMessage());
		}
	}
	
}