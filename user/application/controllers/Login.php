<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('encryption_helper');
		$this->load->model(array('get_data','update'));
	}
	public function index()
	{
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
		$this->form_validation->set_rules('username','Username','trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');
		if($this->form_validation->run()==FALSE)
		{
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
		}
		else
		{
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$pass=encrypt($password);
			$userCheck=$this->get_data->check_credential($username,$pass);
			$spCheck=$this->get_data->check_credential_service($username,$pass);
			if((!empty($userCheck))&&(!empty($spCheck)))
			{
				$result=true;
				$userData=array("type"=>3,"uId"=>$userCheck[0]->userId,"sId"=>$spCheck[0]->spId,"testStatus"=>$spCheck[0]->testStatus);
			}
			if((!empty($userCheck))&&(empty($spCheck)))
			{
				$result=true;
				$userData=array("type"=>2,"uId"=>$userCheck[0]->userId);
			}
			if((!empty($spCheck))&&(empty($userCheck)))
			{
				$result=true;
				$userData=array("type"=>1,"uId"=>$spCheck[0]->spId,"testStatus"=>$spCheck[0]->testStatus);
			}
			if((empty($userCheck))&&(empty($spCheck)))
			{
				$result=false;
			}
			
			if($result!=false)
			{
				$uid=$result[0]->userId;
				$this->session->set_userdata($userData);
				redirect('dashboard/update_user');
			}
			else
			{
				$data['msg']="Invalid username or password";
				$this->load->view('header');
				$this->load->view('login',$data);
				$this->load->view('footer');
			}
		}
			
				
	}
	
	public function reset_password()
	{
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
		$this->form_validation->set_rules('email','email','trim|required|valid_email');
		if($this->form_validation->run()==FALSE)
		{
		$this->load->view('header');
		$this->load->view('password_reset');
		$this->load->view('footer');
		}
		else
		{
			$email=$this->input->post('email');
			if($this->get_data->check_email($email)==true)
			{
				$ranStr = md5(microtime());
				$ranStr = substr($ranStr, 0, 6);
				$pass=encrypt($ranStr);
				if($this->update->update_password($pass,$email)==true)
				{
					$config = array();
        			$config['useragent']           = "CodeIgniter";
       				 $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
       				 $config['protocol']            = "smtp";
       				 $config['smtp_host']           = "localhost:3306";
       				 $config['smtp_port']           = "25";
       				 $config['mailtype'] 			= 'html';
       				 $config['charset']  			= 'utf-8';
       				 $config['newline']  = "\r\n";
       				 $config['wordwrap'] = TRUE;
					$this->load->library('email');
					$this->email->initialize($config);
					$this->email->from("donotreply@adroitminds.com","Qoda");
        			$this->email->to("hairejikumar@gmail.com");
					$this->email->subject('Тест Email');
        			$this->email->message("test mail");

					if ( ! $this->email->send())
						{
       					echo $this->email->print_debugger(array('headers'));
						}
					else
						{
							echo "Email sent successfully";
						}
				}
				else
				{
					
					$info['msg']="OOPS!Something went wrong.Please try again later";
					$this->load->view('header');
					$this->load->view('password_reset',$info);
					$this->load->view('footer');
				}
			}
			else
			{
				$info['msg']="Invalid email";
				$this->load->view('header');
				$this->load->view('password_reset',$info);
				$this->load->view('footer');
			}
		}
	}
	
	public function test()
	{
		$this->load->library('email');
$this->email->set_header('From','info@adroitminds.com\r\n');
$this->email->set_header('MIME-Version', '1.0\r\n');
$this->email->set_header('Content-Type','text/plain; charset=utf-8\r\n');
$this->email->set_header('X-Priority','1\r\n');

$this->email->from('info@adroitminds.com', 'adroitminds');
$this->email->to('hairejikumar@gmail.com');

$this->email->subject('App request from user');
$this->email->message('Hi this is to inform that you are selected');

if ( ! $this->email->send())
						{
       					echo $this->email->print_debugger(array('headers'));
						}
					else
						{
							echo "Email sent successfully";
						}

	}
}
