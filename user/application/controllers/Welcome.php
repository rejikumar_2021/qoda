<?php
//error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	private $_imageBasePath2="http://qodamobile.com/images/layer2.png";

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('encryption_helper');
		$this->load->model(array('get_data','update'));
	}
	public function index()
	{
		$this->load->view('header');
		$this->load->view('email_verify');
		$this->load->view('footer');
	}
	public function login()
	{
	
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
		$this->form_validation->set_rules('username','Username','trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');
		if($this->form_validation->run()==FALSE)
		{
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
		}
		else
		{
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$pass=encrypt($password);
			$result=$this->get_data->check_credential($username,$pass);
			if($result!=false)
			{
				$uid=$result[0]->userId;
				$ses=array("uid"=>$uid);
				$this->session->set_userdata($ses);
				redirect('dashboard/update_user');
			}
			else
			{
				$data['msg']="Invalid username or password";
				$this->load->view('header');
				$this->load->view('login',$data);
				$this->load->view('footer');
			}
			
		}
	}
	public function reset_password()
	{
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
		$this->form_validation->set_rules('email','email','trim|required|valid_email');
		if($this->form_validation->run()==FALSE)
		{
		$this->load->view('header');
		$this->load->view('password_reset');
		$this->load->view('footer');
		}
		else
		{
			$email=$this->input->post('email');
			if($this->get_data->check_email($email)==true)
			{
				$ranStr = md5(microtime());
				$ranStr = substr($ranStr, 0, 6);
				$pass=encrypt($ranStr);
				if($this->update->update_password($pass,$email)==true)
				{
					$config = array();
        			$config['useragent']           = "CodeIgniter";
       				 $config['mailpath']            = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
       				 $config['protocol']            = "smtp";
       				 $config['smtp_host']           = "localhost:3306";
       				 $config['smtp_port']           = "25";
       				 $config['mailtype'] 			= 'html';
       				 $config['charset']  			= 'utf-8';
       				 $config['newline']  = "\r\n";
       				 $config['wordwrap'] = TRUE;
					$this->load->library('email');
					$this->email->initialize($config);
					$this->email->from("donotreply@adroitminds.com","Qoda");
        			$this->email->to("hairejikumar@gmail.com");
					$this->email->subject('Тест Email');
        			$this->email->message("test mail");

					if ( ! $this->email->send())
						{
       					echo $this->email->print_debugger(array('headers'));
						}
					else
						{
							echo "Email sent successfully";
						}
				}
				else
				{
					
					$info['msg']="OOPS!Something went wrong.Please try again later";
					$this->load->view('header');
					$this->load->view('password_reset',$info);
					$this->load->view('footer');
				}
			}
			else
			{
				$info['msg']="Invalid email";
				$this->load->view('header');
				$this->load->view('password_reset',$info);
				$this->load->view('footer');
			}
		}
	}
	
	public function userAuthentcation()
	{
		$email=$this->input->get('emial');
		$result=$this->get_data->checkUserStatus($email);
		if(!empty($result)){
			if($result[0]->is_email_valid==0){
				$uid=$result[0]->userId;
				if($this->update->confirmUserRegistration($uid)==true){
					$userName=$result[0]->userName;
					$conUserName=array("userName"=>$userName);
					$this->session->set_userdata($conUserName);
					$this->sendUserConfirmation($email,$userName);
					redirect('welcome/activated');
				}
				else
				{
					echo "somrthig went wrong";
				}
				
			}else{
				echo "link expired";
			}
		}else{
			$this->load->view('403');
		}
		

	}
	public function activated(){
		$name=$this->session->userdata('userName');
		if($name!=""){
			$this->load->view('header');
			$this->load->view('userconfirmation',$name);
			$this->load->view('footer');
			$this->session->unset_userdata($conUserName);
		}else{
			$this->load->view('403');
		}
	}
	
	private function sendUserConfirmation($email,$userName){
		$configs = array(
			'protocol'  =>  'smtp',
			'smtp_host' =>  'ssl://smtp.gmail.com',
			'smtp_user' =>  'qodainfo@gmail.com',
			'smtp_pass' =>  'qodainfo123',
			'smtp_port' =>  '465',
			'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'wordwrap' => TRUE
		);

		$bOdy="<center><img src='".$this->_imageBasePath2."' class='img-responsive' alt='' /></center>
<center><p style='font-size:25px'><b>Thanks for signing up with QODA!</b></p>
	<p style='font-size:25px'>We are excited to have you use QODA as your<br>
	on-demand roadside assistance service.</p>
	<p style='font-size:25px'>Request a service and in minutes a service provider will be<br>
there to assist you and <b> get you back on the road!</b></p>
<p style='font-size:25px'><b>4 easy steps to get assistance:</b></p>
<p style='font-size:25px;line-height: 35px'>1. Use the QODA app to request service.<br>
2. Accept the service person that has accepted your request.<br>
3. Let the service provider complete the request.<br>
4. We will charge your credit card on file and email you a <br>receipt.</br>
</p>
<p style='font-size:25px'><b>Tips to Get Started:</b></p>
<p style='font-size:25px'>Request as soon as you pull over to a safe place.</p>
<p style='font-size:25px'>Estimate your service charge before committing.</p>
<p style='font-size:25px'>Charges are based on service requested and distance
between you and the service person.</p>
<p style='font-size:25px;margin-top: 100px;'><b> Question? Contact Us.</b></p>
<p style='font-size:25px;'>QODA community managers are here to assist you<br>
throughout your experience. Contact us at<br>
<a href='mailto:support@qodamobile.com'><b>support@qodamobile.com.</b></a></p>
</center>
";
			$this->load->library("email", $configs);
			$this->email->set_newline("\r\n");
			$this->email->to($email);
			$this->email->from("qodainfo@gmail.com", "Qoda");
			$this->email->subject("Thank you for signing up with QODA!  We are excited to have you");
			$this->email->message($bOdy);
			if($this->email->send())
			{
				$result="success";
			}
			else
			{
				$result=$this->email->print_debugger();    
			}
			return $result;
	}

}
