<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Password_reset extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('session','my_phpmailer'));
		$this->load->helper('encryption_helper');
		$this->load->model(array('get_data','update'));
	}
	public function index()
	{
		date_default_timezone_set('Etc/UTC');
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
		$this->form_validation->set_rules('email','email','trim|required|valid_email');
		if($this->form_validation->run()==FALSE)
		{
		$this->load->view('header');
		$this->load->view('password_reset');
		$this->load->view('footer');
		}
		else
		{
			$email=$this->input->post('email');
			if($this->get_data->check_user_email($email)==true)
			{
				$ranStr = md5(microtime());
				$ranStr = substr($ranStr, 0, 6);
				$pass=encrypt($ranStr);
				if($this->update->update_password($pass,$email)==true)
				{
					if($this->get_data->check_sp_email($email)==true)
					{
						$this->update->update_sp_password($pass,$email);	
					}
					
					$mail = new PHPMailer(); // create a new object
					$mail->IsSMTP(); // enable SMTP
					$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
					$mail->SMTPAuth = true; // authentication enabled
					$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
					$mail->Host = "smtp.gmail.com";
					$mail->Port = 465; // or 587
					$mail->IsHTML(true);
					$mail->Username = "qodainfo@gmail.com";
					$mail->Password = "qodainfo123";
					$mail->SetFrom("info@adroitminds.com");
					$mail->Subject = "Qoda password information";
					$mail->Body = "Your new password is $ranStr";
					$mail->AddAddress($email);
					if(!$mail->Send()) 
						{
    							$this->load->view('header');
								$this->load->view('password_reset_error');
								$this->load->view('footer');
 						}
						 else
						 {
							 
							 	$this->load->view('header');
								$this->load->view('password_reset_success');
								$this->load->view('footer');
   						 		
 						}
					
				}
				else
				{
					
					$info['msg']="OOPS!Something went wrong.Please try again later";
					$this->load->view('header');
					$this->load->view('password_reset',$info);
					$this->load->view('footer');
				}
			}
			else
			{
				
				if($this->get_data->check_sp_email($email)==true)
				{
					
					$ranStr = md5(microtime());
					$ranStr = substr($ranStr, 0, 6);
					$pass=encrypt($ranStr);	
					
					if($this->update->update_sp_password($pass,$email)==true)
					{
						if($this->get_data->check_user_email($email)==true)
						{
							$this->update->update_password($pass,$email);
						}
					$mail = new PHPMailer(); // create a new object
					$mail->IsSMTP(); // enable SMTP
					$mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
					$mail->SMTPAuth = true; // authentication enabled
					$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
					$mail->Host = "smtp.gmail.com";
					$mail->Port = 465; // or 587
					$mail->IsHTML(true);
					$mail->Username = "qodainfo@gmail.com";
					$mail->Password = "qodainfo123";
					$mail->SetFrom("info@adroitminds.com");
					$mail->Subject = "Qoda password information";
					$mail->Body = "Your new password is $ranStr";
					$mail->AddAddress($email);
					if(!$mail->Send()) 
						{
    							$this->load->view('header');
								$this->load->view('password_reset_error');
								$this->load->view('footer');
 						}
						 else
						 {
							 
							 	$this->load->view('header');
								$this->load->view('password_reset_success');
								$this->load->view('footer');
   						 		
 						}
					
				}
				else
				{
					
					$info['msg']="OOPS!Something went wrong.Please try again later";
					$this->load->view('header');
					$this->load->view('password_reset',$info);
					$this->load->view('footer');
				}
				
				
					
									
				}
				else
				{
					$info['msg']="Invalid email";
					$this->load->view('header');
					$this->load->view('password_reset',$info);
					$this->load->view('footer');
				}
				
			}
		}
	}
	
	
}