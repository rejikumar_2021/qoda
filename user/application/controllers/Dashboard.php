<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_controller{

	public function __construct()
	{
		parent::__construct();	
		$this->load->model(array('get_data','update'));
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
	}
	public function index()
	{
		$this->load->view('dash_header');
		$this->load->view('menu');
		$this->load->view('user_dashboard');
		$this->load->view('dash_footer');		
		
	}
	
	public function update_user()  // 1 for service,2 for user,3 for both
	{
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
		$cid=$this->session->userdata();
		$type=$cid['type'];
		$id=$cid['uId'];
		switch($type)
		{
			case 1:
			$data['serInfo']=$this->get_data->get_service_info($id); 
			$this->form_validation->set_rules('userName','Username','trim|required');
			$this->form_validation->set_rules('userEmail','Email','trim|required|valid_email');
			$this->form_validation->set_rules('userPhone','Phone number','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('service_profile_update',$data);
				$this->load->view('dash_footer');
			}
			else
			{
				$update=array("spUserName"=>$this->input->post('userName'),
						"spEmail"=>$this->input->post('userEmail'),
						"spMobile"=>$this->input->post("userPhone"),
						"spAddress1"=>$this->input->post("address1"),
						"spAddress2"=>$this->input->post("address2"),
						"spState"=>$this->input->post("state"),
						"spCity"=>$this->input->post("city"),
						"spZipCode"=>$this->input->post("zip"));
						
		if($this->update->update_service($update,$id)==true)
			{
				$data['serInfo']=$this->get_data->get_service_info($id); 
				$data['success']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('service_profile_update',$data);
				$this->load->view('dash_footer');
			}
			else
			{
				$data['serInfo']=$this->get_data->get_service_info($id); 
				$data['error']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('service_profile_update',$data);
				$this->load->view('dash_footer');
			}
						
					
			}
			
			
			break;
			case 2:
			$data['cusInfo']=$this->get_data->get_customer_info($id); 
		$this->form_validation->set_rules('userName','Username','trim|required');
		$this->form_validation->set_rules('userEmail','Email','trim|required|valid_email');
		$this->form_validation->set_rules('userPhone','Phone number','trim|required');
		if($this->form_validation->run()==FALSE)
		{
		$this->load->view('dash_header');
		$this->load->view('menu');
		$this->load->view('user_profile_update',$data);
		$this->load->view('dash_footer');
		}
		else
		{
			$update=array("userName"=>$this->input->post('userName'),
						"userEmail"=>$this->input->post('userEmail'),
						"userMobile"=>$this->input->post("userPhone"),
						"address1"=>$this->input->post("address1"),
						"address2"=>$this->input->post("address2"),
						"state"=>$this->input->post("state"),
						"city"=>$this->input->post("city"),
						"zipCode"=>$this->input->post("zip"));
						
			if($this->update->update_user($update,$id)==true)
			{
				$data['cusInfo']=$this->get_data->get_customer_info($id);
				$data['success']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('user_profile_update',$data);
				$this->load->view('dash_footer');
			}
			else
			{
				$data['cusInfo']=$this->get_data->get_customer_info($id);
				$data['error']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('user_profile_update',$data);
				$this->load->view('dash_footer');
			}
		}
			break;
			case 3:
			$userType=$cid['userType'];
			if($userType!="")
			{
				redirect('dashboard/update_profile');
			}
			else
			{
			$this->form_validation->set_rules('loginChoice','Login choice','required');
			if($this->form_validation->run()==false)
			{
				$this->load->view('switch_screen');
			}
			else
			{
			$login=$this->input->post("loginChoice");
			$cid=array("userType"=>$login);
			$this->session->set_userdata($cid);
			redirect('dashboard/update_profile');
			}
			}
			break;
			default:
			echo "OOPS!Something went wrong please try again";
		}
		
		
	}
	
	public function update_profile()
	{
		$this->load->library(array('form_validation','session'));
		$this->load->helpers(array('form','url'));
		$info=$this->session->userdata();
		$type=$info['userType'];
		
		if($type!="")
		{
			if($type==1)
			{ //sp
				$id=$info['sId'];
				$cid=$info['uId'];
				$data['serInfo']=$this->get_data->get_service_info($id); 
			$this->form_validation->set_rules('userName','Username','trim|required');
			$this->form_validation->set_rules('userEmail','Email','trim|required|valid_email');
			$this->form_validation->set_rules('userPhone','Phone number','trim|required');
			if($this->form_validation->run()==FALSE)
			{
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('service_profile_update',$data);
				$this->load->view('dash_footer');
			}
			else
			{
				$spUpdate=array("spUserName"=>$this->input->post('userName'),
						"spEmail"=>$this->input->post('userEmail'),
						"spMobile"=>$this->input->post("userPhone"),
						"spAddress1"=>$this->input->post("address1"),
						"spAddress2"=>$this->input->post("address2"),
						"spState"=>$this->input->post("state"),
						"spCity"=>$this->input->post("city"),
						"spZipCode"=>$this->input->post("zip"));
						
						$cusUpdate=array("userName"=>$this->input->post('userName'),
						"userEmail"=>$this->input->post('userEmail'),
						"userMobile"=>$this->input->post("userPhone"),
						"address1"=>$this->input->post("address1"),
						"address2"=>$this->input->post("address2"),
						"state"=>$this->input->post("state"),
						"city"=>$this->input->post("city"),
						"zipCode"=>$this->input->post("zip"));
						
		if($this->update->update_service($spUpdate,$id)==true)
			{
				$this->update->update_user($cusUpdate,$cid);
				$data['serInfo']=$this->get_data->get_service_info($id); 
				$data['success']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('service_profile_update',$data);
				$this->load->view('dash_footer');
			}
			else
			{
				$data['serInfo']=$this->get_data->get_service_info($id); 
				$data['error']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('service_profile_update',$data);
				$this->load->view('dash_footer');
			}
						
					
			}
				
			}
			else
			{ //customer
			$sid=$info['sId'];
			$id=$info['uId'];
		$data['cusInfo']=$this->get_data->get_customer_info($id); 
		$this->form_validation->set_rules('userName','Username','trim|required');
		$this->form_validation->set_rules('userEmail','Email','trim|required|valid_email');
		$this->form_validation->set_rules('userPhone','Phone number','trim|required');
		if($this->form_validation->run()==FALSE)
		{
		$this->load->view('dash_header');
		$this->load->view('menu');
		$this->load->view('user_profile_update',$data);
		$this->load->view('dash_footer');
		}
		else
		{
			$cusUpdate=array("userName"=>$this->input->post('userName'),
						"userEmail"=>$this->input->post('userEmail'),
						"userMobile"=>$this->input->post("userPhone"),
						"address1"=>$this->input->post("address1"),
						"address2"=>$this->input->post("address2"),
						"state"=>$this->input->post("state"),
						"city"=>$this->input->post("city"),
						"zipCode"=>$this->input->post("zip"));
						
			$spUpdate=array("spUserName"=>$this->input->post('userName'),
						"spEmail"=>$this->input->post('userEmail'),
						"spMobile"=>$this->input->post("userPhone"),
						"spAddress1"=>$this->input->post("address1"),
						"spAddress2"=>$this->input->post("address2"),
						"spState"=>$this->input->post("state"),
						"spCity"=>$this->input->post("city"),
						"spZipCode"=>$this->input->post("zip"));
						
			if($this->update->update_user($cusUpdate,$id)==true)
			{
				$this->update->update_service($spUpdate,$sid);
				$data['cusInfo']=$this->get_data->get_customer_info($id);
				$data['success']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('user_profile_update',$data);
				$this->load->view('dash_footer');
			}
			else
			{
				$data['cusInfo']=$this->get_data->get_customer_info($id);
				$data['error']=true;
				$this->load->view('dash_header');
				$this->load->view('menu');
				$this->load->view('user_profile_update',$data);
				$this->load->view('dash_footer');
			}
		}
				
			}
		}
		else
		{
			redirect('logout');
		}
	}
	public function showvideo()
	{
		$data['video']=$this->get_data->getVideo();
		$id=$this->session->userdata();
		$sid=$id['sId'];
			if($sid=="")
			{
				$sid=$id['uId'];
			}
			$this->update->clearQuiz($sid);
		if($id['testStatus']==0)
		{
		$this->load->view('dash_header');
		$this->load->view('menu');
		$this->load->view('showVideo',$data);
		$this->load->view('dash_footer');
		}
		else
		{
			redirect("forbidden");
		}
	}
	public function questionnaire($ch)
	{
		$id=$this->session->userdata();
		$sid=$id['sId'];
		
		//if($id['testStatus']==0)
		//{
		
		if($sid=="")
		{
			$sid=$id['uId'];
		}
		
		$this->form_validation->set_rules('start','start','required');
		$this->form_validation->set_rules('stop','stop','required');
		if($this->form_validation->run()==false)
		{
		$queStion=$this->get_data->getQuestion();
		foreach($queStion as $qs)
		{
			$qsid[]=$qs->id;
		}
		$sQid=array("quid"=>$qsid);
		$this->session->set_userdata($sQid);
		
		$ques['data']=$this->get_data->getCurrentQuestion($qsid[0]);
		$qid=$ques['data'][0]->id;
		$ques['ans']=$this->get_data->getAnswer($sid,$qid);
		$ques['current']=0;
		$ques['quesid']=$qsid[0];
		$this->load->view('dash_header');
		$this->load->view('menu');
		$this->load->view('showQuestion',$ques);
		$this->load->view('dash_footer');
		}
		else
		{
			$action=$this->input->post('action');
			if($action=="Next")
			{
				$q=$this->session->userdata("quid");
				
				$start=$this->input->post('start');
				$end=$this->input->post('stop');
				$current=$this->input->post('current');
				$answer=$this->input->post('answer');
				$pre=$this->input->post('preAns');
				$qstid=$this->input->post('QuesID');
				
				if($answer=="")
				{
					$answer=0;
				}
				if($pre==1)
				{
					$this->update->updateAnswer($answer,$sid,$qstid);
				}
				else
				{
					$ans=array("sid"=>$sid,"quid"=>$qstid,"ans"=>$answer);
					$this->update->insertAnswer($ans);
				}
				
				
				if($end>$current)
				{
					 ++$current;
					$ques['data']=$this->get_data->getNextQuestion($q[$current]);
					$qid=$ques['data'][0]->id;
					$ques['ans']=$this->get_data->getAnswer($sid,$qid);
					$ques['current']=$current;
					$ques['quesid']=$q[$current];
					$this->load->view('dash_header');
					$this->load->view('menu');
					$this->load->view('showQuestion',$ques);
					$this->load->view('dash_footer');
				}
				else
				{
					
					$point=$this->get_data->get_SpPoints($sid);
					
					$i=0;
					foreach($point as $p)
					{
						if($p->correct==1)
						{
							$correct=$p->option1;
						}
						if($p->correct==2)
						{
							$correct=$p->option2;
						}
						if($p->correct==3)
						{
							$correct=$p->option3;
						}
						
						if($correct==$p->ans)
						{
							++$i;
						}
						
					}
					
					$this->update->updateSpStatus($sid);
					$st=array("testStatus"=>1);
					
					if($i==5)
					{
					$this->session->set_userdata($st);
					$this->load->view('dash_header');
					$this->load->view('menu');
					$this->load->view('thankyou');
					$this->load->view('dash_footer');
					}
					else
					{
						$this->load->view('dash_header');
						$this->load->view('menu');
						$this->load->view('reTest');
						$this->load->view('dash_footer');
					}
				}
			}
			if($action=="Previous")
			{
				$q=$this->session->userdata("quid");
				$start=$this->input->post('start');
				$end=$this->input->post('stop');
				$current=$this->input->post('current');
				$answer=$this->input->post('answer');
				
				/*if($answer=="")
				{
					$answer=0;
				}
				$pre=$this->input->post('preAns');
				if($pre==1)
				{
					$this->update->updateAnswer($answer,$sid,$q[$current]);
				}
				else
				{
					$ans=array("sid"=>$sid,"quid"=>$q[$current],"ans"=>$answer);
					$this->update->insertAnswer($ans);
				}*/
				
				if($current>$start)
				{
					--$current;	
					$ques['data']=$this->get_data->getNextQuestion($q[$current]);
					$qid=$ques['data'][0]->id;
					$ques['ans']=$this->get_data->getAnswer($sid,$qid);
					$ques['current']=$current;
					$ques['quesid']=$q[$current];
					$this->load->view('dash_header');
					$this->load->view('menu');
					$this->load->view('showQuestion',$ques);
					$this->load->view('dash_footer');
				}
				else
				{
					$ques['data']=$this->get_data->getNextQuestion($q[0]);
					$qid=$ques['data'][0]->id;
					$ques['ans']=$this->get_data->getAnswer($sid,$qid);
					$this->load->view('dash_header');
					$this->load->view('menu');
					$this->load->view('showQuestion',$ques);
					$this->load->view('dash_footer');
				}
				
			}
			
		}
		
		//}
		//else
		//{
			//redirect("forbidden");
		//}
	}
	
	public function clearQuestion($ch)
	{
		if($ch==1)
		{
			$id=$this->session->userdata();
			$sid=$id['sId'];
			if($sid=="")
			{
				$sid=$id['uId'];
			}
			
			$this->update->clearQuiz($sid);
			redirect('dashboard/questionnaire');
		}
	}
	
	public function postpond()
	{
			$id=$this->session->userdata();
			$sid=$id['sId'];
			if($sid=="")
			{
				$sid=$id['uId'];
			}
			$this->update->testStatus($sid);
			redirect('logout');	
	}
	
}