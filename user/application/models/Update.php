<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Update extends CI_Model {

public function update_user($data,$id)
{
	$this->db->where("userId",$id);
	if($this->db->update("userregistration",$data)==true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
public function update_password($pass,$email)
{
	$data=array("userPassword"=>$pass);
	$this->db->where("userEmail",$email);
	if($this->db->update("userregistration",$data)==true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
public function update_service($data,$id)
{
	$this->db->where("spId",$id);
	if($this->db->update("spregistration",$data)==true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
public function update_sp_password($pass,$email) 
{
	$data=array("spPassword"=>$pass);
	$this->db->where("spEmail",$email);
	if($this->db->update("spregistration",$data)==true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

public function insertAnswer($ans)
{
	$this->db->insert("spanswer",$ans);
}
public function updateAnswer($answer,$sid,$qid)
{
	$data=array("ans"=>$answer);
	$this->db->where('sid',$sid);
	$this->db->where('quid',$qid);
	$this->db->update("spanswer",$data);
}
public function updateSpStatus($sid)
{
	$data=array("testStatus"=>1);
	$this->db->where('spId',$sid);
	$this->db->update('spregistration',$data);
}
public function clearQuiz($id)
{
	
	$this->db->where('sid',$id);
	$this->db->delete('spanswer');
}
public function testStatus($id)
{
	$data=array("testStatus"=>0);
	$this->db->where('spId',$id);
	$this->db->update('spregistration',$data);
}
public function confirmUserRegistration($id){

	$data=array("is_email_valid"=>1);
	$this->db->where('userId',$id);
	if($this->db->update('userregistration',$data)==true){
		return true;
	}
}
}
