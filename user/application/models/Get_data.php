<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Get_data extends CI_Model {

public function check_credential($username,$password)
{
	$this->db->select(array('userId'));
	$this->db->from('userRegistration');
	$this->db->where('userEmail',$username);
	$this->db->where('userPassword',$password);
	$this->db->limit(1);
	$data=$this->db->get();
	if($this->db->count_all_results()==1)
	{
		return $data->result();
	}
	else
	{
		return false;
	}
	
}

public function check_credential_service($username,$password)
{
	$this->db->select(array('spId','testStatus','spbLockStatus'));
	$this->db->from('spregistration');
	$this->db->where('spEmail',$username);
	$this->db->where('spPassword',$password);
	$this->db->limit(1);
	$data=$this->db->get();
	if($this->db->count_all_results()==1)
	{
		return $data->result();
	}
	else
	{
		return false;
	}
	
}


public function get_customer_info($id)
{
	$this->db->where('userId',$id);
	$data=$this->db->get('userRegistration');
	return $data->result();
}
public function get_service_info($id)
{
	$this->db->where('spId',$id);
	$data=$this->db->get('spregistration');
	return $data->result();
}

public function check_user_email($email)
{
	$this->db->select(array('userId'));
	$this->db->from('userRegistration');
	$this->db->where('userEmail',$email);
	$this->db->limit(1);
	$data=$this->db->get();
	if($this->db->count_all_results()==1)
	{
		return $data->result();
	}
	else
	{
		return false;
	}
}

public function check_sp_email($email)
{
	$this->db->select(array('spEmail'));
	$this->db->from('spregistration');
	$this->db->where('spEmail',$email);
	$this->db->limit(1);
	$data=$this->db->get();
	if($this->db->count_all_results()==1)
	{
		return $data->result();
	}
	else
	{
		return false;
	}
}

public function getVideo()
{
	$this->db->select(array('sp_video'));
	$video=$this->db->get('app_settings');
	return $video->result();
}
public function getQuestion()
{
	
	//$this->db->limit(1);
	//$data=$this->db->get('questionnaire');
	//return $data->result();
	
    $this->db->order_by('rand()');
    $this->db->limit(5);
    $query = $this->db->get('questionnaire');
    return $query->result();
}

/*public function getNextQuestion($id)
{
	$this->db->where('id >',$id);
	$this->db->limit(1);
	$data=$this->db->get('questionnaire');
	return $data->result();
}*/
public function getNextQuestion($id)
{
	$this->db->where('id',$id);
	$data=$this->db->get('questionnaire');
	return $data->result();
}

public function getPrevQuestion($id)
{
	$this->db->where('id <',$id);
	$this->db->order_by('id','desc');
	$this->db->limit(1);
	$data=$this->db->get('questionnaire');
	return $data->result();
	//return $this->db->last_query();
	
}
public function getAnswer($sid,$qid)
{
	$this->db->select(array('ans'));
	$this->db->where('sid',$sid);
	$this->db->where('quid',$qid);
	$data=$this->db->get('spanswer');
	return $data->result();
	
}
public function getCurrentQuestion($id)
{
	$this->db->where('id',$id);
	$data=$this->db->get('questionnaire');
	return $data->result();
}

public function get_SpPoints($id)
{
	$this->db->select(array('spanswer.*','questionnaire.*'));
	$this->db->from('spanswer');
	$this->db->join('questionnaire','questionnaire.id=spanswer.quid','inner');
	$this->db->where('sid',$id);
	$data=$this->db->get();
	return $data->result();
}
	public function checkUserStatus($mail){
		$this->db->select(array('userEmail','is_email_valid','userId','userName'));
		$this->db->where('userEmail',$mail);
		$this->db->where('is_email_valid',0);
		$this->db->limit(1);
		$data=$this->db->get('userregistration');
		return $data->result();
	}
}
