<div class="loading"></div>
<div class="clearfix"></div>
        <div class="container" id="top">
         <div class="row">
      <div class="col-md-offset-2 col-md-8">
                <div class="signup-form-wrapper">
    <div class="signup-form-inner">
        <h1>Sign Up to Get Roadside Assistance</h1>
        <div class="heading-seperator"></div>
        <p class="p_center">Welcome to Qoda, the easiest way to get around at the tap of a button.
        Create your account and get moving in minutes.</p>
       
      
 <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Become a Service Provider </a></li>
    <li ><a data-toggle="tab" href="#menu1">Become a Customer</a></li>
   
  </ul>
 
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
                 <form action="#" method="post"  id="form-sign-up-customer">
                 <p class="text_description"><i>Sign Up to Provide Services<br>
                        Welcome to QODA - the easiest way to get roadside assistance.<br> 
                        Create your service provider account and start helping others!</i></p>
                    
                        <h2>Account <span> Required* </span> </h2>
                        <div class="form-group">
                                <label  for="spname"><span>*</span>Name:</label> <span id="userErrorName" class="error"> </span>
                                <input type="text" class="rule form-control" id="spname" placeholder="enter the name">
                        </div>  
                         <div class="form-group">
                                <label  for="spmobile"><span>*</span>Mobile:</label><span id="spmobileError" class="error"> </span>
                                <input type="text" onkeypress="return (event.charCode >= 48 &amp;&amp; event.charCode <= 57) ||       event.charCode == 0 " class="rule form-control" id="spmobile" placeholder="enter the mobile">
                        </div>  
                        <div class="form-group"> 
                                 <label for="pwd"><span>*</span>Email:</label><span id="spEmailError" class="error"> </span>
                                <input type="email" class="rule form-control" onblur="checkEmail()" id="spemail" placeholder="Name@example.com">
                        </div>   
                           <div class="form-group">
                                <label for="pwd"><span>*</span>PASSWORD:</label><span id="sppwdError" class="error"> </span>
                                <input type="password" class="rule form-control" id="sppwd" placeholder="At leat 8 characters"> 
                        </div>   
                           <div class="form-group">
               <label  for="spcpwd"><span>*</span>PASSWORD COMFIRMATION:</label><span id="spcpwdError" class="error"> </span>
                                <input type="password" class="rule form-control" id="spcpwd" placeholder="At leat 8 characters">
                        </div>   
                           <label  for="userPsswd"><span>*</span>Service Type:</label><span id="spChkError" class="error"> </span> <br/>
                           
 <label class="checkbox-inline"><input type="checkbox" name="service" value="1">Tire</label>
<label class="checkbox-inline"><input type="checkbox" name="service"  value="2">Battery</label>
<label class="checkbox-inline"><input type="checkbox"  name="service"  value="3">Gas</label>
                  <!--          <div class="form-group">
                                <label  for="splicense"><span>*</span>License No:</label>
                                <input type="text" class="form-control" id="splicense" placeholder="Enter the license no.">
                        </div>  
                        <div class="form-seperator"></div>
                      <h2>Vehicle Information</h2>
                        
                        
                          <div class="form-group">
                                <label  for="spcarmodel"><span>*</span>Car Model:</label>
                                <input type="text" class="form-control" id="spcarmodel" placeholder="Enter car model">
                        </div>  
                        <div class="form-group">
                                <label  for="spcaryear"><span>*</span>Year:</label>
                                <input type="text" class="form-control" id="spcaryear" placeholder="Enter the year">
                        </div>    -->
                        
                        <input value="CREATE ACCOUNT" id="signUpSubmit" name="submit-signup" type="submit">
                        <div class="clearfix"></div><br/>
                         <div id="mSgError" ></div>
                        <div id="mSgSuccess"></div>
                    </form>
                    
                        <div class="agree-text">
                <p>By clicking “Create Account” , you agree to Qoda's <br/>
                 <a href="http://qodamobile.com/terms-and-conditions-pages/">Terms and Conditions and Privacy Policy.</a></p>
            </div>
        
    </div>
    <div id="menu1" class="tab-pane fade">
     <form action="#" method="post" id="form-sign-up-customer" onsubmit="return validate();">
                       <p class="text_description"><i>Sign Up to Get Roadside Assistance<br>
                        Welcome to QODA - the easiest way to get roadside assistance.  <br>
                        Create your customer account and get quick, affordable roadside assistance.</i></p>
                        <h2>Account <span> Required<span>* </span></span></h2>
                        <div class="form-group">
                                <label  for="username"><span>*</span>Name:</label> <span id="cNameError" class="error"> </span>
                                <input type="text" class="rule form-control" id="username" placeholder="enter the name">
                        </div>  
                         <div class="form-group">
                                <label  for="usermobile"><span>*</span>Mobile:</label>  <span id="cMobileError" class="error"> </span>
                                <input type="text" class="rule form-control" id="usermobile" placeholder="+1 (502) 6666 9999">
                        </div>  
                        <div class="form-group"> 
                                 <label for="userEmail"><span>*</span>Email: </label>  <span id="cemailError" class="error"> </span>
                                <input type="email" class="rule form-control" id="userEmail" placeholder="Name@example.com">
                        </div>   
                           <div class="form-group">
                                <label for="userPasswd"><span>*</span>PASSWORD:  </label>  <span id="cpswdError" class="error"> </span>
                                <input type="password" class="rule form-control" id="userPasswd" placeholder="At leat 8 characters"> 
                        </div>   
                           <div class="form-group">
                                <label  for="usercPsswd"><span>*</span>PASSWORD COMFIRMATION:</label> <span id="cpswddError" class="error"> </span>
                                <input type="password" class="rule form-control" id="usercPsswd" placeholder="At leat 8 characters">
                        </div>  

                     <input value="CREATE ACCOUNT" id="signUpCSubmit" name="submit-signup" type="submit">
                      <div class="clearfix"></div><br/>
                         <div id="mSgErrort" ></div>
                        <div id="mSgSuccesss"></div>
                    </form>
                        <div class="agree-text">
                <p>By clicking “Create Account” , you agree to Qoda's <br/>
                <a href="http://qodamobile.com/terms-and-conditions-pages/">Terms and Conditions and Privacy Policy.</a></p>
            </div>
    </div>
                    
  </div>
</div>
 
  </div> </div>
  </div></div>