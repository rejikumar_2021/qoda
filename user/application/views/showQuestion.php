<style>
.qtn
{
background-color: #ecf0f5;
    padding: 15px;
    font-size: 15px;
    /* letter-spacing: unset; */
    border: 1px solid #c3b2b2;
    border-radius: 5px;
}
</style>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Evaluation
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">evaluation</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">



<form action="" method="post">
<input type="hidden" name="start" value="0">
<input type="hidden" name="stop" value="4">
<input type="hidden" name="current" value="<?php echo $current; ?>">
<input type="hidden" name="QuesID" value="<?php echo $quesid?>" />
<table class="table" width="100%">
    <tbody>
      
      <tr class="success">
        <td colspan="3"><p class="qtn"><?php echo  $data[0]->question; ?></p></td>
        
      </tr>
       <?php
	  if(!empty($ans))
	  {
		  ?>
          <input type="hidden" name="preAns" value="1">
          <?php
	  }
	  else
	  {
		  ?>
          <input type="hidden" name="preAns" value="0">
          <?php
	  }
	  ?>
       <?php
	  if($data[0]->type==1)
	  {
	  ?>
      <tr class="info">
    
        <td colspan="3"><input type="radio" name="answer" value="<?php echo $data[0]->option1; ?>" <?php if($ans[0]->ans==$data[0]->option1){ ?> checked <?php } ?> ><?php echo  $data[0]->option1; ?></td>
        </tr>
        <tr class="info">
        <td colspan="3"><input type="radio" name="answer" value="<?php echo $data[0]->option2; ?>" <?php if($ans[0]->ans==$data[0]->option2){ ?> checked <?php } ?>  ><?php echo  $data[0]->option2; ?></td>
        </tr>
        <tr class="info">
        <td colspan="3"><input type="radio" name="answer" value="<?php echo $data[0]->option3; ?>" <?php if($ans[0]->ans==$data[0]->option3){ ?> checked <?php } ?>  ><?php echo  $data[0]->option3; ?></td>
        </tr>
        <?php
	  }
	  else
	  {
		  ?>
          <tr class="info">
          <td colspan="3"> 
          <textarea class="form" name="answer" rows="5" style="width:100%">
          <?php echo $ans[0]->ans; ?>
          
          </textarea>
          </td>
          </tr>
          <?php
	  }
	  ?>
      </tr>
      
      <tr class="warning">
        <td><button type="submit" name="action" value="Previous"class="btn btn-info">Previous</button></td>
        <td></td>
        <td><button type="submit" name="action" value="Next"class="btn btn-success">Next</button></td>
      </tr>
      
      
    </tbody>
  </table>
</form>


</div>