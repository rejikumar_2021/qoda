<?php
include('header.php');
include('menu.php');
?>
               <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Display values
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Display values</li>
          </ol>
        </section>
                <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">  
               
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>slno</th>
                        <th>Name</th>
                        <th>Department</th>
                         <th>grade</th>
                        <th>salary</th>
                		<th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    
                    
                      <tr>
                        <td>Lorem</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                         <td>
                         <button class="btn btn-small btn-warning">Edit</button> 
                         <button class="btn btn-small btn-danger">Delete</button>
                         </td>
                      </tr>
 
 
 
                    </tbody>
                   <!-- <tfoot>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                      </tr>
                    </tfoot>-->
                  </table>
                </div>
                </div>
                </div>
                </section>
            </div>
            
           
          </div>
                </div>
                
  <?php
  include('footer.php');
  ?>