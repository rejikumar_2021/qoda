<style>
.customInfo
{
	font-size:20px;
}
.btn
{
	width:125px;
}
.confirm
{
	margin-top:10px;
}
</style>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Thank you
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">thank you</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <div class="alert alert-info customInfo">
                You did not score 100% on the quiz.<font color="#FF0000">To become an official service provder you must score 100%.</<br />Do you want to take the quiz again ?</font>
                <div  class="confirm">
                <a href="<?php echo base_url(); ?>index.php/dashboard/clearQuestion/1"><button class="btn btn-md btn-success">Yes</button></a>
               <a href="<?php echo base_url(); ?>index.php/dashboard/postpond">
                <button class="btn btn-md btn-danger">No</button>
                </div>
                </div>
                
                </div><!-- /.box -->