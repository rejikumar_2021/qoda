<style>
#customerSignupForm
{
	display:none;
}

#serviceSignupForm
{
	display:none;
}

#CustomersingUp
{
	display:none;
}

#spSignUp
{
	display:none;
}
.submit
{
	float: right;
    width: 150px;
    background-color: #1481b9;
}
.customButton
{
	width: 100%;
    background-color: #4596dc;
    height: 49px;
    color: #fff;
    font-size: 20px;
    letter-spacing: 2px;
}
.req
{
	color: #fb0e0e;
    margin-right: 10px;
}
.customButton:hover
{
	background-color:#759e07;
	color:#fff;
}
.frmError
{
	border: 1px solid #e61111;
}
.frmOk
{
	border: 1px solid #ddd;
}
.error
{
	color: #f00;
    margin-left: 10px;
    float: right;
	display:none;
}
.passError
{
	float: right;
    text-align: center;
    color: #d60d0d;
    padding-left: 25px;
    padding-right: 25px;
    border-radius: 2px;
	display:none;
}
.frmMobile
{
	float: right;
    text-align: center;
    color: #d60d0d;
    padding-left: 25px;
    padding-right: 25px;
    border-radius: 2px;
	display:none;
}
.serviceError
{
	float: right;
    text-align: center;
    color: #d60d0d;
    padding-left: 25px;
    padding-right: 25px;
    border-radius: 2px;
	display:none;
}
</style>
<div class="loading"></div>
<div class="clearfix"></div>
        <div class="container" id="top">
         <div class="row">
      <div class="col-md-offset-2 col-md-8">
      <!---- Existing customer form---------->
       <div class="customerSignupForm" id="CustomersingUp">
      
       <p class="text_description"><i>Sign Up to Get Roadside Assistance<br>
                        Welcome to QODA - the easiest way to get roadside assistance.  <br>
                        Create your customer account and get quick, affordable roadside assistance.</i></p>
                         <form id="customerSignUp1">
                         <input type="hidden" name="ID" id="CUSID" />
                         <input type="hidden" name="cusEmaiL1" id="cusEmaiL1" />
      <input type="hidden" name="CusType" id="CusType1" value="2" />
                         <div class="form-group formName">
      <label for="usr"><span class="req">*</span>Name:</label>
      <input type="text" class="form-control" id="cus_usrName" name="userName">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group formMobile">
      <label for="mobile" style="width:100%"><span class="req">*</span>Mobile:<span class="frmMobile">Invalid mobile number</span></label>
      <input type="text" class="form-control" id="cus_mobile" name="mobile">
       <span class="error">This field is required</span>
    </div>
    
    <div class="form-group">
      <label for="housename">Address</label>
      <input type="text" class="form-control" id="cus_houseName">
    </div>
    <div class="form-group">
      <label for="street">Address 2</label>
      <input type="text" class="form-control" id="cus_street" name="street">
    </div>
    <div class="form-group">
      <label for="city">City</label>
      <input type="text" class="form-control" id="cus_city" name="city">
    </div>
    
     <div class="form-group">
      <label for="city">State</label>
      <input type="text" class="form-control" id="cus_state" name="state">
    </div>
    
    <div class="form-group">
      <label for="zipcode">zip Code</label>
      <input type="text" class="form-control" id="cus_zip" name="zip">
    </div>
    <button type="submit" class="btn  customButton">CREATE ACCOUNT</button>
  </form>
    
       </div>
      
      
      <!-- existing customer form ends here-->
      
     <div class="customerSignupForm" id="customerSignupForm">
     <p class="text_description"><i>Sign Up to Get Roadside Assistance<br>
                        Welcome to QODA - the easiest way to get roadside assistance.  <br>
                        Create your customer account and get quick, affordable roadside assistance.</i></p>
                         <form id="customerSignUp">
                         <input type="hidden" name="email" id="cusemail" />
    <div class="form-group formName">
      <label for="usr"><span class="req">*</span>Name:</label>
      <input type="text" class="form-control" id="UsrName" name="userName">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group formMobile">
      <label for="mobile" style="width:100%"><span class="req">*</span>Mobile:<span class="frmMobile">Invalid mobile number</span></label>
      <input type="text" class="form-control" id="Mobile" name="mobile">
       <span class="error">This field is required</span>
    </div>
    
    <div class="form-group formPassword">
      <label for="Password" style="width:100%"><span class="req">*</span>Password:<span class="passError">Both password should be same</span></label>
      <input type="password" class="form-control" id="password" name="password">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group formConfirm">
      <label for="Password"><span class="req">*</span>PASSWORD COMFIRMATION:</label>
      <input type="password" class="form-control" id="conFirm" name="Confirm">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group">
      <label for="housename">Address</label>
      <input type="text" class="form-control" id="houseName">
    </div>
    <div class="form-group">
      <label for="street">Address 2</label>
      <input type="text" class="form-control" id="street" name="street">
    </div>
    <div class="form-group">
      <label for="city">City</label>
      <input type="text" class="form-control" id="city" name="city">
    </div>
    <div class="form-group">
      <label for="city">State</label>
      <input type="text" class="form-control" id="state" name="state">
    </div>
    <div class="form-group">
      <label for="zipcode">Zip Code</label>
      <input type="text" class="form-control" id="zip" name="zip">
    </div>
    <button type="submit" class="btn  customButton">CREATE ACCOUNT</button>
  </form>
     </div>
     
     <!--*************************************-->
     
     <div class="customerSignupForm" id="serviceSignupForm">
     <p class="text_description"><i>Sign Up to Provide Services<br>
                        Welcome to QODA - the easiest way to get roadside assistance.<br> 
                        Create your service provider account and start helping others!</i></p>
                        
                         <form id="sPSignUp">
                         <input type="hidden" name="email" id="sPEmail" />
    <div class="form-group formName">
      <label for="usr"><span class="req">*</span>Name:</label>
      <input type="text" class="form-control" id="SpName" name="userName">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group formMobile">
      <label for="mobile" style="width:100%"><span class="req">*</span>Mobile:<span class="frmMobile">Invalid mobile number</span></label>
      <input type="text" class="form-control" id="spMobile" name="mobile">
       <span class="error">This field is required</span>
    </div>
    
    <div class="form-group formPassword">
      <label for="Password" style="width:100%"><span class="req">*</span>Password:<span class="passError">Both password should be same</span></label>
      <input type="password" class="form-control" id="sPpassword" name="password">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group formConfirm">
      <label for="Password"><span class="req">*</span>PASSWORD COMFIRMATION:</label>
      <input type="password" class="form-control" id="sPconFirm" name="Confirm">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group">
      <label for="housename">Address</label>
      <input type="text" class="form-control" id="sphouseName">
    </div>
    <div class="form-group">
      <label for="street">Address 1</label>
      <input type="text" class="form-control" id="spstreet" name="street">
    </div>
    <div class="form-group">
      <label for="city">City</label>
      <input type="text" class="form-control" id="spcity" name="city">
    </div>
    <div class="form-group">
      <label for="city">State</label>
      <input type="text" class="form-control" id="spstate" name="spstate">
    </div>
    <div class="form-group">
      <label for="zipcode">Zip Code</label>
      <input type="text" class="form-control" id="spzip" name="zip">
    </div>
    <div class="form-group">
      <label for="service" style="width:100%"><span class="req">*</span>SERVICE TYPE:<span class="serviceError">You must select atleast one service</span></label><br />
      <label class="checkbox-inline"><input type="checkbox" name="service" value="1">Tire</label>
      <label class="checkbox-inline"><input type="checkbox" name="service" value="2">Battery</label>
      <label class="checkbox-inline"><input type="checkbox" name="service" value="3">Gas</label>
    </div>
    <button type="submit" class="btn  customButton">CREATE ACCOUNT</button>
  </form>
  <p>By clicking “Create Account” , you agree to Qoda's <br>
                 <a href="http://qodamobile.com/terms-and-conditions-pages/">Terms and Conditions and Privacy Policy.</a></p>
     </div>
     <!--************************************-->
     <div class="customerSignupForm" id="spSignUp">
     
     <p class="text_description"><i>Sign Up to Provide Services<br>
                        Welcome to QODA - the easiest way to get roadside assistance.<br> 
                        Create your service provider account and start helping others!</i></p>
                        
     <form id="SpfromCustomer">
        <input type="hidden" name="ID" id="SID" /> 
        <input type="hidden" name="spEmaiL1" id="spEmaiL1" />           
    <div class="form-group formName">
      <label for="usr"><span class="req">*</span>Name:</label>
      <input type="text" class="form-control" id="SpName1" name="userName">
      <span class="error">This field is required</span>
    </div>
    <div class="form-group formMobile">
      <label for="mobile" style="width:100%"><span class="req">*</span>Mobile:<span class="frmMobile">Invalid mobile number</span></label>
      <input type="text" class="form-control" id="spMobile1" name="mobile">
       <span class="error">This field is required</span>
    </div>
    
    
    <div class="form-group">
      <label for="housename">Address</label>
      <input type="text" class="form-control" id="sphouseName1">
    </div>
    <div class="form-group">
      <label for="street">Address 1</label>
      <input type="text" class="form-control" id="spstreet1" name="street">
    </div>
    <div class="form-group">
      <label for="city">city</label>
      <input type="text" class="form-control" id="spcity1" name="city">
    </div>
    
    <div class="form-group">
      <label for="city">State</label>
      <input type="text" class="form-control" id="state1" name="state1">
    </div>
    
    <div class="form-group">
      <label for="zipcode">Zip Code</label>
      <input type="text" class="form-control" id="spzip1" name="zip">
    </div>
    <div class="form-group">
      <label for="service" style="width:100%"><span class="req">*</span>SERVICE TYPE:<span class="serviceError">You must select atleast one service</span></label><br />
      <label class="checkbox-inline"><input type="checkbox" name="service" value="1">Tire</label>
      <label class="checkbox-inline"><input type="checkbox" name="service" value="2">Battery</label>
      <label class="checkbox-inline"><input type="checkbox" name="service" value="3">Gas</label>
    </div>
    <button type="submit" class="btn  customButton">CREATE ACCOUNT</button>
  </form>
     
     <p>By clicking “Create Account” , you agree to Qoda's <br>
                 <a href="http://qodamobile.com/terms-and-conditions-pages/">Terms and Conditions and Privacy Policy.</a></p>
     </div>
     
     
     <div class="modal fade" id="response" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
       
        <div class="modal-body">
          <p id="response_text"></p>
        </div>
       <div class="modal-footer">
          <a href="<?php echo base_url();?>"  class="btn btn-default">Ok</a>
        </div>
      </div>
    </div>
  </div>
  
  
                <div class="signup-form-wrapper" id="checkEmail">
    <div class="signup-form-inner">
        <h1>Sign Up to Get Roadside Assistance</h1>
        <div class="heading-seperator"></div>
        <p class="p_center">Welcome to Qoda, the easiest way to get around at the tap of a button.
        Create your account and get moving in minutes.</p>
       
  		<form class="form" action="" method="post" id="email_submit">
        <div class="form-group">
      <label for="service" style="text-transform: capitalize;">I would like to join as </label>
      <select class="form-control" required id="service">
      <option value="">---select---</option>
      <option value="1">Customer</option>
      <option value="2">Service Provider</option>
      </select>
    </div>
    <div class="form-group">
      <label for="email" style="text-transform: capitalize;">Enter your email:</label>
      <input type="email" class="form-control rule" id="email" placeholder="Enter email" name="email" required="required" autocomplete="off">
    </div>
    
    <button type="submit" class="btn btn-info btn-lg submit">Register</button>
  </form>    
 
 
  
</div>

 
  
  
  </div> </div>
  </div></div>