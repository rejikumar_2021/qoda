<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Register – QODA</title>
  <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
     <link href="<?php echo base_url(); ?>vendor/bootstrap/css/custom.css" rel="stylesheet">
    
<script src="<?php echo base_url(); ?>vendor/jquery/jqueryMin.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  
</head>
<body class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
          
            <div class="navbar-header logo">
           <li class="blOg">
                        <a href="http://qoda.adroitminds.com" class="hiddenLower">Blog</a>
                    </li>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a href="http://qoda.adroitminds.com" class="loGo"><img src="http://qoda.adroitminds.com/wp-content/uploads/2017/07/cropped-cropped-Qoda_Cs602.png" title="Qodamobile.com" alt="qodamobile.com" ></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
             
            <div class="collapse navbar-collapse mrtop" id="bs-example-navbar-collapse-1">
            
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                      <li class="showLowerr page-scroll">
                      <a href="http://qoda.adroitminds.com" >Blog</a>
                    </li>             
                    <li class="page-scroll">
                        <a href="http://qoda.adroitminds.com/?page_id=91">Help</a>
                    </li>
                    <li class="page-scroll">
                        <a href="http://qoda.adroitminds.com/?page_id=84">Contact</a>
                    </li>
                      <li >
                        <a href="<?php echo base_url(); ?>" class="register">Become a Service Provider</a>
                    </li>
                    
                    <li >
                        <a href="<?php echo base_url(); ?>index.php/login" class="login">Login</a>
                    </li>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>