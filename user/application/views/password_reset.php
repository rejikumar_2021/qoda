<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<img src="http://qoda.adroitminds.com/wp-content/uploads/2017/07/cropped-cropped-Qoda_Cs602.png" title="Qodamobile.com" alt="qodamobile.com" >
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
                <form class="form-horizontal" method="post" action="#">
                <?php
				echo $msg;
				?>
						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Enter your email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
									<input type="email" class="form-control" name="email" id="email"  placeholder="Enter your email" autocomplete="off" required/>
								</div>
							</div>
						</div>
                        
                        <div class="form-group ">
							<input type="submit" class="btn btn-primary btn-lg btn-block login-button" value="Reset">
						</div>
                        
                        </form>
                
                
                </div>
                </div>
                </div>
               