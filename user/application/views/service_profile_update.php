
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User profile
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Update profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
              <?php
			  echo validation_errors();
			  if(isset($success)==true)
			  {
				  ?>
                  <div class="alert alert-success">
                  Profile updated successfully
                  </div>
                  <?php
			  }
			  
			  if(isset($error)==true)
			  {
				  ?>
                  <div class="alert alert-warning">
                  OOPS!Something went wrong.Please try again later
                  </div>
                  <?php
			  }
			  ?>
                <!-- form start -->
                <?php
				$info=$this->session->userdata();
				if($info['testStatus']==0)
				{
				?>
                <div class="modal fade" id="surveyAlert" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        
        <div class="modal-body">
          <p>To become an official service provider, you must answer all quiz questions correctly. Before taking this short quiz, you must have passed the mandatory background check and watched the short training video. Proceed to quiz?</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-success" id="qsYes">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Not now</button>
        </div>
      </div>
    </div>
  </div>

 <?php
				}
				$st=$serInfo[0]->testStatus;
				
				?>
                <input type="hidden" id="base_url" value="<?php echo base_url();?>" />
                <form role="form" action="" method="post">
               
                  <div class="box-body">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">User Name</label>
                      <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" placeholder="Your name" value="<?php echo $serInfo[0]->spUserName; ?>" required="required" name="userName">
                  	</div>
                    </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                     <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="email" class="form-control" placeholder="Email" value="<?php echo $serInfo[0]->spEmail; ?>" name="userEmail">
                  	</div>
                    </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Phone number</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $serInfo[0]->spMobile; ?>" name="userPhone"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Address1</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $serInfo[0]->spAddress1; ?>" name="address1"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Address2</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $serInfo[0]->spAddress2; ?>" name="address2"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>state</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $serInfo[0]->spState; ?>" name="state"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    
                     <div class="col-md-6">
                    <div class="form-group">
                    <label>City</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $serInfo[0]->spCity; ?>" name="city"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Zipcode</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $serInfo[0]->spZipCode; ?>" name="zip"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    
                 
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div><!-- /.box -->

             
