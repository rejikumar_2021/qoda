<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Thank you
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">thank you</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
               
                 <?php
              $info=$this->session->userdata();
			  
			  if($info['userType']!="")
			  {
				  ?>
                  <div class="alert alert-success">  
                 Congratulations! You have passed the QODA quiz.  Your service provider account will be activated within 24 hours.<a href="<?php echo base_url(); ?>index.php/dashboard/update_profile"><button class="btn btn-md btn-info" style="margin-left: 1%;">OK</button></a></div>
                  <?php
			  }
			  else
			  {
				  ?>
                   <div class="alert alert-success">  
                Congratulations! You have passed the QODA quiz.  Your service provider account will be activated within 24 hours. <a href="<?php echo base_url(); ?>index.php/dashboard/update_user"><button class="btn btn-md btn-info" style="margin-left: 1%;">OK
          </button></a> </div>
                  <?php
			  }
			  ?>
                
              </div><!-- /.box -->
             