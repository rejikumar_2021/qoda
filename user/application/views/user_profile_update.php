
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User profile
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Update profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
              <?php
			  echo validation_errors();
			  if(isset($success)==true)
			  {
				  ?>
                  <div class="alert alert-success">
                  Profile updated successfully
                  </div>
                  <?php
			  }
			  
			  if(isset($error)==true)
			  {
				  ?>
                  <div class="alert alert-warning">
                  OOPS!Something went wrong.Please try again later
                  </div>
                  <?php
			  }
			  ?>
                <!-- form start -->
                <form role="form" action="" method="post">
                  <div class="box-body">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">User Name</label>
                      <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" placeholder="Your name" value="<?php echo $cusInfo[0]->userName; ?>" required="required" name="userName">
                  	</div>
                    </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                     <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="email" class="form-control" placeholder="Email" value="<?php echo $cusInfo[0]->userEmail; ?>" name="userEmail">
                  	</div>
                    </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Phone number</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $cusInfo[0]->userMobile; ?>" name="userPhone"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Address1</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $cusInfo[0]->address1; ?>" name="address1"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Address2</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $cusInfo[0]->address2; ?>" name="address2"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>state</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $cusInfo[0]->state; ?>" name="state"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    
                     <div class="col-md-6">
                    <div class="form-group">
                    <label>City</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $cusInfo[0]->city; ?>" name="city"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    <div class="col-md-6">
                    <div class="form-group">
                    <label>Zipcode</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                      </div>
                      <input type="text" class="form-control" value="<?php echo $cusInfo[0]->zipCode; ?>" name="zip"/>
                    </div><!-- /.input group -->
                  </div>
                    </div>
                    
                    
                 
                    
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div><!-- /.box -->

             
