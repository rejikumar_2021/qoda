<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Qoda</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Choose your account</h4>
      </div>
      <div class="modal-body">
        <form action="" method="post">
  <div class="form-group">
    <label for="accountType">Login As</label>
    <select class="form-control" required id="loginChoice" name="loginChoice">
    <option value="">---select---</option>
    <option value="1">Service Provider</option>
    <option value="2">Customer</option>
    </select>
  </div>
  <button type="submit" class="btn btn-info btn-md">Continue</button>
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>

  </div>
</div>
</body>
<script>
$(function(){
	
$('#myModal').modal('show'); 

$('form').submit(function(e){
	var type=$('#loginChoice').val();
	if(type=="")
	{
		e.preventDefault();
	}
	});
});
</script>
</html>