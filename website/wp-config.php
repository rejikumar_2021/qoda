<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'qoda_web');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VI059#6]`Yxwhj@GtUroeN8P%=ZwBwFijQZwZ0y|dm%uE8*hg;UEmLp&Xw(.Ocvy');
define('SECURE_AUTH_KEY',  '(`M`NUoAfW|[GN-rU)85e.qPfuqZ UPqRFHGS>L]0,z?22cnf5oSHp<tPSh,R^4+');
define('LOGGED_IN_KEY',    '^nou>0+L#9cC[x/P1>|,0Ml3$7nsX% Qv j:pjJ_GOMC>6(Q(s;7W/{iqJ(Ds=y*');
define('NONCE_KEY',        '$a^HzTn-x+V~4<J14kF+vlL$7?f1$,C$n,nI!fF}DonmSaVy%b-;7uF,A=M TN|]');
define('AUTH_SALT',        '%<Q[2h8#f9/u^DX_l5%Yu`Uh>LdF)x{v*v: (ea4[;6^]O;qT:5|b%@;7P@b>%=2');
define('SECURE_AUTH_SALT', 'OrN&9dSlKGk/JaSj/Pt?mR0b?=$LZ$*4<P#?H#AXR7g+d4T<g-W+^E&gr0/C 9=d');
define('LOGGED_IN_SALT',   'XCsJW3OTZaCIo<+dPEF|dfhYzw_D-s-t8n6sarE+,/+$7f)Qcw:I9)2wE`uA5oXm');
define('NONCE_SALT',       'a#c{reki%A9nE&2>x!{T@Dr;V}Ge( @c&~,/*mdP7u{&(_apK^b%0Tae:_!X%}NI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
