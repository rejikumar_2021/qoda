<?php

get_header();
?>
<div class="main-content-wrapper">
<div class="breadcrumb_wrapper">
<div class="block-content-inner">
<?php do_shortcode('[breadcrumb]');?>
</div>
</div>
<div class="breadcrumb">
    
</div>
<div class="block-content-inner">
    <div class="content_left">
    <div class="single-post-wrapper">
<?php
if(have_posts()):the_post();
    ?>
    <div class="single-post-item">
        <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
        <div class="post-info">By: <?php echo get_the_author();?> - <?php echo get_the_date('M, d');?> - <?php the_category(', ');?> - <?php comments_number( '0 comments', '1 comment', '% comments' );?></div>
        <?php if(has_tag){?><div class="tag-list"><?php the_tags();?></div><?php }?>
        <div class="content_single_detail">        
            <?php echo get_the_content();?>
        </div>
    </div>    
    <?php
endif;
?>
    <div class="page_elem_seperator"></div>
    <div class="share_social">
        Share: <?php
        $crunchifyURL=get_the_permalink();
        $crunchifyTitle=get_the_title();
        $crunchifyThumbnail=get_the_post_thumbnail_url();        
        $twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL.'&amp;via=Crunchify';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
        $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail.'&amp;description='.$crunchifyTitle;
        ?>
        <ul class="share-social-list">
            <li><a target="_blank" href="<?php echo $facebookURL?>"><span class="social-icon facebook"></span></a></li>
            <li><a target="_blank" href="<?php echo $twitterURL?>"><span class="social-icon twitter"></span></a></li>
            <li><a target="_blank" href="<?php echo $googleURL?>"><span class="social-icon googleplus"></span></a></li>
            <li><a target="_blank" href="<?php echo $pinterestURL?>"><span class="social-icon pinterest"></span></a></li>
        </ul>
    </div>
    <div class="comment-single">
    <?php
    if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
    ?>
    </div>
    </div>
</div>
<div class="content_right">
    <?php get_sidebar();?>
</div>
</div>
</div>
<div class="block-author-wrapper space-top">
<div class="block-author-inner">
        <img src="/blog/wp-content/themes/qoda/images/chichi.png">
        <p>CHICHI MADUS</p>
        <p>QODA CEO &amp; Founder</p>
        <p><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> support@qodamobile.com</a></p>
    </div>
</div>
<?php
get_footer();
?>