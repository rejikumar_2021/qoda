<?php
/* Template name: Reset password page */

get_header();
?>
<div class="signup-form-wrapper">
    <div class="signup-form-inner">
        <h1>Forgot Password</h1>
        <div class="heading-seperator"></div>
        <p class="p_center">Enter your email or phone number to reset password.<br/>We only support reseting password by phone number for driver accounts.</p>
        <div class="signup-form">
            <form action="" method="post" id="form-reset-password">
                <p>
                    <label for=""><span>*</span> EMAIL</label>
                    <input type="email" name="mail" placeholder="Name@example.com"/>
                </p>                        
                <div class="clear-fix"></div>
                <p>
                    <input type="submit" value="RESET PASSWORD" name="submit-reset-password"/>
                </p>
            </form>
            <p class="p_center"><a href="#">Sign in to Qoda</a></p>
            <p class="p_center">Don't have an account? <a href="#">Sign up</a></p>
        </div>
    </div>
</div>
<?php
get_footer();
?>