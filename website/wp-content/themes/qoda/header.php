<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="aht tech" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
	$url=get_bloginfo('wpurl');
	?>
    <link href="<?php echo $url ?>/wp-content/themes/qoda/css/font-awesome.min.css" rel="stylesheet"/>
	<?php wp_head();?>
</head>
<body <?php body_class(); ?>>
<?php
global $current_user; // Use global
get_currentuserinfo();
if ( user_can( $current_user, "administrator" ) ){ 
}else { ?>
     <?php show_admin_bar( false );?>
<style>
html{
margin-top:0!important;
}
</style>
<?php } ?>
<div class="main-container">
    <div class="header-landing">
        <div class="header-landing-logo">
            <!--a href=""><img src="/qoda/wp-content/themes/qoda/images/logo-img.png"/></a-->
            <?php the_custom_logo();?>
            <div class="header-landing-reponsive-menu">
            <div class="control-responsive">
            </div>
        </div>
        </div>
        <div class="header-landing-left">
            <nav>
                <!--ul class="menu-left">
                    <li class="responsive-icon"><a href="#"></a></li>
                    <li><a href="#">QODA</a></li>
                    <li><a href="#">Ride</a></li>
                    <li><a href="#">Driver</a></li>
                </ul-->
                <?php if ( has_nav_menu( 'primary-left' ) ) : ?>
                <?php
					// Primary navigation menu.
					wp_nav_menu( array(
						'menu_class'     => 'menu-left',
						'theme_location' => 'primary-left',
					) );
				?>
                <?php endif;?>
            </nav>
        </div>
        <div class="header-landing-right">
            <nav>
                <!--ul class="menu-right">
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Contact</a></li>
                    <li class="link-btn-blue"><a href="#">Become a Driver</a></li>
                </ul-->
                <?php if ( has_nav_menu( 'primary-right' ) ) : ?>
                <?php
					// Primary navigation menu.
					wp_nav_menu( array(
						'menu_class'     => 'menu-right',
						'theme_location' => 'primary-right',
					) );
				?>
                <?php endif;?>
            </nav>
        </div>
            <nav>
                <ul class="menu-responsive">
                </ul>
            </nav>
    </div>
    <div class="clear-fix"></div>
    <div class="content-wrapper">
