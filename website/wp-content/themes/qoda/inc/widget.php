<?php
class qoda_category_widget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct(
	           'qoda_category_widget', // Base ID
        	   __('QODA category widget', 'qoda'), // Name
 	           array( 'description' => __( 'QODA category widget', 'qoda' ), ) // Args
		);
	}

	function widget( $args, $instance ) {
		//echo '<h2>'.$instance['title'].'</h2>';
		//echo $instance['cat_id'];
        if( $instance) {
			$title = esc_attr($instance['title']);
		}
        ?>
        <div class="category_content_wrapper">
        <h2 class="widget-title bg_purple_line"><?php echo $title;?></h2><?php
            
        $list_cates=get_categories(array(
            'type'                     => 'post',
            'orderby'                  => 'term_id',
            'order'                    => 'ASC',
            'hide_empty'               => 0,
            'taxonomy'                 => 'category'
            ));
            //var_dump($cates);
            ?>
            <ul class="list_category_wrapper">
            <?php
            wp_reset_query();
            $current_cat='0';$parent_cat='0';
            foreach($list_cates as $c1){                    
                if($c1->term_id==get_query_var('cat')){
                    $current_cat=$c1->term_id; 
                $parent_cat=$c1->parent;   
                }
            }
            $list_cates1=$list_cates;
            foreach($list_cates as $c){
                if($c->parent==0){
        ?>
            <li><span class=""></span><a class="<?php if($c->term_id==$current_cat && is_category()){echo 'current_active';}?>" href="<?php echo get_category_link($c->term_id)?>"><?php echo $c->name;?></a>
                <?php
                $i=0;$test=false;
                
                foreach($list_cates1 as $c1){
                    if($c1->parent==$c->term_id){
                        if($i==0){ 
                            $test=true;
                        ?><ul class="<?php if(($c->term_id==$parent_cat || $c->term_id==$current_cat) && is_category()){echo 'show_expand';}?>">
                        <?php }
                        ?><li><a class="<?php if($c1->term_id==$current_cat && is_category()){echo 'current_active';}?>" href="<?php echo get_category_link($c1->term_id)?>"><?php echo $c1->name;?></a></li><?php
                        $i++;    
                    }
                }
                if($test){?></ul><?php }
                ?>
            </li>
        <?php
                }
            }
            ?></ul></div><?php
            
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		// Fields
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
    
	// Widget form creation
	function form($instance) {
	 	$title = '';
		$text = '';

		// Check values
		if( $instance) {
			$title = esc_attr($instance['title']);
			$text = esc_attr($instance['text']);
		} ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'qoda'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		
	<?php }
}

class qoda_comment_status_widget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct(
	           'qoda_comment_status_widget', // Base ID
        	   __('QODA comment/latest posts widget', 'qoda'), // Name
 	           array( 'description' => __( 'QODA comment/latest posts widget', 'qoda' ), ) // Args
		);
	}

	function widget( $args, $instance ) {
		//echo '<h2>'.$instance['title'].'</h2>';
		//echo $instance['cat_id'];
        if( $instance) {
			$title = esc_attr($instance['title']);
			$title1 = esc_attr($instance['title1']);
			$num_comments = esc_attr($instance['num_comments']);
            $num_comments = ($num_comments!='')?$num_comments:5;
			$num_posts = esc_attr($instance['num_posts']);
            $num_posts = ($num_posts!='')?$num_posts:3;
		}
        
        global $post;
        ?>
        <div class="comment_featured_wrapper">
            <div class="comment_featured_tab_control">
                <ul>
                    <li class="active_control"><a href="#comment_tab"><?php echo ($title!='')?$title:'Comments';?></a></li>
                    <li><a href="#latest_tab"><?php echo ($title1!='')?$title1:'Latest posts';?></a></li>
                </ul>
            </div>
                <div class="tab_content_inner show_expand" id="comment_tab">
                    <ul class="comment_list">
                        <?php
                        $args = array(
                            'status' => 'approve',
                            'number' => $num_comments,
                            'order' => 'DESC'
                        );
                        $comments = get_comments($args);
                    
                        foreach($comments as $comment) : $count++;                
                            $post_args = array(
                                'post_type' => 'post',
                                'p' => $comment->comment_post_ID,
                                'posts_per_page' => $num_posts
                                );
                
                            $posts = get_posts($post_args);
                
                            foreach($posts as $post) : setup_postdata($post);
                                ?>
                                <li class="m_bottom_15">
        						<span><?php echo get_comment_author( $comment->comment_ID );?></span> on <a href="<?php echo get_page_link($post->ID);?>" class="color_dark"><?php echo $post->post_title;?></a>
        					   </li>
                                <?php                
                            endforeach;
                        endforeach;
                        wp_reset_postdata();
                        ?>
    				</ul>
                </div>
                <div class="tab_content_inner" id="latest_tab">
                    <ul class="latest_posts_inner">
                        <?php
                        global $post;
                        $args=array('post_type'=>'post','posts_per_page' => $num_posts,'order'=>'desc');
                        $posts = get_posts($args);                
                        foreach($posts as $post) : setup_postdata($post);
                          ?><li><a href="<?php the_permalink();?>"><?php if(has_post_thumbnail()){the_post_thumbnail();}the_title();?><p><i><?php echo 'by '.get_the_author().' - '.get_the_date('M, d');?></i></p></a></li><?php
                        endforeach;
                        wp_reset_postdata();
                        ?>
                    </ul>
                </div>
        </div>
        <?php
            
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		// Fields
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['title1'] = strip_tags($new_instance['title1']);
		$instance['num_comments'] = strip_tags($new_instance['num_comments']);
		$instance['num_posts'] = strip_tags($new_instance['num_posts']);
		return $instance;
	}
    
	// Widget form creation
	function form($instance) {
	 	$title = '';
		$text = '';

		// Check values
		if( $instance) {
			$title = esc_attr($instance['title']);
			$title1 = esc_attr($instance['title1']);
			$num_comments = esc_attr($instance['num_comments']);
			$num_posts = esc_attr($instance['num_posts']);
		} ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'qoda'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo ($title!='')?$title:'Comments'; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('num_comments'); ?>"><?php _e('Number comments:', 'qoda'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('num_comments'); ?>" name="<?php echo $this->get_field_name('num_comments'); ?>" type="text" value="<?php echo ($num_comments!='')?$num_comments:6; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('title1'); ?>"><?php _e('Title 2:', 'qoda'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title1'); ?>" name="<?php echo $this->get_field_name('title1'); ?>" type="text" value="<?php echo ($title1!='')?$title1:'Latest posts'; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Number posts:', 'qoda'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('num_posts'); ?>" name="<?php echo $this->get_field_name('num_posts'); ?>" type="text" value="<?php echo ($num_posts!='')?$num_posts:3; ?>" />
		</p>
		
	<?php }
}

class qoda_featured_widget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct(
	           'qoda_featured_widget', // Base ID
        	   __('QODA featured widget', 'qoda'), // Name
 	           array( 'description' => __( 'QODA featured widget', 'qoda' ), ) // Args
		);
	}

	function widget( $args, $instance ) {
		//echo '<h2>'.$instance['title'].'</h2>';
		//echo $instance['cat_id'];
        if( $instance) {
			$title = esc_attr($instance['title']);
            $title=($title!='')?$title:'feature posts';
			$nums = esc_attr($instance['nums']);
            $nums=($nums!='')?$nums:2;
		}
        global $post;
        ?>
        <div class="featured_posts_wrapper">
        <h2 class="widget-title"><?php echo $title;?></h2><ul class="list_featured"><?php
        
        $query = get_posts(  array( 'post_type'=>'post','posts_per_page'=>$nums,'meta_key' => '_is_ns_featured_post', 'meta_value' => 'yes' ) );
        foreach($query as $post):setup_postdata($post);
            ?>
            <li>
            <a href="<?php the_permalink();?>"><?php the_post_thumbnail();?>
                <h3><?php the_title();?></h3></a>
                <p><i>By: <?php echo get_the_author();?> - <?php echo get_the_date('M, d');?></i></p>
                <p><?php echo wp_trim_words(get_the_content(),10);?> <a href="<?php the_permalink();?>">MORE</a></p>
            </li>
            <?php
        endforeach;
        wp_reset_postdata();
        ?></ul>
        </div>
        <?php
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		// Fields
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['nums'] = strip_tags($new_instance['nums']);
		return $instance;
	}
    
	// Widget form creation
	function form($instance) {
	 	$title = '';
		$text = '';

		// Check values
		if( $instance) {
			$title = esc_attr($instance['title']);
			$nums = esc_attr($instance['nums']);
		} ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'qoda'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('nums'); ?>"><?php _e('Number posts:', 'qoda'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('nums'); ?>" name="<?php echo $this->get_field_name('nums'); ?>" type="text" value="<?php echo ($nums!='')?$nums:2; ?>" />
		</p>
		
	<?php }
}

function qoda_register_widgets() {
	register_widget( 'qoda_category_widget');
	register_widget( 'qoda_comment_status_widget');//
	register_widget( 'qoda_featured_widget');
}
add_action( 'widgets_init', 'qoda_register_widgets' );