<?php
/* Template name: Contact page */

get_header();
?>
<div class="signup-form-wrapper bg_bridge">
    <div class="signup-form-inner">
        <h1>Contact us</h1>
        <div class="heading-seperator"></div>
        <p class="p_center">Send your message here.</p>
        <div class="contact-form">
            <?php echo do_shortcode('[contact-form-7 id="40" title="Contact page"]');?>
        </div>
    </div>
</div>
<?php
get_footer();
?>