<?php
/* Template name: Login page */

get_header();
?>
<div class="signup-form-wrapper">
    <div class="signup-form-inner">
        <h1>Sign In</h1>
        <div class="heading-seperator"></div>
        <p class="p_center">Welcome back to Qoda, Please Sign in to use</p>
        <div class="signup-form">
            <form action="" method="post" id="form-sign-in">
                <p>
                    <label for=""><span>*</span> EMAIL</label>
                    <input type="email" name="mail" placeholder="Name@example.com"/>
                </p>
                <p>
                    <label for=""><span>*</span> PASSWORD</label>
                    <input type="email" name="pass" placeholder="At leat 8 characters"/>
                </p>                        
                <div class="clear-fix"></div>
                <p>
                    <input type="submit" value="SIGN IN" name="submit-signin"/>
                </p>
            </form>
            <p class="p_center"><a href="#">Forgot password</a></p>
            <p class="p_center">Don't have an account? <a href="#">Sign up</a></p>
        </div>
    </div>
</div>

<?php
get_footer();
?>