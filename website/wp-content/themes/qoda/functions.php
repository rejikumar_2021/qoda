<?php
function qoda_setup() {

	//load_theme_textdomain( 'qoda' );

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	register_nav_menus( array(
		'primary-left' => __( 'Primary Menu Left',      'qoda' ),
		'primary-right' => __( 'Primary Menu Right',      'qoda' ),
		'menu-footer'  => __( 'Footer Menu', 'qoda' ),
	) );

	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
	) );

}
add_action( 'after_setup_theme', 'qoda_setup' );

function theme_enqueue_script(){
    
	wp_enqueue_style( 'font-awesome-style', get_template_directory_uri() . '/css/font-awesome.min.css' );
	wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_script( 'jquery-script', get_template_directory_uri() . '/js/jquery-3.1.js' );
	wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/js/script.js' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_script' );

function qoda_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar Widget area', 'qoda' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'qoda' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'qoda_widgets_init' );

include('inc/widget.php');

function qoda_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'div';
		$add_below = 'div-comment';
	}
?>
<div class="comment-box <?php empty( $args['has_children'] ) ? '' : 'comment_level_2 ' ;?>">
	<div class="d_table w_full m_bottom_20">
    	<div class="comment-content relative r_corners bg_light_3 fw_light">
            <a href="#" class="link-author-avatar">
            <?php echo get_avatar( $comment, $args['avatar_size'] );?>
            </a>
            <?php comment_text();?>
            <?php
            comment_reply_link( array_merge( $args, array( 
                'reply_text' => '<span class="d_inline_m m_right_5 icon_wrap_size_0 circle color_grey_light tr_all">
    					<i class="icon-reply"></i>
    				</span>Reply <i class="fa fa-reply" aria-hidden="true"></i>',            
                'depth' => $depth,
                'max_depth' => $args['max_depth'] ,
                'class'=>'color_purple d_inline_b color_pink_hover d_block f_right fw_light'
                ) ) );
            ?>
        </div>
		<div class="comment-infor">
		          <!--author name-->				
				<p class="fs_medium color_grey d_mxs_none"><?php echo get_comment_author_link();?> <span>/ <?php echo get_comment_date('M d, Y');?></span></p>
			
		</div>
	</div>
</div>

<?php
}
function bwpm_pre_get_posts($query){
    if(!$query->get('post_type')=='post'){
        $query->set('posts_per_page',6);
    }    
}
add_action('pre_get_posts','bwpm_pre_get_posts');

function breadcrumb_html(){
$style='';
    //if($var[0]=='' || $var[0]=='light'){
//        $style='color_grey_light_3';
//    }else{
//        $style='color_default';
//    }
$breadcrumb_separator='';
    $str='<ul class="hr_list d_inline_m breadcrumbs">
		<li class="m_right_8 f_xs_none"><a href="index.html" class="'.$style.' d_inline_m m_right_10">Home</a><i class="icon-angle-right d_inline_m color_grey_light_3 fs_small">›</i></li>
		<li class="m_right_8 f_xs_none"><a href="#" class="'.$style.' d_inline_m m_right_10">Pages</a><i class="icon-angle-right d_inline_m color_grey_light_3 fs_small">›</i></li>
		<li class="f_xs_none"><a class="'.$style.' d_inline_m">1 Column With Right Sidebar</a></li>
	</ul>';
	global $post;
	$html  = '';
	$html  .= '<style type="text/css">';				
	$html  .= '
			.breadcrumb-container {
			  font-size: '.$breadcrumb_font_size.';
			}	
			.breadcrumb-container li a {
			  color: '.$breadcrumb_link_color.';
			}			
			.breadcrumb-container li .separator {
			  color: '.$breadcrumb_separator_color.';
			}	
	';				
	$html  .= '</style>';	
	$html .= '
	<ul class="hr_list d_inline_m breadcrumbs">';
	$html .= '';		
	$html .= '<li class="m_right_8 f_xs_none">'.$breadcrumb_text.'</li>';		
	if(is_home())
		{			
			$html .= '<li class="m_right_8 f_xs_none"><a title="Home" href="#">Home</a><i>></i></li><li><a href="'.home_url('/work/adroit/wordpress').'">Blog</a><i></i></li>';			
		}		
	else if(is_attachment())
		{
			$current_attachment_id = get_query_var('attachment_id');
			$current_attachment_link = get_attachment_link($current_attachment_id);	
			$html .= '<li><a title="Home" href="'.get_bloginfo('url').'">Home</a></li>';
			$html .= '<li><a href="'.$current_attachment_link.'">'.get_the_title().'</a><span>'.$breadcrumb_separator.'</span></li>';
		}
	else if(is_singular())
		{
			$post_parent_id = wp_get_post_parent_id(get_the_ID());
			$parent_title = get_the_title($post_parent_id);
			$paren_get_permalink = get_permalink($post_parent_id);
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="Home" href="'.get_bloginfo('url').'">Home</a><i class="icon-angle-right d_inline_m '.$style.' fs_small">></i></li>';
            $cates=get_the_category(get_the_ID());
            $option=get_option("brilliantred_option");
            $cate='';$catelink='';
            foreach($cates as $c){
                $cate=$c->name;
                $catelink=$c->slug;
            }			
			if(is_page() ){				
					$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="'.get_the_title().'" href="#">'.get_the_title().'</a><i class="icon-angle-right d_inline_m '.$style.' fs_small"></i></li>';
				}else{
				    //$post_categories = wp_get_post_category( get_the_ID() );
                    //$post_categories->
				    $html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="'.$cate.'" href="'.home_url().'/'.$catelink.'">'.$cate.'</a><i class="icon-angle-right d_inline_m '.$style.' fs_small">></i></li><li><a class="'.$style.' d_inline_m m_right_10" title="'.get_the_title().'" href="#">'.get_the_title().'</a><i class="icon-angle-right d_inline_m '.$style.' fs_small"></i></li>';
				}		
	}else if(is_front_page() ){				
					$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="'.get_the_title().'" href="#">'.get_the_title().'</a><i class="icon-angle-right d_inline_m '.$style.' fs_small"></i></li>';
				}
	else if( is_tax())
		{
			$queried_object = get_queried_object();
			$term_name = $queried_object->name;	
			$html .= '<li><a title="Home" href="'.get_bloginfo('url').'">Home</a><i class="icon-angle-right d_inline_m '.$style.' fs_small">></i></li>';
			$html .= '<li><a href="#">'.$term_name.'</a><i class="icon-angle-right d_inline_m '.$style.' fs_small"></i></li>';
		}		
	else if(is_category())
		{			
			$current_cat_id = get_query_var('cat');
			$parent_cat_links = get_category_parents( $current_cat_id, true, '<i>></i></li><li>' );
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="Home" href="'.get_bloginfo('url').'">Home</a><i class="icon-angle-right d_inline_m '.$style.' fs_small">></i></li>';
			$html .= '<li class="m_right_8 f_xs_none">'.$parent_cat_links.'</li>';
		}		
	else if(is_tag())
		{
			$current_tag_id = get_query_var('tag_id');
			$current_tag = get_tag($current_tag_id);
			$current_tag_name = $current_tag->name;	
			$current_tag_link = get_tag_link($current_tag_id);;				
			//$parent_cat_links = get_category_parents( $current_cat_id, true, $breadcrumb_separator );
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="Home" href="'.get_bloginfo('url').'">Home</a><i class="icon-angle-right d_inline_m '.$style.' fs_small">></i></li>';			
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="Home" href="'.$current_tag_link.'">'.$current_tag_name.'</a><i class="icon-angle-right d_inline_m '.$style.' fs_small"></i></li>';
		}	
	else if(is_author())
		{
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="Home" href="'.get_bloginfo('url').'">Home</a><i>></i></li>';
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" href="'.esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ).'">'.get_the_author().'</a></li>';
		}		
	else if(is_search())
		{
			$current_query = sanitize_text_field(get_query_var('s'));
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" title="Home" href="'.get_bloginfo('url').'">Home</a><i class="icon-angle-right d_inline_m '.$style.' fs_small">></i></li>';
			$html .= '<li class="m_right_8 f_xs_none"><a class="'.$style.' d_inline_m m_right_10" href="#">'.$current_query.'</a><i class="icon-angle-right d_inline_m '.$style.' fs_small"></i></li>';
		}
	else if(is_404())
		{
			$html .= '<li><a title="Home" href="'.get_bloginfo('url').'">Home</a><i class="icon-angle-right d_inline_m '.$style.' fs_small">></i></li>';
			$html .= '<li><a href="#">404</a></li>';
		}		
	else
		{
			$html .= '';
		}			
	$html .= '</ul>';
	$html .= '';
	return $html;
}
function breadcrumb_display($atts, $content = null ) {
	$atts = shortcode_atts(
	array(
		'id' => "",
        'var'=>""
		), $atts);
	echo breadcrumb_html($atts['var']);
}

add_shortcode('breadcrumb', 'breadcrumb_display'); 

//Sync users
add_action('wp_synchronization_users','wp_synchronization_users');
function wp_synchronization_users(){
	
	global $wpdb;
	$customers = getSecondDatas();
	$email_notification = get_option('sync_email_notice', '');
	$adminEmail = get_option('admin_email','');
	//$headers = array('Content-Type: text/html; charset=UTF-8');
	$headers = "From: " . $adminEmail . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$connect = getSecondConnect();
	$messages = '';
	$tableUser = $wpdb->users;
	$tableUserMeta = $wpdb->usermeta;
	if(isset($customers)) {
		foreach($customers as $customer) {
			$email = $customer['user_email'];
			$user_id =  email_exists($email);
			$first_name = (!empty($customer['first_name'])) ? $customer['first_name'] : '&nbsp;';
			$last_name = (!empty($customer['last_name'])) ? $customer['last_name'] : '&nbsp;';
			$ph_no = (!empty($customer['ph_no'])) ? $customer['ph_no'] : 0;
			$fb_id = (!empty($customer['fb_id'])) ? $customer['fb_id'] : 0;
			$user_image = (!empty($customer['user_image'])) ? $customer['user_image'] : '&nbsp;';
			$paypal_user_name = (!empty($customer['paypal_user_name'])) ? $customer['paypal_user_name'] : '&nbsp;';
			$paypal_email = (!empty($customer['paypal_email'])) ? $customer['paypal_email'] : '&nbsp;';
			$service = (!empty($customer['service'])) ? $customer['service'] : '&nbsp;';
			$temp_password = (!empty($customer['temp_password'])) ? $customer['temp_password'] : '&nbsp;';
			$remember_token = (!empty($customer['remember_token'])) ? $customer['remember_token'] : '&nbsp;';
			$user_token = (!empty($customer['user_token'])) ? $customer['user_token'] : '&nbsp;';
			$lattitude = (!empty($customer['lattitude'])) ? $customer['lattitude'] : '&nbsp;';
			$longitude = (!empty($customer['longitude'])) ? $customer['longitude'] : '&nbsp;';
			$device_token = (!empty($customer['device_token'])) ? $customer['device_token'] : '&nbsp;';
			$userSettingsTime = (!empty($customer['wp_usersettingstime'])) ? $customer['wp_usersettingstime'] : '&nbsp;';
			if ( !$user_id && email_exists($email) == false ) {
				$display_name = $customer['last_name'] . ' ' . $customer['first_name'];
				$split = explode('@',$customer['user_email']);				
				$nicename = $split[0];
				$wpdb->query( "INSERT INTO " . $tableUser ." (`user_login`,`user_pass`,`user_nicename`,`user_email`,`user_registered`,`display_name`) VALUES ('" . $nicename ."','". $customer['user_pass'] ."','". $nicename ."','". $email ."','". $customer['user_registered'] ."','". $display_name ."')");			
		
				$user_id = $wpdb->insert_id;
				if($user_id){
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'nickname','". $nicename ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'first_name','". $first_name ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'last_name','". $last_name ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'ph_no',". $ph_no .")");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'fb_id',". $fb_id .")");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'user_image','". $user_image ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'paypal_user_name','". $paypal_user_name ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'paypal_email','". $paypal_email ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'service','". $service ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'temp_password','". $temp_password ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'remember_token','". $remember_token ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'user_token','". $user_token ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'lattitude','". $lattitude ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'longitude','". $longitude ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'device_token','". $device_token ."')");
					$wpdb->query( "INSERT INTO " . $tableUserMeta ." (`user_id`,`meta_key`,`meta_value`) VALUES (" . $user_id .",'wp_usersettingstime','". $userSettingsTime ."')");
					$user = new WP_User( $user_id );
					$user->set_role( 'subscriber' );
					$messages .= "We've been added info from email: " . $email . '<br/>';
				} else {
					$messages .= "We can't add info from email: " . $email . '<br/>';
				}
			} else {
				$adate = $customer['wp_usersettingstime'];
				$wpdate = get_user_meta( $user_id, 'wp_usersettingstime', true );
				if($adate > $wpdate) {
					$wpdb->query("UPDATE `". $tableUser ."` SET `user_pass` = '". $customer['user_pass'] ."' WHERE `ID` = ". $user_id ." AND `user_email` = '". $email ."'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $last_name ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'last_name'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = ". $ph_no ." WHERE `user_id` = ". $user_id ." AND `meta_key` = 'ph_no'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = ". $fb_id ." WHERE `user_id` = ". $user_id ." AND `meta_key` = 'fb_id'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $user_image ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'user_image'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $paypal_user_name ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'paypal_user_name'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $paypal_email ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'paypal_email'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $service ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'service'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $temp_password ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'temp_password'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $remember_token ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'remember_token'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $user_token ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'user_token'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $lattitude ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'lattitude'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $longitude ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'longitude'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $device_token ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'device_token'");
					$wpdb->query("UPDATE `". $tableUserMeta ."` SET `meta_value` = '". $userSettingsTime ."' WHERE `user_id` = ". $user_id ." AND `meta_key` = 'wp_usersettingstime'");
					$messages .= "We've been updated info from email: " . $email . '<br/>';
				} else {
					$messages .= "We don't synchronization user by email: " . $email . '<br/>';
				}
			}
		}		
	} else {
		$messages .= "We can't synchronization users FROM QODA TO WP by now" . "<br/>";
	}
	//return $messages;
	wp_mail( $email_notification, 'Synchronization users FROM QODA TO WP', $messages, $headers);
}

add_action('qoda_synchronization_users','qoda_synchronization_users');
function qoda_synchronization_users(){
	$email_notification = get_option('sync_email_notice', '');
	$adminEmail = get_option('admin_email','');
	//$headers = array('Content-Type: text/html; charset=UTF-8');
	$headers = "From: " . $adminEmail . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	$connect = getSecondConnect();
	if(isset($connect)){
		$dbName = get_option( 'su_con_db_name', '' );
		mysql_select_db($dbName,$connect);
		$wpUsers = getAllUsersWP();
		$anotherUsers = getSecondDatas();
		$query = '';
		$messages = '';
		if($wpUsers && $anotherUsers) {		
			foreach($wpUsers as $wpUser) {
				$email = $wpUser['user_email'];
				//$k = array_search($email, array_column($anotherUsers, 'user_email'));
				$k = recursive_array_search($email, $anotherUsers);
				$ph_no = (!empty($wpUser['ph_no'])) ? $wpUser['ph_no'] : 0;
				$fb_id = (!empty($wpUser['fb_id'])) ? $wpUser['fb_id'] : 0;
				$wpdate = get_user_meta( $wpUser->ID, 'wp_usersettingstime', true );
				if(isset($k) && $k >= 0 && $anotherUsers[$k]['user_email'] == $email) {
					$adate = $anotherUsers[$k]['wp_usersettingstime'];				
					if($adate < $wpdate) {
						$query .= "UPDATE users SET `first_name` = '". $wpUser['first_name'] ."', `last_name` = '". $wpUser['last_name'] ."', `ph_no` = ". $ph_no .", `password` = '". $wpUser['user_pass'] ."',`temp_password` = '". $wpUser['temp_password'] ."', `fb_id` = ". $fb_id .", `user_image` = '". $wpUser['user_image'] ."', `paypal_user_name` = '". $wpUser['paypal_user_name'] ."', `paypal_email` = '". $wpUser['paypal_email'] ."',`service` = '". $wpUser['service'] ."',`remember_token` = '". $wpUser['remember_token'] ."',`user_token` = '". $wpUser['user_token'] ."', `lattitude` = '". $wpUser['lattitude'] ."', `longitude` = '". $wpUser['longitude'] ."', `device_token` = '". $wpUser['device_token'] ."', `updated_at` = '". date('Y-m-d H:m:i',$wpdate) ."' WHERE email='". $email . "';";					
						
						$messages .= "We've synchronizated user by email". $email . "<br/>";
					} else {
						$messages .= "We don't synchronization user by email". $email . "<br/>";
					}
				} else {
					$query .= "INSERT INTO `users` (`user_type`,`login_type`,`first_name`,`last_name`,`ph_no`,`email`,`password`,`temp_password`,`fb_id`,`user_image`,`paypal_user_name`,`paypal_email`,`service`,`status`,`remember_token`,`user_token`,`lattitude`,`longitude`,`device_token`,`created_at`,`updated_at`,`admin_approved`,`notification_status`) VALUES (1,1,'". $wpUser['first_name'] ."','". $wpUser['last_name'] ."',". $ph_no .",'". $email ."','". $wpUser['user_pass'] ."','". $wpUser['temp_password'] ."',". $fb_id .",'". $wpUser['user_image'] ."','". $wpUser['paypal_user_name'] ."','". $wpUser['paypal_email'] ."','". $wpUser['service'] ."',1,'". $wpUser['remember_token'] ."','". $wpUser['user_token'] ."','". $wpUser['lattitude'] ."','". $wpUser['longitude'] ."','". $wpUser['device_token'] ."','". $wpUser['user_registered'] ."','". $wpUser['user_registered'] ."',1,1);";
					$messages .= "We've added user by email ". $email . "<br/>";
				}
			}
			mysql_query($query,$connect);			
		} elseif($wpUsers && empty($anotherUsers)) {
			foreach($wpUsers as $wpUser) {
				$wpdate = get_user_meta( $wpUser->ID, 'wp_usersettingstime', true );
				$email = $wpUser['user_email'];
				$query .= "INSERT INTO `users` (`user_type`,`login_type`,`first_name`,`last_name`,`ph_no`,`email`,`password`,`temp_password`,`fb_id`,`user_image`,`paypal_user_name`,`paypal_email`,`service`,`status`,`remember_token`,`user_token`,`lattitude`,`longitude`,`device_token`,`created_at`,`updated_at`,`admin_approved`,`notification_status`) VALUES (1,1,'". $wpUser['first_name'] ."','". $wpUser['last_name'] ."',". $ph_no .",'". $email ."','". $wpUser['user_pass'] ."','". $wpUser['temp_password'] ."',". $fb_id .",'". $wpUser['user_image'] ."','". $wpUser['paypal_user_name'] ."','". $wpUser['paypal_email'] ."','". $wpUser['service'] ."',1,'". $wpUser['remember_token'] ."','". $wpUser['user_token'] ."','". $wpUser['lattitude'] ."','". $wpUser['longitude'] ."','". $wpUser['device_token'] ."','". $wpUser['user_registered'] ."','". $wpUser['user_registered'] ."',1,1)";
				$messages .= "We've added user by email ". $email . "<br/>";
			}
			mysql_query($query,$connect);
		} else {
			$messages .= "We can't synchronization users WP to QODA by now" . "<br/>";
		}		
	} else {
		$messages .= "We can't synchronization users from WP to QODA by now" . "<br/>";
	}
	wp_mail( $email_notification, 'Synchronization users from WP to QODA', $messages, $headers);
		//return $query;
}

function recursive_array_search($needle,$haystack) {
    foreach($haystack as $key=>$value) {
        $current_key=$key;
        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {
            return $current_key;
        }
    }
    return false;
}

function getSecondDatas() {
	$connect = getSecondConnect();

	$dbName = get_option( 'su_con_db_name', '' );
	mysql_select_db($dbName,$connect);
	$query = 'SELECT * FROM users';
	$qry = mysql_query($query,$connect);
	$customers = array();
	while ($user = mysql_fetch_array($qry)) {
		if($user['user_type'] != 2) {
			$customer['first_name'] = $user['first_name'];
			$customer['last_name'] = $user['last_name'];
			$customer['ph_no'] = $user['ph_no'];
			$customer['user_email'] = $user['email'];
			$customer['user_pass'] = $user['password'];
			$customer['temp_password'] = $user['temp_password'];
			$customer['fb_id'] = $user['fb_id'];
			$customer['user_image'] = $user['user_image'];
			$customer['paypal_user_name'] = $user['paypal_user_name'];
			$customer['paypal_email'] = $user['paypal_email'];
			$customer['service'] = $user['service'];
			$customer['remember_token'] = $user['remember_token'];
			$customer['user_token'] = $user['user_token'];
			$customer['lattitude'] = $user['lattitude'];
			$customer['longitude'] = $user['longitude'];
			$customer['device_token'] = $user['device_token'];
			$customer['user_registered'] = $user['created_at'];
			$customer['wp_usersettingstime'] = strtotime($user['updated_at']);
			$customers[] = $customer;
		}
    }
	return $customers;
}

function getSecondConnect() {
	global $connect;
	$connect = false;
	$host = get_option( 'su_con_host_ip', '' ) . ':' . get_option( 'su_con_port', '' );
	$user = get_option( 'su_con_db_user', '' );
	$pass = get_option( 'su_con_db_password', '' );
	
	$connectHost = mysql_connect($host, $user, $pass);
	
	if($connectHost) {
		$connect = $connectHost;		
	} else {
		$connect = false;
	}
	return $connect;
}

function getAllUsersWP() {
	global $wpdb;
	$users = get_users('orderby=id&role=subscriber');
	
	$datas = array();
	// User Loop
	if ( ! empty( $users) ) {
		foreach ( $users as $user ) {
			$data['first_name'] = $user->first_name;
			$data['last_name'] = $user->last_name;
			$data['ph_no'] = $user->ph_no;
			$data['user_email'] = $user->user_email;
			$data['user_pass'] = $user->user_pass;
			$data['temp_password'] = $user->temp_password;
			$data['fb_id'] = $user->fb_id;
			$data['user_image'] = $user->user_image;
			$data['paypal_user_name'] = $user->paypal_user_name;
			$data['paypal_email'] = $user->paypal_email;
			$data['service'] = $user->service;
			$data['remember_token'] = $user->remember_token;
			$data['user_token'] = $user->user_token;
			$data['lattitude'] = $user->lattitude;
			$data['longitude'] = $user->longitude;
			$data['device_token'] = $user->device_token;
			$data['user_registered'] = $user->user_registered;
			$data['wp_usersettingstime'] = get_user_meta( $user->ID, 'wp_usersettingstime', true );
			
			$datas[] = $data;
		}
	} else {
		return 'No users found.';
	}
	return $datas;
}

function display_user_custom_qoda( $user ) { ?>
    <h3>QODA Fields</h3>
    <table class="form-table">
        <tr>
            <th><label>Phone no.</label></th>
            <td><input type="text" id="ph_no" name="ph_no" value="<?php echo get_user_meta( $user->ID, 'ph_no', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Temp password</label></th>
            <td><input type="text" id="temp_password" name="temp_password" value="<?php echo get_user_meta( $user->ID, 'temp_password', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Facebook Id</label></th>
            <td><input type="text" id="fb_id" name="fb_id" value="<?php echo get_user_meta( $user->ID, 'fb_id', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Paypal user name</label></th>
            <td><input type="text" id="paypal_user_name" name="paypal_user_name" value="<?php echo get_user_meta( $user->ID, 'paypal_user_name', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Paypal Email</label></th>
            <td><input type="text" id="paypal_email" name="paypal_email" value="<?php echo get_user_meta( $user->ID, 'paypal_email', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Service</label></th>
            <td><input type="text" id="service" name="service" value="<?php echo get_user_meta( $user->ID, 'service', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Remember token</label></th>
            <td><input type="text" id="remember_token" name="remember_token" value="<?php echo get_user_meta( $user->ID, 'remember_token', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>User token</label></th>
            <td><input type="text" id="user_token" name="user_token" value="<?php echo get_user_meta( $user->ID, 'user_token', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Lattitude</label></th>
            <td><input type="text" id="lattitude" name="lattitude" value="<?php echo get_user_meta( $user->ID, 'lattitude', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Longitude</label></th>
            <td><input type="text" id="longitude" name="longitude" value="<?php echo get_user_meta( $user->ID, 'longitude', true ); ?>" class="regular-text"  /></td>
		</tr>
		<tr>
			<th><label>Device token</label></th>
            <td><input type="text" id="device_token" name="device_token" value="<?php echo get_user_meta( $user->ID, 'device_token', true ); ?>" class="regular-text"  /></td>
        </tr>
    </table>
    <?php
}

add_action( 'show_user_profile', 'display_user_custom_qoda' );
add_action( 'personal_options_update', 'save_profile_qoda' );
add_action( 'edit_user_profile_update', 'save_profile_qoda' );

function save_profile_qoda($user_id) {
	$blogtime = strtotime(current_time( 'mysql' ));
	$ph_no = isset( $_POST['ph_no'] ) ? $_POST['ph_no'] : 0;
    $temp_password = isset( $_POST['temp_password'] ) ? $_POST['temp_password'] : false;
    $fb_id = isset( $_POST['fb_id'] ) ? $_POST['fb_id'] : 0;
    $paypal_user_name = isset( $_POST['paypal_user_name'] ) ? $_POST['paypal_user_name'] : false;
    $paypal_email = isset( $_POST['paypal_email'] ) ? $_POST['paypal_email'] : false;
    $service = isset( $_POST['service'] ) ? $_POST['service'] : 0;
    $remember_token = isset( $_POST['remember_token'] ) ? $_POST['remember_token'] : false;
    $user_token = isset( $_POST['user_token'] ) ? $_POST['user_token'] : false;
    $lattitude = isset( $_POST['lattitude'] ) ? $_POST['lattitude'] : false;
    $longitude = isset( $_POST['longitude'] ) ? $_POST['longitude'] : false;
    $device_token = isset( $_POST['device_token'] ) ? $_POST['device_token'] : false;
    update_usermeta( $user_id, 'ph_no', $ph_no );
    update_usermeta( $user_id, 'temp_password', $temp_password );
    update_usermeta( $user_id, 'fb_id', $fb_id );
    update_usermeta( $user_id, 'paypal_user_name', $paypal_user_name );
    update_usermeta( $user_id, 'paypal_email', $paypal_email );
    update_usermeta( $user_id, 'service', $service );
    update_usermeta( $user_id, 'remember_token', $remember_token );
    update_usermeta( $user_id, 'user_token', $user_token );
    update_usermeta( $user_id, 'lattitude', $lattitude );
    update_usermeta( $user_id, 'device_token', $device_token );
    update_usermeta( $user_id, 'wp_usersettingstime', $blogtime );
}
function con_another_host_field($args) {
	$value = get_option( 'su_con_host_ip', '' );
	?>
	<input type="text" name="su_con_host_ip" id="su_con_host_ip" value="<?php echo $value; ?>" />
	<?php
}
function con_another_port_field($args) {
	$value = get_option( 'su_con_port', '' );
	?>
	<input type="text" name="su_con_port" id="su_con_port" value="<?php echo $value; ?>" />
	<?php
}

function con_another_db_user_field($args) {
	$value = get_option( 'su_con_db_user', '' );
	?>
	<input type="text" name="su_con_db_user" id="su_con_db_user" value="<?php echo $value; ?>" />
	<?php
}

function con_another_db_password_field($args) {
	$value = get_option( 'su_con_db_password', '' );
	?>
	<input type="text" name="su_con_db_password" id="su_con_db_password" value="<?php echo $value; ?>" />
	<?php
}

function con_another_db_name_field($args) {
	$value = get_option( 'su_con_db_name', '' );
	?>
	<input type="text" name="su_con_db_name" id="su_con_db_name" value="<?php echo $value; ?>" />
	<?php
}

function sync_email_notice_field($args) {
	$value = get_option( 'sync_email_notice', '' );
	?>
	<input type="text" name="sync_email_notice" id="sync_email_notice" value="<?php echo $value; ?>" />
	<?php
}

add_action('admin_init', 'selection_connect_database');  
function selection_connect_database() { 
	add_settings_section(  
        'synchronization_users_settings', // Section ID 
        'Synchronization user settings', // Section Title
        null, // Callback
        'general' // What Page?  This makes the section show up on the General Settings Page
    );
	
	add_settings_field( // Option 1
        'sync_email_notice', // Option ID
        'Email notification:', // Label
        'sync_email_notice_field', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'synchronization_users_settings', // Name of our section
        array( // The $args
            'sync_email_notice' // Should match Option ID
        )  
    );
	
	add_settings_field( // Option 1
        'su_con_host_ip', // Option ID
        'Host name | Ip Address:', // Label
        'con_another_host_field', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'synchronization_users_settings', // Name of our section
        array( // The $args
            'su_con_host_ip' // Should match Option ID
        )  
    );
	
    add_settings_field( // Option 1
        'su_con_port', // Option ID
        'Port:', // Label
        'con_another_port_field', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'synchronization_users_settings', // Name of our section
        array( // The $args
            'su_con_port' // Should match Option ID
        )  
    );

    add_settings_field( // Option 2
        'su_con_db_user', // Option ID
        'User:', // Label
        'con_another_db_user_field', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'synchronization_users_settings', // Name of our section
        array( // The $args
            'su_con_db_user' // Should match Option ID
        )  
    );

    add_settings_field( // Option 2
        'su_con_db_password', // Option ID
        'Password:', // Label
        'con_another_db_password_field', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'synchronization_users_settings', // Name of our section
        array( // The $args
            'su_con_db_password' // Should match Option ID
        )  
    );
	
	add_settings_field( // Option 2
        'su_con_db_name', // Option ID
        'DB Name:', // Label
        'con_another_db_name_field', // !important - This is where the args go!
        'general', // Page it will be displayed (General Settings)
        'synchronization_users_settings', // Name of our section
        array( // The $args
            'su_con_db_name' // Should match Option ID
        )  
    );
	
	register_setting('general','sync_email_notice', 'esc_attr');
	register_setting('general','su_con_host_ip', 'esc_attr');
	register_setting('general','su_con_port', 'esc_attr');
	register_setting('general','su_con_db_user', 'esc_attr');
	register_setting('general','su_con_db_password', 'esc_attr');
	register_setting('general','su_con_db_name', 'esc_attr');
}

add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );
function ajax_test_enqueue_scripts() {

  wp_localize_script( 'theme-script', 'postlove', array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'action' => 'register_account'
  ));}

add_action("wp_ajax_register_account", "register_account");
add_action("wp_ajax_nopriv_register_account", "register_account");

function register_account(){
    if(isset($_POST['user_type'])){
        $userdata = array(
            'user_login'    =>   $_POST['mail'],
            'user_email'    =>   $_POST['mail'],
            'user_pass'     =>   $_POST['pass'],
            'first_name'    =>   $_POST['fname'],
            'last_name'     =>   $_POST['lname'],
            'user_registered' => date('Y-m-d H:i:s'),
            'role'        => 'subscriber'
        );
        $new_user_id = wp_insert_user($userdata);
        if(!is_wp_error($new_user_id)){
            add_user_meta($new_user_id,'phone',$_POST['phone']);
            add_user_meta($new_user_id,'language',$_POST['language']);
            $card_list=array('visa-card'=>'Visa card','master-card'=>'Master card','maestro-card'=>'Maestro card');
            add_user_meta($new_user_id,'card_type',$card_list[$_POST['card_type']]);
            add_user_meta($new_user_id,'card_number',$_POST['card_number']);
            add_user_meta($new_user_id,'cvv',$_POST['cvv']);
            if($_POST['exp_month']<10){
                $month='0'.$_POST['exp_month'];
            }else{
                $month=$_POST['exp_month'];
            }
            $date=$_POST['exp_year'].'/'.$month;
            add_user_meta($new_user_id,'expiration_date',$date);
            add_user_meta($new_user_id,'postal_code',$_POST['postcode']);
            add_user_meta($new_user_id,'user_type',$_POST['user_type']);
            if($_POST['user_type']=='Service Provider'){
                add_user_meta($new_user_id,'address',$_POST['address']);
                add_user_meta($new_user_id,'city',$_POST['city']);
                add_user_meta($new_user_id,'state',$_POST['state']);
                add_user_meta($new_user_id,'zipcode',$_POST['zipcode']);
            }
            $login_data['user_login'] = $_POST['mail'];
            $login_data['user_password'] = $_POST['pass'];
            $login_data['remember'] = false; // set true or false for remember                
            $user_verify = wp_signon( $login_data, false ); 
            if ( !is_wp_error( $user_verify ) ) {                    
                //$user = get_userdatabylogin($username);
                $user_id = $new_user_id;
                wp_setcookie($_POST['mail'], $_POST['pass'], true);
                wp_set_current_user($user_id, $_POST['mail']);
                do_action('wp_login', $_POST['mail']);
            }
            echo 'success';
        }else{
            echo 'Error:'.$new_user_id->get_error_message();
        }
        
    }else{
        if(isset($_POST['email'])){
            if(email_exists($_POST['email'])){
                echo 'exist';
            }else{
                echo 'no-exist';
            }
        }
    }
}
