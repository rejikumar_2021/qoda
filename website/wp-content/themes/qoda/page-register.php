<?php
/* Template name: Registerr page */

get_header();
?>
<div class="signup-form-wrapperr">
    <div class="signup-form-innerr">
        <h1>Sign Up to Get Roadside Assistance</h1>
        <div class="heading-seperator"></div>
        <p class="p_center">Welcome to Qoda, the easiest way to get around at the tap of a button.
        Create your account and get moving in minutes.</p>
        <div class="tab-regis-frm-wrapper">
            <ul class="tab-register-control">
                <li class="current_show"><a href="#customer">Become a Customer</a></li>
                <li><a href="#provider">Become a Service Provider</a></li>
            </ul>
        </div>
        <div class="signup-form">
                <div id="customer" class="tab_frm_register">
                    <form action="" method="post" id="form-sign-up-customer">
                        <p class="text_description"><i>Sign Up to Get Roadside Assistance<br />
                        Welcome to QODA - the easiest way to get roadside assistance.  <br />
                        Create your customer account and get quick, affordable roadside assistance.</i></p>
                        <h2>Account <span> Required<span>* </span></span></h2>
                        <p>
                            <label for=""><span>*</span> EMAIL</label>
                            <input type="email" name="mail" placeholder="Name@example.com"/>
                        </p>
                        <p>
                            <label for=""><span>*</span> PASSWORD</label>
                            <input type="password" name="pass" placeholder="At leat 8 characters"/>
                        </p>
                        <p>
                            <label for=""><span>*</span> PASSWORD COMFIRMATION</label>
                            <input type="password" name="confirm-pass" placeholder="At leat 8 characters"/>
                        </p>
                        <div class="form-seperator"></div>
                        <h2>Profile</h2>
                        <p>
                            <label for=""><span>*</span> NAME</label>
                            <input type="text" name="fname" placeholder="First Name" class="input-half" />
                            <input type="text" name="lname" placeholder="Last Name" class="input-half" />
                        </p>
                        <p>
                            <label for=""><span>*</span> MOBILE NUMBER</label>
                            <input type="text" name="phone" placeholder="+1 (502) 6666 9999"/>
                        </p>
                        <p>
                            <label for=""><span>*</span> LANGUAGE</label>
                            <select name="language">
                                <option value="English (US)">English (US)</option>
                                <option value="English (UK)">English (UK)</option>
                                <option value="France">France</option>
                            </select>
                        </p>
                       
                        <div class="clear-fix"></div>
                        <p>
                            <input type="submit" value="CREATE ACCOUNT" name="submit-signup"/>
                        </p>                
                    </form>
                </div>
                <div id="provider" class="tab_frm_register">
                    <form action="" method="post" id="form-sign-up-sp">
                        <p class="text_description"><i>Sign Up to Provide Services<br />
                        Welcome to QODA - the easiest way to get roadside assistance.<br /> 
                        Create your service provider account and start helping others!</i></p>
                        <h2>Account <span> Required<span>* </span></span></h2>
                        <p>
                            <label for=""><span>*</span> EMAIL</label>
                            <input type="email" name="mail" placeholder="Name@example.com"/>
                        </p>
                        <p>
                            <label for=""><span>*</span> PASSWORD</label>
                            <input type="password" name="pass" placeholder="At leat 8 characters"/>
                        </p>
                        <p>
                            <label for=""><span>*</span> PASSWORD COMFIRMATION</label>
                            <input type="password" name="confirm-pass" placeholder="At leat 8 characters"/>
                        </p>
                        <div class="form-seperator"></div>
                        <h2>Profile</h2>
                        <p>
                            <label for=""><span>*</span> NAME</label>
                            <input type="text" name="fname" placeholder="First Name" class="input-half" />
                            <input type="text" name="lname" placeholder="Last Name" class="input-half" />
                        </p>
                        <p>
                            <label for=""><span>*</span> MOBILE NUMBER</label>
                            <input type="text" name="phone" placeholder="+1 (502) 6666 9999"/>
                        </p>
                        <p>
                            <label for=""><span>*</span> LANGUAGE</label>
                            <select name="language">
                                <option value="English (US)">English (US)</option>
                                <option value="English (UK)">English (UK)</option>
                                <option value="France">France</option>
                            </select>
                        </p>
                        <p>
                            <label for=""><span>*</span> ADDRESS - Your address is needed because QODA will mail you QODA supplies needed to complete services</label>
                            <input type="text" name="address" placeholder="Address"/>
                        </p>
                        <p>
                            <label><span>*</span> CITY</label>
                            <input type="text" name="city" placeholder="City"/>                                 
                        </p>
                        <div class="col-50-percent first">
                            <p>
                                <label><span>*</span> STATE</label>
                                <input type="text" name="state" placeholder="State"/>
                            </p>
                        </div>  
                        <div class="col-50-percent last">
                            <p>
                                <label><span>*</span> ZIP CODE</label>
                                <input type="text" name="zip-code" placeholder="Zip code"/>
                            </p>
                        </div>  
                      
                        <div class="col-40-percent">
                            <p>                                
                                <label><span>*</span> POSTAL CODE</label>
                                <input placeholder="10000" name="postal-code" type="text"/>
                            </p>
                        </div>
                        <div class="clear-fix"></div>
                        <p>
                            <input type="submit" value="CREATE ACCOUNT" name="submit-signup"/>
                        </p>                
                    </form>
                </div>
            <div class="agree-text">
                <p>By clicking “Create Account” , you agree to Qoda's</p>
                <p><a href="<?php echo home_url();?>/terms-and-conditions-pages/">Terms and Conditions and Privacy Policy.</a></p>
            </div>
            <input type="hidden" class="link_to_redirect" value="<?php echo home_url('/thank-you/');?>"/>
        </div>
    </div>
</div>
<?php
get_footer();
?>
