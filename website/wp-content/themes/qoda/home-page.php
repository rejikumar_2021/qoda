<?php
/*
Template name: Home page
*/

get_header();
?>

<div class="block-content-wrapper with_bg">
    <img class="bg-block" src="/qoda/wp-content/themes/qoda/images/stockvault-san-francisco111358.jpg"/>
    <div class="block-content-inner">
        <div class="banner-wrapper">
            <div class="banner-content-left center-height">
                <h2>On Demand Road Side Assistance</h2>
                <p>Nam fringilla id sapien at pulvinar. Quisque laoreet sem imperdiet, pharetra nibh eget, bibendum felis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at tortor blandit, facilisis est in, ultricies tellus. </p>
                <a href="http://qodamobile.com/register/" class="link-btn-blue">Sign up Now <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div>
            <div class="banner-content-right">
                <img src="/qoda/wp-content/themes/qoda/images/iphone-img.png"/>
            </div>
        </div>
    </div>
</div>
<div class="clear-fix"></div>
<div class="block-content-wrapper bg_gray">
    <div class="block-content-inner">
        <div class="col-full-width">
            <h3>Beautiful Interface</h3>
            <div class="heading-seperator"></div>
            <p>The sight of the tumblers restored Bob Sawyer to a degree of equanimity which he had not possessed since his interview with his landlady. His face brightened up, and he began to feel quite convivial.</p>
        </div>
    </div>
    <div class="col-full-width">
        <img class="img-full-width" src="/qoda/wp-content/themes/qoda/images/banner-landing.png"/>
    </div>
</div>
<div class="clear-fix"></div>
<div class="block-content-wrapper">
    <div class="block-content-inner">
        <div class="col-half top_180"><img src="/qoda/wp-content/themes/qoda/images/iphone-img-2.png"/></div>
        <div class="col-half">
            <h3>Request a Service</h3>
            <p>Rest easy with QODA.  With QODA, choose which service you need and one of our service providers will be on the way.</p>                    
        </div>
    </div>
</div>
<div class="clear-fix"></div>
<div class="block-content-wrapper bg_gray">
    <div class="block-content-inner">
        <div class="col-half">
            <h3>Pay for your serive with Stripeᵀᴹ</h3>
            <p>Use Stripe to pay for your service. Charges are based on a fixed fee for the service and travel cost of the service person. Travel costs are based on distance to the customer</p>                    
        </div>
        <div class="col-half top_250"><img src="/qoda/wp-content/themes/qoda/images/iphone-screen-img.png"/></div>
    </div>
</div>
<div class="clear-fix"></div>
<div class="block-content-wrapper">
    <div class="block-content-inner">
        <div class="col-half top_180"><img src="/qoda/wp-content/themes/qoda/images/iphone-img-3.png"/></div>
        <div class="col-half">
            <h3>Set Your Location</h3>
            <p>Tell QODA where you are. QODA will find a service person nearest to you. Our goal is to have your roadside emergency resolved as soon as possible. </p>                    
        </div>
    </div>
</div>
<div class="block-content-wrapper bg_gray">
    <div class="block-content-inner">
        <div class="col-half">
            <h3>Service Person Accepts Your Request</h3>
            <p>QODA service people will see all requests within a 50 mile radius.  Once the service person accepts a customer's request, the customer will be one step closer to having their roadside emergency resolved.</p>                    
        </div>
        <div class="col-half top_250"><img src="/qoda/wp-content/themes/qoda/images/iphone-screen-img1.png"/></div>
    </div>
</div>
<div class="block-author-wrapper">
    <div class="block-author-inner">
        <img src="/qoda/wp-content/themes/qoda/images/chichi.png"/>
        <p>CHICHI MADUS</p>
        <p>QODA CEO & Founder</p>
        <p><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> support@qodamobile.com</a></p>
    </div>
</div>
<?php 
get_footer();
?>
