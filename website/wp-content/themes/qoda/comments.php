<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<style>
ul{list-style:none;}
li{list-style: none;}
</style>


	<?php if ( have_comments() ) : ?>
		<h2 class="fw_light color_dark m_bottom_25">Comments (<?php comments_number('0','1','%');?>)</h2>

		<?php //twentyfifteen_comment_nav(); ?>

		
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 56,
                    'callback'=>'qoda_comment'
				) );
			?>


		<?php //twentyfifteen_comment_nav(); ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfifteen' ); ?></p>
	<?php endif; ?>

<?php $comment_args = array( 'title_reply'=>'<h2 class="fw_light color_dark m_bottom_23">Leave a Comment</h2>',

'fields' => apply_filters( 'comment_form_default_fields', array(

'author' => '<ul>
				<li class="row fw_light"><label>Your Name*</label>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        
                        <input id="author" name="author" value="'.esc_attr( $commenter['comment_author'] ).'" type="text" class="r_corners color_grey r_corners w_full m_bottom_10">
					</div></li>',   

    'email'  => '<li class="row fw_light"><label>Email Address *</label><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    
					<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" class="r_corners color_grey r_corners w_full m_bottom_10">
				</div></li>',

    'url'    => '<li class="row fw_light"><label>Website</label><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        
						<input type="url" id="url" name="url" value="'. esc_attr(  $commenter['comment_author_url'] ) .'" class="r_corners color_grey r_corners w_full m_bottom_10">
					</div>
				</li>' ) ),

    'comment_field' => '
						<li class="m_bottom_20 fw_light">
                        <label>Your Comments*</label>
							<div><textarea id="comment" name="comment" class="color_grey d_block r_corners w_full height_4"></textarea></div>
						</li>',

    'comment_notes_after' => '</ul>',
    'class_submit'=>'btn-submit-blue',
    'label_submit'=>__('Submit')

);

comment_form($comment_args); ?>



<!-- .comments-area -->
