<?php

get_header();
?>
<div class="block-content-wrapper">
<div class="block-content-inner">
<?php
if(have_posts()):the_post();
    the_content();
endif;
?>
</div>
</div>
<?php
get_footer();
?>